# -*- encoding: utf-8 -*-
import pytest

from django.urls import reverse
from http import HTTPStatus

from invoice.tests.factories import (
    BatchFactory,
    BatchInvoiceFactory,
    InvoiceCreditFactory,
    InvoiceFactory,
)
from login.tests.factories import TEST_PASSWORD, UserFactory


@pytest.mark.django_db
def test_batch_invoice_list(client):
    batch = BatchFactory()
    BatchInvoiceFactory(
        batch=batch,
        invoice=InvoiceFactory(invoice_date=batch.batch_date, number=1),
    )
    BatchInvoiceFactory(
        batch=BatchFactory(),
        invoice=InvoiceFactory(invoice_date=batch.batch_date, number=2),
    )
    BatchInvoiceFactory(
        batch=batch,
        invoice=InvoiceFactory(invoice_date=batch.batch_date, number=3),
    )
    BatchInvoiceFactory(
        batch=batch,
        invoice=InvoiceFactory(invoice_date=batch.batch_date, number=4),
    )
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    response = client.get(
        reverse("invoice.batch.invoice.list", args=[batch.pk])
    )
    assert HTTPStatus.OK == response.status_code
    assert "batch" in response.context
    assert "batch_gross" in response.context
    assert "batch_net" in response.context
    assert "batch_vat" in response.context
    assert "object_list" in response.context
    assert "batchinvoice_list" in response.context
    assert batch == response.context["batch"]
    assert [4, 3, 1] == [
        x.invoice.number for x in response.context["batchinvoice_list"]
    ]


@pytest.mark.django_db
def test_invoice_detail(client):
    invoice = InvoiceFactory()
    InvoiceCreditFactory(
        invoice=invoice, credit_note=InvoiceFactory(is_credit=True)
    )
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    response = client.get(reverse("invoice.detail", args=[invoice.pk]))
    assert HTTPStatus.OK == response.status_code


@pytest.mark.django_db
def test_invoice_detail_credit_note(client):
    credit_note = InvoiceFactory(is_credit=True)
    InvoiceCreditFactory(
        credit_note=credit_note, invoice=InvoiceFactory(is_credit=True)
    )
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    response = client.get(reverse("invoice.detail", args=[credit_note.pk]))
    assert HTTPStatus.OK == response.status_code
