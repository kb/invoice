# -*- encoding: utf-8 -*-
import pytest

from django.urls import reverse

from contact.tests.factories import ContactFactory, UserContactFactory
from crm.tests.factories import CrmContactFactory, TicketFactory
from invoice.tests.factories import (
    BatchFactory,
    InvoiceContactFactory,
    InvoiceFactory,
)
from login.tests.factories import TEST_PASSWORD, UserFactory
from login.tests.fixture import perm_check
from login.tests.scenario import get_user_web


@pytest.mark.django_db
def test_batch_invoice_list(perm_check):
    batch = BatchFactory()
    url = reverse("invoice.batch.invoice.list", args=[batch.pk])
    perm_check.staff(url)


@pytest.mark.django_db
def test_contact_detail(perm_check):
    contact = ContactFactory()
    url = reverse("contact.detail", args=[contact.pk])
    perm_check.staff(url)


@pytest.mark.django_db
def test_contact_list(perm_check):
    perm_check.staff(reverse("contact.list"))


@pytest.mark.django_db
def test_invoice_detail(perm_check):
    invoice = InvoiceFactory()
    url = reverse("invoice.detail", args=[invoice.pk])
    perm_check.staff(url)
