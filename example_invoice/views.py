# -*- encoding: utf-8 -*-
from braces.views import LoginRequiredMixin, StaffuserRequiredMixin
from django.views.generic import DetailView, TemplateView, ListView

from base.view_utils import BaseMixin
from crm.views import TicketDetailMixin
from contact.views import ContactDetailMixin, ContactListMixin
from invoice.views import (
    BatchInvoiceListMixin,
    InvoiceDetailMixin,
    InvoiceListMixin,
)


class BatchInvoiceListView(
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    BatchInvoiceListMixin,
    BaseMixin,
    ListView,
):
    template_name = "example/batch_invoice_list.html"


class ContactDetailView(
    LoginRequiredMixin, ContactDetailMixin, BaseMixin, DetailView
):
    pass


class ContactListView(
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    ContactListMixin,
    BaseMixin,
    ListView,
):
    template_name = "example/contact_list.html"


class InvoiceDetailView(
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    InvoiceDetailMixin,
    BaseMixin,
    DetailView,
):
    template_name = "example/invoice_detail.html"


class InvoiceListView(
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    InvoiceListMixin,
    BaseMixin,
    ListView,
):
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({"show_download": True})
        return context


class HomeView(TemplateView):
    template_name = "example/home.html"


class SettingsView(TemplateView):
    template_name = "example/settings.html"


class TicketDetailView(
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    TicketDetailMixin,
    BaseMixin,
    ListView,
):
    template_name = "example/ticket_detail.html"
