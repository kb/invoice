# -*- encoding: utf-8 -*-
from django.conf import settings
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.urls import include, re_path, reverse_lazy
from django.views.generic import RedirectView

from example_invoice.views import InvoiceListView
from .views import (
    BatchInvoiceListView,
    ContactDetailView,
    ContactListView,
    HomeView,
    InvoiceDetailView,
    SettingsView,
    TicketDetailView,
)


urlpatterns = [
    re_path(r"^$", view=HomeView.as_view(), name="project.home"),
    re_path(r"^", view=include("login.urls")),
    re_path(
        r"^batch/(?P<pk>\d+)/invoice/$",
        view=BatchInvoiceListView.as_view(),
        name="invoice.batch.invoice.list",
    ),
    re_path(r"^contact/", view=include("contact.urls")),
    re_path(
        r"^contact/(?P<pk>\d+)/$",
        view=ContactDetailView.as_view(),
        name="contact.detail",
    ),
    re_path(r"^contact/$", view=ContactListView.as_view(), name="contact.list"),
    re_path(r"^crm/", view=include("crm.urls")),
    re_path(r"^invoice/$", view=InvoiceListView.as_view(), name="invoice.list"),
    re_path(r"^invoice/", view=include("invoice.urls")),
    re_path(
        r"^invoice/(?P<pk>\d+)/$",
        view=InvoiceDetailView.as_view(),
        name="invoice.detail",
    ),
    re_path(r"^report/", view=include("report.urls")),
    re_path(
        r"^ticket/(?P<pk>\d+)/$",
        view=TicketDetailView.as_view(),
        name="ticket.detail",
    ),
    re_path(
        r"^home/user/$",
        view=RedirectView.as_view(url=reverse_lazy("project.home")),
        name="project.dash",
    ),
    re_path(
        r"^settings/$", view=SettingsView.as_view(), name="project.settings"
    ),
]

urlpatterns += staticfiles_urlpatterns()

if settings.DEBUG:
    import debug_toolbar

    urlpatterns = [
        re_path(r"^__debug__/", include(debug_toolbar.urls))
    ] + urlpatterns
