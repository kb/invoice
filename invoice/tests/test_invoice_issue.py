# -*- encoding: utf-8 -*-
import pytest
import pytz

from datetime import date, datetime
from django.db import IntegrityError
from freezegun import freeze_time

from invoice.models import InvoiceIssue, InvoiceIssueLine
from .factories import InvoiceFactory, InvoiceIssueFactory


@pytest.mark.django_db
def test_duplicate():
    """
    not entirely sure how to test the ordering of the invoice issues...
    """
    invoice = InvoiceFactory()
    with freeze_time(date(2019, 7, 20)):
        InvoiceIssue.objects.init_invoice_issue(invoice, "Apple")
        with pytest.raises(IntegrityError) as e:
            InvoiceIssueFactory(invoice=invoice)
    assert "duplicate key value violates unique constraint" in str(e.value)


@pytest.mark.django_db
def test_init_invoice_issue():
    invoice = InvoiceFactory()
    # create invoice issue using InvoiceIssueManager
    InvoiceIssue.objects.init_invoice_issue(invoice, "test")
    # check it exists
    assert 1 == InvoiceIssue.objects.count()
    assert 1 == InvoiceIssueLine.objects.count()
    invoice_issue = InvoiceIssue.objects.first()
    invoice_issue_line = InvoiceIssueLine.objects.first()
    # a newly created invoice issue should not be confirmed
    assert invoice == invoice_issue.invoice
    assert invoice_issue.comment is None
    assert invoice_issue.confirmed is False
    assert invoice_issue.confirmed_by_user is None
    assert invoice_issue == invoice_issue_line.invoice_issue
    assert "test" == invoice_issue_line.description


@pytest.mark.django_db
def test_init_invoice_issue_two_issues_for_one_invoice():
    invoice = InvoiceFactory()
    with freeze_time(date(2019, 7, 20)):
        InvoiceIssue.objects.init_invoice_issue(invoice, "A")
        InvoiceIssue.objects.init_invoice_issue(invoice, "B")
    assert 1 == InvoiceIssue.objects.count()
    assert 2 == InvoiceIssueLine.objects.count()


@pytest.mark.django_db
def test_init_invoice_issue_duplicate_date_and_description():
    invoice = InvoiceFactory()
    with freeze_time(date(2019, 1, 1)):
        InvoiceIssue.objects.init_invoice_issue(invoice, "A")
    with freeze_time(date(2019, 1, 31)):
        InvoiceIssue.objects.init_invoice_issue(invoice, "A")
        InvoiceIssue.objects.init_invoice_issue(invoice, "B")
        InvoiceIssue.objects.init_invoice_issue(invoice, "C")
    assert ["A", "B", "C"] == [
        x.description
        for x in InvoiceIssueLine.objects.all().order_by("description")
    ]


@pytest.mark.parametrize(
    "invoice_date,expect",
    [
        (date(2019, 1, 31), ["A"]),
        (date(2019, 7, 20), ["B", "C"]),
        (None, ["A", "B", "C"]),
    ],
)
@pytest.mark.django_db
def test_issues(invoice_date, expect):
    issue_a = InvoiceIssue.objects.init_invoice_issue(
        InvoiceFactory(invoice_date=date(2019, 1, 31)), "A"
    )
    issue_a.invoice_issue.comment = "A"
    issue_a.invoice_issue.save()
    # b
    issue_b = InvoiceIssue.objects.init_invoice_issue(
        InvoiceFactory(invoice_date=date(2019, 7, 20)), "B"
    )
    issue_b.invoice_issue.comment = "B"
    issue_b.invoice_issue.save()
    # c
    issue_c = InvoiceIssue.objects.init_invoice_issue(
        InvoiceFactory(invoice_date=date(2019, 7, 20)), "C"
    )
    issue_c.invoice_issue.comment = "C"
    issue_c.invoice_issue.save()
    assert set(expect) == set(
        [x.comment for x in InvoiceIssue.objects.issues(invoice_date)]
    )


@pytest.mark.parametrize(
    "invoice_date,expect",
    [
        (date(2019, 1, 31), ["A"]),
        (date(2019, 7, 20), ["C"]),
        (None, ["A", "C"]),
    ],
)
@pytest.mark.django_db
def test_issues_outstanding(invoice_date, expect):
    issue_a = InvoiceIssue.objects.init_invoice_issue(
        InvoiceFactory(invoice_date=date(2019, 1, 31)), "A"
    )
    issue_a.invoice_issue.comment = "A"
    issue_a.invoice_issue.save()
    # b
    issue_b = InvoiceIssue.objects.init_invoice_issue(
        InvoiceFactory(invoice_date=date(2019, 7, 20)), "B"
    )
    issue_b.invoice_issue.comment = "B"
    issue_b.invoice_issue.confirmed = True
    issue_b.invoice_issue.save()
    # c
    issue_c = InvoiceIssue.objects.init_invoice_issue(
        InvoiceFactory(invoice_date=date(2019, 7, 20)), "C"
    )
    issue_c.invoice_issue.comment = "C"
    issue_c.invoice_issue.save()
    assert set(expect) == set(
        [
            x.comment
            for x in InvoiceIssue.objects.issues_outstanding(invoice_date)
        ]
    )


@pytest.mark.django_db
def test_lines():
    invoice = InvoiceFactory()
    InvoiceIssue.objects.init_invoice_issue(invoice, "A")
    InvoiceIssue.objects.init_invoice_issue(InvoiceFactory(), "B")
    InvoiceIssue.objects.init_invoice_issue(invoice, "C")
    assert 2 == InvoiceIssue.objects.count()
    invoice_issue = InvoiceIssue.objects.get(invoice=invoice)
    assert ["C", "A"] == invoice_issue.lines()


@pytest.mark.django_db
def test_ordering():
    invoice_1 = InvoiceFactory(invoice_date=date(2019, 1, 1))
    invoice_2 = InvoiceFactory(invoice_date=date(2019, 1, 31))
    with freeze_time(date(2019, 1, 1)):
        issue_line_a = InvoiceIssue.objects.init_invoice_issue(invoice_1, "A")
    with freeze_time(date(2019, 1, 31)):
        issue_line_b = InvoiceIssue.objects.init_invoice_issue(invoice_2, "B")
    assert [issue_line_b.invoice_issue.pk, issue_line_a.invoice_issue.pk] == [
        x.pk for x in InvoiceIssue.objects.all()
    ]


@pytest.mark.django_db
def test_ordering_invoice_issue_line():
    invoice_1 = InvoiceFactory(invoice_date=date(2019, 1, 31))
    invoice_2 = InvoiceFactory(invoice_date=date(2019, 1, 1))
    with freeze_time(date(2019, 1, 31)):
        InvoiceIssue.objects.init_invoice_issue(invoice_1, "x")
        InvoiceIssue.objects.init_invoice_issue(invoice_1, "z")
    with freeze_time(date(2019, 1, 1)):
        InvoiceIssue.objects.init_invoice_issue(invoice_2, "y")
    assert ["z", "x", "y"] == [
        x.description for x in InvoiceIssueLine.objects.all()
    ]


@pytest.mark.django_db
def test_str():
    invoice_issue = InvoiceIssueFactory(
        invoice=InvoiceFactory(
            invoice_date=date(2019, 11, 23), number=3, prefix="I"
        )
    )
    assert "Invoice issue {} for I000003 dated 23/11/2019".format(
        invoice_issue.pk
    ) == str(invoice_issue)


@pytest.mark.django_db
def test_str_invoice_issue_line():
    with freeze_time(datetime(2019, 7, 31, 14, 23, 0, tzinfo=pytz.utc)):
        InvoiceIssue.objects.init_invoice_issue(
            InvoiceFactory(number=3, prefix="I"), "Lemon"
        )
    assert 1 == InvoiceIssueLine.objects.count()
    invoice_issue_line = InvoiceIssueLine.objects.first()
    assert "Invoice issue {} for I000003 dated 31/07/2019 14:23: Lemon".format(
        invoice_issue_line.invoice_issue.pk
    ) == str(invoice_issue_line)
