# -*- encoding: utf-8 -*-
import pytest

from decimal import Decimal
from django.urls import reverse

from contact.tests.factories import ContactFactory
from finance.models import Country, VatSettings
from finance.tests.factories import CountryFactory
from invoice.models import InvoiceContact
from invoice.tests.factories import InvoiceContactFactory
from login.tests.factories import UserFactory


@pytest.mark.django_db
def test_get_absolute_url():
    contact = ContactFactory(user=UserFactory(username="alan"))
    obj = InvoiceContactFactory(contact=contact)
    expect = reverse("contact.detail", args=[contact.pk])
    assert expect == obj.get_absolute_url()


@pytest.mark.parametrize(
    "hourly_rate,vat_number,expect",
    [
        (Decimal("12.34"), "", "Mr Alan Jones @ 12.34"),
        (Decimal("12.34"), "ABC", "Mr Alan Jones @ 12.34, VAT Number ABC"),
        (None, "", "Mr Alan Jones"),
        (None, "ABC", "Mr Alan Jones, VAT Number ABC"),
    ],
)
@pytest.mark.django_db
def test_str(hourly_rate, vat_number, expect):
    user = UserFactory(first_name="Alan", last_name="Jones")
    contact = ContactFactory(user=user, company_name="", title="Mr")
    x = InvoiceContactFactory(
        contact=contact, hourly_rate=hourly_rate, vat_number=vat_number
    )
    assert expect == str(x)


@pytest.mark.parametrize(
    (
        "country_code,hourly,vat_number,on_account,"
        "expect_country_code,expect_hourly,expect_vat_number,expect_on_account"
    ),
    [
        ("GB", Decimal("1"), "ABC", False, "GB", Decimal("1"), "ABC", False),
        ("GB", Decimal("1"), "XYZ", True, "GB", Decimal("1"), "XYZ", True),
        ("GB", Decimal("2"), "ABC", False, "GB", Decimal("2"), "ABC", False),
        ("GB", Decimal("2"), "XYZ", None, "GB", Decimal("2"), "XYZ", False),
        ("GB", Decimal(), "", True, "GB", Decimal("1"), "ABC", True),
        (None, Decimal(), "", False, "FR", Decimal("1"), "ABC", False),
        ("GB", Decimal(), "ABC", True, "GB", Decimal("1"), "ABC", True),
        (None, None, None, False, "FR", Decimal("1"), "ABC", False),
    ],
)
@pytest.mark.django_db
def test_init_invoice_contact(
    country_code,
    hourly,
    vat_number,
    on_account,
    expect_country_code,
    expect_hourly,
    expect_vat_number,
    expect_on_account,
):
    contact = ContactFactory()
    country = None
    if country_code:
        country = CountryFactory(iso2_code=country_code)
    country_france = CountryFactory(iso2_code="FR")
    InvoiceContactFactory(
        contact=contact,
        country=country_france,
        hourly_rate=Decimal("1"),
        vat_number="ABC",
    )
    x = InvoiceContact.objects.init_invoice_contact(
        contact,
        country=country,
        hourly_rate=hourly,
        vat_number=vat_number,
        on_account=on_account,
    )
    assert contact == x.contact
    assert Country.objects.get(iso2_code=expect_country_code) == x.country
    assert expect_hourly == x.hourly_rate
    assert expect_on_account == x.on_account
    assert expect_vat_number == x.vat_number


@pytest.mark.django_db
def test_init_invoice_contact_create():
    contact = ContactFactory()
    country = CountryFactory(iso2_code="FR")
    x = InvoiceContact.objects.init_invoice_contact(
        contact, country, Decimal("12.50"), "ABC"
    )
    assert contact == x.contact
    assert country == x.country
    assert Decimal("12.50") == x.hourly_rate
    assert "ABC" == x.vat_number


@pytest.mark.django_db
def test_init_invoice_contact_create_contact_only():
    contact = ContactFactory()
    x = InvoiceContact.objects.init_invoice_contact(contact)
    assert contact == x.contact
    assert x.country is None
    assert x.hourly_rate is None
    assert "" == x.vat_number


@pytest.mark.django_db
def test_init_invoice_contact_country():
    contact = ContactFactory()
    country = CountryFactory(iso2_code="FR")
    x = InvoiceContact.objects.init_invoice_contact(contact, country=country)
    assert contact == x.contact
    assert country == x.country
    assert x.hourly_rate is None
    assert "" == x.vat_number


@pytest.mark.django_db
def test_init_invoice_contact_hourly_rate():
    contact = ContactFactory()
    x = InvoiceContact.objects.init_invoice_contact(
        contact, hourly_rate=Decimal("12.50")
    )
    assert contact == x.contact
    assert x.country is None
    assert Decimal("12.50") == x.hourly_rate
    assert "" == x.vat_number


@pytest.mark.parametrize(
    "init_on_account,on_account,expect",
    [
        (True, True, True),
        (True, False, True),
        (False, False, False),
        (False, True, True),
    ],
)
@pytest.mark.django_db
def test_init_invoice_on_account(init_on_account, on_account, expect):
    """Initialise the on-account status for the contact.

    .. note:: The on account flag (``on_account``) can only be switched on.
              It can't be switched off.
              If we are posting to an accounts package e.g. Xero, then we
              should always post on-account if we ever posted on-account.

    """
    contact = ContactFactory()
    InvoiceContact.objects.init_invoice_contact(
        contact,
        on_account=init_on_account,
    )
    x = InvoiceContact.objects.init_invoice_contact(
        contact,
        on_account=on_account,
    )
    x.refresh_from_db()
    assert expect == x.on_account


@pytest.mark.django_db
def test_init_invoice_contact_vat_number():
    contact = ContactFactory()
    x = InvoiceContact.objects.init_invoice_contact(contact, vat_number="ABC")
    assert contact == x.contact
    assert x.country is None
    assert x.hourly_rate is None
    assert "ABC" == x.vat_number


@pytest.mark.parametrize(
    "country_code,vat_number,expect",
    [("AA", "123", True), (None, "123", False), ("AA", None, False)],
)
@pytest.mark.django_db
def test_is_european_union_vat_transaction(country_code, vat_number, expect):
    contact = ContactFactory()
    vat_settings = VatSettings.load()
    vat_settings.save()
    country = None
    if country_code:
        country = CountryFactory(iso2_code=country_code)
        vat_settings.european_union_countries.add(country)
    x = InvoiceContact.objects.init_invoice_contact(
        contact, country=country, vat_number=vat_number
    )
    assert x.is_european_union_vat_transaction() is expect
