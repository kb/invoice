# -*- encoding: utf-8 -*-
import pytest

from datetime import date, datetime, timedelta, time
from dateutil.relativedelta import relativedelta
from django.utils import timezone
from freezegun import freeze_time
from report.pdf import PDFReport, NumberedCanvas
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.platypus import Paragraph, Spacer, Table, Image

from contact.models import Contact
from contact.tests.factories import ContactFactory
from crm.tests.factories import TicketFactory
from invoice.models import TimeRecord, TimeAnalysis
from invoice.pdf import TicketTimeReport
from invoice.tests.factories import TimeRecordFactory
from login.tests.factories import TEST_PASSWORD, UserFactory


@pytest.mark.django_db
def test_report():
    user = UserFactory()
    contact = ContactFactory(user=user)
    ticket_1 = TicketFactory(contact=contact, title="Orange")
    ticket_2 = TicketFactory(contact=contact, title="Apple")
    TimeRecordFactory(
        ticket=ticket_1,
        date_started=date(2017, 1, 16),
        start_time=time(11, 0),
        end_time=time(11, 30),
        user=user,
    )
    TimeRecordFactory(
        ticket=ticket_1,
        date_started=date(2017, 1, 16),
        start_time=time(11, 0),
        end_time=time(11, 10),
        billable=False,
        user=user,
    )
    TimeRecordFactory(
        ticket=ticket_1,
        date_started=date(2017, 1, 14),
        start_time=time(11, 0),
        end_time=time(11, 10),
        user=user,
    )
    TimeRecordFactory(
        ticket=ticket_2,
        date_started=date(2017, 1, 14),
        start_time=time(11, 15),
        end_time=time(11, 30),
        user=user,
    )
    TimeRecordFactory(
        ticket=ticket_2,
        date_started=date(2017, 1, 14),
        start_time=time(11, 15),
        end_time=None,
        user=user,
    )
    with freeze_time(date(2017, 1, 17)):
        data = TicketTimeReport().data(contact.user.username)
    assert {
        "data": {
            date(2017, 1, 14): {
                "tickets": {
                    ticket_1.pk: {
                        "title": "Orange",
                        "total": TimeAnalysis(charge=10.0, pending=10.0),
                    },
                    ticket_2.pk: {
                        "title": "Apple",
                        "total": TimeAnalysis(charge=15.0, pending=15.0),
                    },
                },
                "total": TimeAnalysis(charge=25.0, pending=25.0),
            },
            date(2017, 1, 16): {
                "tickets": {
                    ticket_1.pk: {
                        "title": "Orange",
                        "total": TimeAnalysis(
                            charge=30.0, non_charge=10.0, pending=30.0
                        ),
                    }
                },
                "total": TimeAnalysis(
                    charge=30.0, non_charge=10.0, pending=30.0
                ),
            },
        },
        "total": TimeAnalysis(charge=55.0, non_charge=10.0, pending=55.0),
    } == data
    assert [date(2017, 1, 14), date(2017, 1, 16)] == list(data["data"].keys())
