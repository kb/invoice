# -*- encoding: utf-8 -*-
import pytest
import pytz

from datetime import date, datetime, time
from django.urls import reverse
from django.utils import timezone

from contact.tests.factories import ContactFactory
from crm.tests.factories import (
    FundedFactory,
    KanbanCardFactory,
    KanbanColumnFactory,
    KanbanLaneFactory,
    TicketFactory,
)
from dateutil.relativedelta import relativedelta
from invoice.models import InvoiceError, TimeRecord
from invoice.tests.factories import (
    InvoiceLineFactory,
    TimeRecordFactory,
    QuickTimeRecordFactory,
)
from login.tests.factories import UserFactory
from sales_order.models import SalesOrder
from sales_order.tests.factories import SalesOrderFactory
from sales_order_ticket.tests.factories import SalesOrderTicketFactory
from search.tests.helper import check_search_methods


@pytest.mark.django_db
def test_get_update_url():
    x = TimeRecordFactory()
    assert reverse("invoice.time.update", args=[x.pk]) == x.get_update_url()


@pytest.mark.django_db
def test_is_legacy_fixed_price():
    # legacy 'fixed_price' (before December 2023)
    time_record_1 = TimeRecordFactory(
        ticket=TicketFactory(fixed_price=True), date_started=date(2023, 10, 31)
    )
    assert time_record_1.is_legacy_fixed_price() is True
    # ignore 'fixed_price' on the ticket (after May 2024)
    time_record_2 = TimeRecordFactory(
        ticket=TicketFactory(fixed_price=True), date_started=date(2024, 6, 1)
    )
    assert time_record_2.is_legacy_fixed_price() is False
    # ignore 'fixed_price' on the ticket (after May 2024)
    time_record_3 = TimeRecordFactory(
        ticket=TicketFactory(fixed_price=True), date_started=date(2024, 7, 1)
    )
    assert time_record_3.is_legacy_fixed_price() is False
    # ignore 'fixed_price' on the ticket (after May 2024)
    time_record_4 = TimeRecordFactory(
        ticket=TicketFactory(fixed_price=True), date_started=date(2025, 1, 1)
    )
    assert time_record_4.is_legacy_fixed_price() is False


@pytest.mark.django_db
def test_is_today():
    obj = TimeRecordFactory(date_started=timezone.now().date())
    assert obj.is_today() is True


@pytest.mark.django_db
def test_is_today_not():
    d = timezone.now().date() + relativedelta(days=-7)
    obj = TimeRecordFactory(date_started=d)
    assert obj.is_today() is False


@pytest.mark.django_db
def test_modified_today():
    assert TimeRecordFactory().modified_today is True


@pytest.mark.django_db
def test_modified_today_23_hours():
    created = timezone.now() - relativedelta(hours=23)
    obj = TimeRecordFactory()
    obj.created = created
    obj.save()
    assert obj.modified_today is True


@pytest.mark.django_db
def test_modified_today_25_hours():
    created = timezone.now() - relativedelta(hours=25)
    obj = TimeRecordFactory()
    obj.created = created
    obj.save()
    assert obj.modified_today is False


@pytest.mark.django_db
def test_modified_today_yesterday():
    created = timezone.now() - relativedelta(days=1)
    obj = TimeRecordFactory()
    obj.created = created
    obj.save()
    assert obj.modified_today is False


@pytest.mark.django_db
def test_running():
    user = UserFactory()
    TimeRecordFactory(title="t1", user=user, end_time=None)
    TimeRecordFactory(title="t2", user=user, end_time=time(11, 0))
    d = timezone.now().date() + relativedelta(days=-7)
    TimeRecordFactory(title="t3", date_started=d, user=user, end_time=None)
    TimeRecordFactory(title="t4", user=UserFactory(), end_time=None)
    qs = TimeRecord.objects.running(user).order_by("title")
    assert ["t1", "t3"] == [obj.title for obj in qs]


@pytest.mark.django_db
def test_running_today():
    user = UserFactory()
    TimeRecordFactory(title="t1", user=user, end_time=None)
    TimeRecordFactory(title="t2", user=user, end_time=time(11, 0))
    d = timezone.now().date() + relativedelta(days=7)
    TimeRecordFactory(title="t3", date_started=d, user=user, end_time=None)
    TimeRecordFactory(title="t4", user=UserFactory(), end_time=None)
    qs = TimeRecord.objects.running_today(user).order_by("title")
    assert ["t1"] == [obj.title for obj in qs]


@pytest.mark.django_db
def test_search_methods():
    time_record = TimeRecordFactory()
    check_search_methods(time_record)


@pytest.mark.django_db
def test_start():
    user = UserFactory()
    quick = QuickTimeRecordFactory(user=user)
    ticket = TicketFactory()
    time_record = TimeRecord.objects.start(ticket, quick)
    assert quick.time_code == time_record.time_code
    assert ticket == time_record.ticket
    assert time_record.end_time is None
    assert time_record.start_time is not None
    assert quick.user == time_record.user


@pytest.mark.django_db
def test_start_and_stop():
    user = UserFactory()
    running = TimeRecordFactory(user=user, end_time=None)
    assert running.end_time is None
    quick = QuickTimeRecordFactory(user=user)
    ticket = TicketFactory()
    time_record = TimeRecord.objects.start(ticket, quick)
    assert quick.time_code == time_record.time_code
    assert quick.description == time_record.title
    assert time_record.billable is False
    assert time_record.end_time is None
    assert time_record.start_time is not None
    assert user == time_record.user
    running.refresh_from_db()
    assert running.end_time == time_record.start_time


@pytest.mark.django_db
def test_start_and_stop_billable():
    quick = QuickTimeRecordFactory(chargeable=True)
    ticket = TicketFactory()
    time_record = TimeRecord.objects.start(ticket, quick)
    assert time_record.billable is True


@pytest.mark.django_db
def test_start_and_stop_too_many():
    user = UserFactory()
    TimeRecordFactory(user=user, end_time=None)
    TimeRecordFactory(user=user, end_time=None)
    quick = QuickTimeRecordFactory(user=user)
    ticket = TicketFactory()
    with pytest.raises(InvoiceError) as e:
        TimeRecord.objects.start(ticket, quick)
    assert "Cannot start a time record when 2 are already" in str(e.value)


@pytest.mark.django_db
def test_stop():
    obj = TimeRecordFactory(end_time=None)
    assert obj.end_time is None
    obj.stop()
    assert obj.end_time is not None


@pytest.mark.django_db
def test_stop_end_time():
    obj = TimeRecordFactory(end_time=None)
    assert obj.end_time is None
    end_time = datetime(2014, 10, 1, 18, 13, 32, tzinfo=pytz.utc)
    obj.stop(end_time)
    assert end_time == obj.end_time


@pytest.mark.django_db
def test_stop_already_stopped():
    end_time = datetime(2014, 10, 1, 18, 13, 32, tzinfo=pytz.utc)
    obj = TimeRecordFactory(end_time=end_time)
    with pytest.raises(InvoiceError) as e:
        obj.stop()
    assert "has already been stopped" in str(e.value)


@pytest.mark.django_db
def test_str():
    time_record = TimeRecordFactory()
    str(time_record)


@pytest.mark.django_db
def test_ticket_totals():
    start_date = date(day=3, month=2, year=2017)
    ticket = TicketFactory()
    """
    This loop generates the following timerecords:
    Date       Start Tm End Tm   Delta   Billable Invoice Line
    2017-02-03 01:01:00 01:02:00 0:01:00 False    None
    2017-02-03 02:02:00 02:05:00 0:03:00 True     None
    2017-02-03 03:03:00 03:10:00 0:07:00 False    None
    2017-02-03 04:04:00 04:17:00 0:13:00 True     None
    2017-02-03 05:05:00 05:26:00 0:21:00 False    None
    2017-02-03 06:06:00 06:37:00 0:31:00 True     0 1.00 description_0 @0.00
    2017-02-03 08:00:00 None     0:00:00 True     None
    """
    last_time_record = None
    for t in range(1, 7):
        start_time = time(hour=t, minute=t)
        end_time = time(hour=t, minute=t * t + 1)
        tr = TimeRecordFactory(
            ticket=ticket,
            date_started=start_date,
            start_time=start_time,
            end_time=end_time,
            billable=t % 2 == 0,
        )
        last_time_record = tr
    # make the last time record invoiced
    last_time_record.invoice_line = InvoiceLineFactory()
    last_time_record.save()
    # open time record - should be ignored
    TimeRecordFactory(
        ticket=ticket,
        date_started=start_date,
        start_time=time(hour=8),
        end_time=None,
        billable=True,
    )
    totals = TimeRecord.objects.ticket_totals(ticket)
    assert totals.total_fmt == "01:16"
    assert totals.non_charge_fmt == "00:29"
    assert totals.pending_fmt == "00:16"
    assert totals.charge_fmt == "00:47"
    assert totals.invoiced_fmt == "00:31"


@pytest.mark.django_db
def test_tickets():
    """List of tickets where time was recorded for a user."""
    user = UserFactory()
    today = date.today()
    # first day of the month
    from_date = today + relativedelta(day=1)
    # last day of the month
    to_date = today + relativedelta(months=+1, day=1, days=-1)
    # last month
    last_month = today + relativedelta(months=-1)
    # next month
    next_month = today + relativedelta(months=+1)
    TimeRecordFactory(
        ticket=TicketFactory(title="t0"), user=user, date_started=last_month
    )
    TimeRecordFactory(
        ticket=TicketFactory(title="t1"), user=user, date_started=from_date
    )
    TimeRecordFactory(
        ticket=TicketFactory(title="t2"), user=user, date_started=today
    )
    TimeRecordFactory(ticket=TicketFactory(title="t3"), date_started=to_date)
    TimeRecordFactory(
        ticket=TicketFactory(title="t4"),
        user=user,
        date_started=to_date,
        end_time=None,
    )
    TimeRecordFactory(
        ticket=TicketFactory(title="t5"), user=user, date_started=to_date
    )
    TimeRecordFactory(
        ticket=TicketFactory(title="t6"), user=user, date_started=next_month
    )
    qs = TimeRecord.objects.tickets(from_date, to_date, user)
    assert ["t1", "t2", "t5"] == [x.title for x in qs]


@pytest.mark.django_db
@pytest.mark.parametrize(
    "exclude_fixed_price,expect",
    [
        (False, ["t1", "t3", "t5"]),
        (None, ["t1", "t3", "t5"]),
        (True, ["t3", "t5"]),
    ],
)
def test_to_invoice(exclude_fixed_price, expect):
    """Time records waiting to be invoiced."""
    TimeRecordFactory(
        title="t1",
        billable=True,
        date_started=date(2023, 10, 1),
        ticket=TicketFactory(contact=ContactFactory(), fixed_price=True),
    )
    TimeRecordFactory(
        title="t2",
        billable=False,
        date_started=date(2023, 10, 1),
        ticket=TicketFactory(contact=ContactFactory()),
    )
    TimeRecordFactory(
        title="t3",
        billable=True,
        date_started=date(2023, 10, 1),
        ticket=TicketFactory(contact=ContactFactory()),
    )
    TimeRecordFactory(
        title="t4",
        billable=True,
        invoice_line=InvoiceLineFactory(),
        date_started=date(2023, 10, 1),
        ticket=TicketFactory(contact=ContactFactory()),
    )
    # stop checking 'fixed_price' on the ticket after May 2024
    TimeRecordFactory(
        title="t5",
        billable=True,
        date_started=date(2024, 7, 1),
        ticket=TicketFactory(contact=ContactFactory(), fixed_price=True),
    )
    assert expect == [
        x.title
        for x in TimeRecord.objects.to_invoice(
            exclude_fixed_price=exclude_fixed_price
        ).order_by("title")
    ]


@pytest.mark.django_db
def test_to_invoice_null_date_time():
    TimeRecordFactory(
        title="t1",
        ticket=TicketFactory(contact=ContactFactory()),
        date_started=date(2012, 7, 1),
        start_time=time(11, 0),
        end_time=None,
    )
    TimeRecordFactory(
        title="t2",
        ticket=TicketFactory(contact=ContactFactory()),
        date_started=date(2012, 7, 1),
        start_time=time(11, 0),
        end_time=time(11, 10),
    )
    # Cannot check 'start_time' or 'date_started':
    # null value in column "start_time" violates not-null constraint
    # null value in column "date_started" violates not-null constraint
    qs = TimeRecord.objects.to_invoice()
    assert ["t2"] == [x.title for x in qs]


@pytest.mark.django_db
@pytest.mark.parametrize(
    "hours_type_fixed_price",
    [
        SalesOrder.FIXED_PRICE,
        SalesOrder.SUPPORT_ANNUAL,
        SalesOrder.SUPPORT_MONTHLY,
    ],
)
def test_to_invoice_sales_order(hours_type_fixed_price):
    """Time records waiting to be invoiced (with and without a sales order).

    The following sales order ``hours_type`` values are fixed price
    i.e. individual time records will not be invoiced::

      SalesOrder.FIXED_PRICE
      SalesOrder.SUPPORT_ANNUAL
      SalesOrder.SUPPORT_MONTHLY

    """
    contact = ContactFactory(user=UserFactory(username="patrick"))
    sales_order_1 = SalesOrderFactory(
        contact=contact, hours_type=SalesOrder.HOURS_TOTAL
    )
    sales_order_2 = SalesOrderFactory(
        contact=contact, hours_type=hours_type_fixed_price
    )
    ticket_1 = TicketFactory(contact=contact)
    ticket_2 = TicketFactory(contact=contact)
    SalesOrderTicketFactory(sales_order=sales_order_1, ticket=ticket_1)
    SalesOrderTicketFactory(sales_order=sales_order_2, ticket=ticket_2)
    TimeRecordFactory(
        title="t1",
        billable=True,
        ticket=ticket_1,
    )
    TimeRecordFactory(
        title="t2",
        billable=True,
        ticket=ticket_2,
    )
    assert ["t1"] == [
        x.title for x in TimeRecord.objects.to_invoice(exclude_fixed_price=True)
    ]


@pytest.mark.django_db
def test_to_invoice_contact():
    """Time records waiting to be invoiced for a contact and before a date."""
    contact = ContactFactory()
    d = date(2012, 7, 1)
    #
    TimeRecordFactory(
        title="t1", ticket=TicketFactory(contact=contact), date_started=d
    )
    # exclude records created after the invoice date
    TimeRecordFactory(
        title="t2",
        ticket=TicketFactory(contact=contact),
        date_started=date(2012, 8, 1),
    )
    # exclude records for another contact
    TimeRecordFactory(
        title="t3",
        ticket=TicketFactory(contact=ContactFactory()),
        date_started=d,
    )
    # exclude records which have already been invoiced
    TimeRecordFactory(
        title="t4",
        ticket=TicketFactory(contact=contact),
        date_started=d,
        invoice_line=InvoiceLineFactory(),
    )
    # exclude records which have a fixed price ticket
    TimeRecordFactory(
        title="t5",
        ticket=TicketFactory(contact=contact, fixed_price=True),
        date_started=d,
    )
    #
    TimeRecordFactory(
        title="t6", ticket=TicketFactory(contact=contact), date_started=d
    )
    # fixed price sales order
    ticket = TicketFactory(contact=contact)
    sales_order_fixed = SalesOrderFactory(
        contact=contact, hours_type=SalesOrder.FIXED_PRICE
    )
    SalesOrderTicketFactory(sales_order=sales_order_fixed, ticket=ticket)
    TimeRecordFactory(title="t7", ticket=ticket, date_started=d)
    # sales order (not fixed price)
    ticket = TicketFactory(contact=contact)
    sales_order_hours = SalesOrderFactory(
        contact=contact, hours_type=SalesOrder.HOURS_TOTAL
    )
    SalesOrderTicketFactory(sales_order=sales_order_hours, ticket=ticket)
    TimeRecordFactory(title="t8", ticket=ticket, date_started=d)
    # not funded - so do not include
    funded = FundedFactory(free_of_charge=True)
    ticket = TicketFactory(contact=contact, funded=funded)
    TimeRecordFactory(title="t9", ticket=ticket, date_started=d)
    # kanban lane - not funded - so do not include
    ticket = TicketFactory(contact=contact)
    lane = KanbanLaneFactory(funded=False)
    column = KanbanColumnFactory(lane=lane)
    KanbanCardFactory(column=column, ticket=ticket)
    TimeRecordFactory(title="t10", ticket=ticket, date_started=d)
    # funded - so include
    funded = FundedFactory(free_of_charge=False)
    ticket = TicketFactory(contact=contact, funded=funded)
    TimeRecordFactory(title="t11", ticket=ticket, date_started=d)
    # kanban lane - funded - so include
    ticket = TicketFactory(contact=contact)
    lane = KanbanLaneFactory(funded=True)
    column = KanbanColumnFactory(lane=lane)
    KanbanCardFactory(column=column, ticket=ticket)
    TimeRecordFactory(title="t12", ticket=ticket, date_started=d)
    # test
    qs = TimeRecord.objects.to_invoice_contact(contact, date(2012, 7, 31))
    assert ["t1", "t6", "t8", "t11", "t12"] == [x.title for x in qs]


@pytest.mark.django_db
def test_to_invoice_contact_fixed_time_before_november_2023():
    contact = ContactFactory()
    # fixed price (exclude)
    ticket = TicketFactory(contact=contact)
    sales_order_fixed = SalesOrderFactory(
        contact=contact, hours_type=SalesOrder.FIXED_PRICE
    )
    SalesOrderTicketFactory(sales_order=sales_order_fixed, ticket=ticket)
    TimeRecordFactory(title="t1", ticket=ticket, date_started=date(2024, 1, 1))
    # exclude fixed price time records before June 2024
    TimeRecordFactory(
        title="t2",
        ticket=TicketFactory(contact=contact, fixed_price=True),
        date_started=date(2024, 5, 15),
    )
    # ignore fixed price time records after May 2024
    TimeRecordFactory(
        title="t3",
        ticket=TicketFactory(contact=contact, fixed_price=True),
        date_started=date(2024, 7, 15),
    )
    # test
    qs = TimeRecord.objects.to_invoice_contact(contact, date(2025, 1, 31))
    assert ["t3"] == [x.title for x in qs]


@pytest.mark.django_db
def test_user_can_edit():
    time_record = TimeRecordFactory()
    assert time_record.user_can_edit is True


@pytest.mark.django_db
def test_user_can_edit_invoice_line():
    invoice_line = InvoiceLineFactory()
    time_record = TimeRecordFactory(invoice_line=invoice_line)
    assert time_record.user_can_edit is False
