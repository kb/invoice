# -*- encoding: utf-8 -*-
import pytest

from django.core.management import call_command


@pytest.mark.django_db
def test_export_to_r():
    """Also see 'test_export_to_r' in 'invoice/tests/test_service.py'."""
    call_command("export_to_r")


@pytest.mark.django_db
def test_init_app():
    call_command("init_app_invoice")


@pytest.mark.django_db
def test_report_hours_per_week():
    call_command("report_hours_per_week")
