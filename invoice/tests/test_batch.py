# -*- encoding: utf-8 -*-
import pytest

from datetime import date
from decimal import Decimal
from django.db import IntegrityError

from finance.models import Currency, VatCode
from invoice.models import Batch, BatchInvoice, Invoice
from invoice.tests.factories import (
    BatchFactory,
    BatchInvoiceFactory,
    InvoiceContactFactory,
    InvoiceFactory,
    InvoiceLineFactory,
    PaymentProcessorFactory,
)


@pytest.mark.django_db
def test_batch_number():
    batch = BatchFactory()
    assert "{:06d}".format(batch.pk) == batch.batch_number


@pytest.mark.django_db
def test_create_batch():
    batch_date = date(2018, 2, 22)
    currency = Currency.objects.get(slug="GBP")
    payment_processor = PaymentProcessorFactory()
    invoice_1 = InvoiceFactory(
        invoice_date=batch_date,
        currency=currency,
        upfront_payment=True,
        upfront_payment_processor=payment_processor,
    )
    invoice_2 = InvoiceFactory(
        invoice_date=batch_date,
        currency=currency,
        upfront_payment=True,
        upfront_payment_processor=payment_processor,
    )
    batch = Batch.objects.create_batch(
        batch_date, currency, Decimal(), payment_processor
    )
    assert set([invoice_1.pk, invoice_2.pk]) == set(
        [x.invoice.pk for x in batch.invoices()]
    )


@pytest.mark.django_db
def test_create_batch_exchange_rate():
    batch_date = date(2018, 2, 22)
    currency = Currency.objects.get(slug="EUR")
    exchange_rate = Decimal("0.987")
    payment_processor = PaymentProcessorFactory()
    InvoiceFactory(
        invoice_date=batch_date,
        currency=currency,
        upfront_payment=True,
        upfront_payment_processor=payment_processor,
    )
    invoice_2 = InvoiceFactory(
        invoice_date=batch_date,
        currency=currency,
        exchange_rate=exchange_rate,
        upfront_payment=True,
        upfront_payment_processor=payment_processor,
    )
    batch = Batch.objects.create_batch(
        batch_date, currency, exchange_rate, payment_processor
    )
    assert [invoice_2.pk] == [x.invoice.pk for x in batch.invoices()]
    assert [] == [x.pk for x in Invoice.objects.on_account_invoices(batch_date)]


@pytest.mark.django_db
def test_create_batch_on_account():
    """

    .. note:: ``invoice_2`` is an ``on_account`` invoice so must not be
              included in a batch:
              https://www.kbsoftware.co.uk/crm/ticket/4924/

    """
    batch_date = date(2018, 2, 22)
    currency = Currency.objects.get(slug="GBP")
    payment_processor = PaymentProcessorFactory()
    invoice_1 = InvoiceFactory(
        invoice_date=batch_date,
        currency=currency,
        upfront_payment=True,
        upfront_payment_processor=payment_processor,
    )
    invoice_2 = InvoiceFactory(
        invoice_date=batch_date,
        currency=currency,
        upfront_payment=True,
        upfront_payment_processor=payment_processor,
    )
    InvoiceContactFactory(contact=invoice_2.contact, on_account=True)
    invoice_3 = InvoiceFactory(
        invoice_date=batch_date,
        currency=currency,
        upfront_payment=True,
        upfront_payment_processor=payment_processor,
    )
    batch = Batch.objects.create_batch(
        batch_date, currency, Decimal(), payment_processor
    )
    assert set([invoice_1.pk, invoice_3.pk]) == set(
        [x.invoice.pk for x in batch.invoices()]
    )
    assert [invoice_2.pk] == [
        x.pk for x in Invoice.objects.on_account_invoices(batch_date)
    ]


@pytest.mark.django_db
def test_create_batch_no_invoices():
    batch_date = date(2018, 2, 22)
    currency = Currency.objects.get(slug="GBP")
    payment_processor = PaymentProcessorFactory()
    batch = Batch.objects.create_batch(
        batch_date, currency, None, payment_processor
    )
    assert batch is None
    assert 0 == BatchInvoice.objects.count()


@pytest.mark.django_db
def test_duplicate():
    batch_date = date(2018, 2, 22)
    currency = Currency.objects.get(slug="GBP")
    exchange_rate = Decimal()
    payment_processor = PaymentProcessorFactory()
    BatchFactory(
        batch_date=batch_date,
        currency=currency,
        exchange_rate=exchange_rate,
        payment_processor=payment_processor,
    )
    # different payment processor - should create without a problem
    BatchFactory(
        batch_date=batch_date,
        currency=currency,
        exchange_rate=exchange_rate,
        payment_processor=PaymentProcessorFactory(),
    )
    with pytest.raises(IntegrityError):
        BatchFactory(
            batch_date=batch_date,
            currency=currency,
            exchange_rate=exchange_rate,
            payment_processor=payment_processor,
        )


@pytest.mark.django_db
def test_net_and_vat():
    vat_code_standard = VatCode.objects.get(slug=VatCode.STANDARD)
    vat_code_zero = VatCode.objects.get(slug=VatCode.EXEMPT)
    batch = BatchFactory()
    invoice_1 = InvoiceFactory(invoice_date=batch.batch_date)
    InvoiceLineFactory(
        invoice=invoice_1,
        net=Decimal(100),
        vat=Decimal(20),
        vat_code=vat_code_standard,
    )
    invoice_2 = InvoiceFactory(invoice_date=batch.batch_date)
    InvoiceLineFactory(
        invoice=invoice_2,
        net=Decimal(200),
        vat=Decimal(40),
        vat_code=vat_code_standard,
    )
    InvoiceLineFactory(
        invoice=invoice_2,
        net=Decimal(100),
        vat=Decimal(20),
        vat_code=vat_code_zero,
    )
    BatchInvoiceFactory(batch=batch, invoice=invoice_1)
    BatchInvoiceFactory(batch=batch, invoice=invoice_2)
    assert (Decimal("400"), Decimal("80")) == batch.net_and_vat()


@pytest.mark.django_db
def test_str():
    currency = Currency.objects.get(slug="GBP")
    payment_processor = PaymentProcessorFactory(description="PayPal")
    batch = BatchFactory(
        batch_date=date(2018, 2, 22),
        currency=currency,
        payment_processor=payment_processor,
    )
    assert (
        "Invoice batch {} for 22/02/2018 currency "
        "'GBP' payment processor 'PayPal'".format(batch.pk)
    ) == str(batch)


@pytest.mark.django_db
def test_str_exchange_rate():
    currency = Currency.objects.get(slug="EUR")
    payment_processor = PaymentProcessorFactory(description="Stripe")
    batch = BatchFactory(
        batch_date=date(2018, 2, 22),
        currency=currency,
        exchange_rate=Decimal("0.834"),
        payment_processor=payment_processor,
    )
    assert (
        "Invoice batch {} for 22/02/2018 currency 'EUR' (0.834) "
        "payment processor 'Stripe'".format(batch.pk)
    ) == str(batch)


@pytest.mark.django_db
def test_vat_analysis():
    vat_code_standard = VatCode.objects.get(slug=VatCode.STANDARD)
    vat_code_zero = VatCode.objects.get(slug=VatCode.EXEMPT)
    batch = BatchFactory()
    invoice_1 = InvoiceFactory(invoice_date=batch.batch_date)
    InvoiceLineFactory(
        invoice=invoice_1,
        net=Decimal(100),
        vat=Decimal(20),
        vat_code=vat_code_standard,
    )
    invoice_2 = InvoiceFactory(invoice_date=batch.batch_date)
    InvoiceLineFactory(
        invoice=invoice_2,
        net=Decimal(200),
        vat=Decimal(40),
        vat_code=vat_code_standard,
    )
    InvoiceLineFactory(
        invoice=invoice_2,
        net=Decimal(100),
        vat=Decimal(0),
        vat_code=vat_code_zero,
    )
    BatchInvoiceFactory(batch=batch, invoice=invoice_1)
    BatchInvoiceFactory(batch=batch, invoice=invoice_2)
    assert {
        "S": {"count": 2, "net": Decimal("300"), "vat": Decimal("60")},
        "E": {"count": 1, "net": Decimal("100"), "vat": Decimal("0")},
    } == batch.vat_analysis()
