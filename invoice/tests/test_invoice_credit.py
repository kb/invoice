# -*- encoding: utf-8 -*-
import pytest

from django.db import IntegrityError

from invoice.models import InvoiceCredit
from login.tests.factories import UserFactory
from .factories import InvoiceCreditFactory, InvoiceFactory


@pytest.mark.django_db
def test_create_invoice_credit():
    invoice = InvoiceFactory()
    credit_note = InvoiceFactory(is_credit=True)
    invoice_credit = InvoiceCredit.objects.create_invoice_credit(
        invoice, credit_note
    )
    assert invoice == invoice_credit.invoice
    assert credit_note == invoice_credit.credit_note


@pytest.mark.django_db
def test_duplicate():
    invoice = InvoiceFactory()
    credit_note = InvoiceFactory(is_credit=True)
    InvoiceCreditFactory(invoice=invoice, credit_note=credit_note)
    with pytest.raises(IntegrityError):
        InvoiceCredit(invoice=invoice, credit_note=credit_note).save()


@pytest.mark.django_db
def test_init_invoice_credit():
    invoice = InvoiceFactory()
    credit_note = InvoiceFactory(is_credit=True)
    invoice_credit = InvoiceCredit.objects.init_invoice_credit(
        invoice, credit_note
    )
    assert invoice == invoice_credit.invoice
    assert credit_note == invoice_credit.credit_note


@pytest.mark.django_db
def test_init_invoice_credit_undelete():
    invoice = InvoiceFactory()
    credit_note = InvoiceFactory()
    invoice_credit = InvoiceCreditFactory(
        invoice=invoice, credit_note=credit_note
    )
    assert invoice_credit.is_deleted is False
    invoice_credit.set_deleted(UserFactory())
    assert invoice_credit.is_deleted is True
    invoice_credit = InvoiceCredit.objects.init_invoice_credit(
        invoice, credit_note
    )
    assert 1 == InvoiceCredit.objects.count()
    assert invoice == invoice_credit.invoice
    assert credit_note == invoice_credit.credit_note
    assert invoice_credit.is_deleted is False


@pytest.mark.django_db
def test_init_invoice_two_credit_notes():
    invoice = InvoiceFactory()
    credit_note_1 = InvoiceFactory()
    credit_note_2 = InvoiceFactory()
    InvoiceCredit.objects.init_invoice_credit(invoice, credit_note_1)
    InvoiceCredit.objects.init_invoice_credit(invoice, credit_note_2)
    assert 2 == InvoiceCredit.objects.count()
    InvoiceCredit.objects.get(invoice=invoice, credit_note=credit_note_1)
    InvoiceCredit.objects.get(invoice=invoice, credit_note=credit_note_2)


@pytest.mark.django_db
def test_str():
    assert "Credit 000099 for Invoice 000033" == str(
        InvoiceCreditFactory(
            invoice=InvoiceFactory(number=33),
            credit_note=InvoiceFactory(number=99, is_credit=True),
        )
    )
