# -*- encoding: utf-8 -*-
import pytest

from invoice.models import QuickTimeRecord
from invoice.tests.factories import QuickTimeRecordFactory
from login.tests.factories import UserFactory


@pytest.mark.django_db
def test_quick_chargeable():
    user = UserFactory()
    QuickTimeRecordFactory(user=user, description="a", chargeable=False)
    QuickTimeRecordFactory(user=user, description="b", chargeable=True)
    QuickTimeRecordFactory(user=user, description="c", chargeable=False)
    QuickTimeRecordFactory(user=user, description="d", chargeable=True)
    QuickTimeRecordFactory(user=user, description="e", deleted=True)
    qs = QuickTimeRecord.objects.quick_chargeable(user)
    assert ["b", "d"] == [x.description for x in qs]


@pytest.mark.django_db
def test_quick_non_chargeable():
    user = UserFactory()
    QuickTimeRecordFactory(user=user, description="a", chargeable=False)
    QuickTimeRecordFactory(user=user, description="b", chargeable=True)
    QuickTimeRecordFactory(user=user, description="c", chargeable=False)
    QuickTimeRecordFactory(user=user, description="d", chargeable=True)
    QuickTimeRecordFactory(user=user, description="e", deleted=True)
    qs = QuickTimeRecord.objects.quick_non_chargeable(user)
    assert ["a", "c"] == [x.description for x in qs]


@pytest.mark.django_db
def test_str():
    quick_time_record = QuickTimeRecordFactory(description="Apple")
    assert "Apple" == str(quick_time_record)
