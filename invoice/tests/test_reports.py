# -*- encoding: utf-8 -*-
import pytest

from datetime import date, time

from contact.tests.factories import ContactFactory
from crm.tests.factories import TicketFactory
from invoice.models import TimeAnalysis
from invoice.reports import time_summary_by_user
from invoice.tests.factories import TimeRecordFactory
from login.tests.factories import UserFactory


@pytest.mark.django_db
def test_time_summary_by_user():
    user = UserFactory(username="green", first_name="P", last_name="Kimber")
    contact = ContactFactory(user=user)
    TimeRecordFactory(
        ticket=TicketFactory(contact=contact),
        date_started=date(2017, 1, 16),
        start_time=time(11, 0),
        end_time=time(11, 30),
        user=user,
    )
    TimeRecordFactory(
        billable=False,
        ticket=TicketFactory(contact=contact),
        date_started=date(2016, 12, 1),
        start_time=time(11, 0),
        end_time=time(11, 15),
        user=user,
    )
    TimeRecordFactory(
        ticket=TicketFactory(contact=contact, fixed_price=True),
        date_started=date(2016, 11, 30),
        start_time=time(11, 0),
        end_time=time(11, 10),
        user=user,
    )
    data = time_summary_by_user(date(2017, 3, 17))
    assert {
        "green": {
            "2015-03": {
                "label": "Mar",
                "month": 3,
                "year": 2015,
                "analysis": TimeAnalysis(),
            },
            "2015-04": {
                "label": "Apr",
                "month": 4,
                "year": 2015,
                "analysis": TimeAnalysis(),
            },
            "2015-05": {
                "label": "May",
                "month": 5,
                "year": 2015,
                "analysis": TimeAnalysis(),
            },
            "2015-06": {
                "label": "Jun",
                "month": 6,
                "year": 2015,
                "analysis": TimeAnalysis(),
            },
            "2015-07": {
                "label": "Jul",
                "month": 7,
                "year": 2015,
                "analysis": TimeAnalysis(),
            },
            "2015-08": {
                "label": "Aug",
                "month": 8,
                "year": 2015,
                "analysis": TimeAnalysis(),
            },
            "2015-09": {
                "label": "Sep",
                "month": 9,
                "year": 2015,
                "analysis": TimeAnalysis(),
            },
            "2015-10": {
                "label": "Oct",
                "month": 10,
                "year": 2015,
                "analysis": TimeAnalysis(),
            },
            "2015-11": {
                "label": "Nov",
                "month": 11,
                "year": 2015,
                "analysis": TimeAnalysis(),
            },
            "2015-12": {
                "label": "Dec",
                "month": 12,
                "year": 2015,
                "analysis": TimeAnalysis(),
            },
            "2016-01": {
                "label": "Jan",
                "month": 1,
                "year": 2016,
                "analysis": TimeAnalysis(),
            },
            "2016-02": {
                "label": "Feb",
                "month": 2,
                "year": 2016,
                "analysis": TimeAnalysis(),
            },
            "2016-03": {
                "label": "Mar",
                "month": 3,
                "year": 2016,
                "analysis": TimeAnalysis(),
            },
            "2016-04": {
                "label": "Apr",
                "month": 4,
                "year": 2016,
                "analysis": TimeAnalysis(),
            },
            "2016-05": {
                "label": "May",
                "month": 5,
                "year": 2016,
                "analysis": TimeAnalysis(),
            },
            "2016-06": {
                "label": "Jun",
                "month": 6,
                "year": 2016,
                "analysis": TimeAnalysis(),
            },
            "2016-07": {
                "label": "Jul",
                "month": 7,
                "year": 2016,
                "analysis": TimeAnalysis(),
            },
            "2016-08": {
                "label": "Aug",
                "month": 8,
                "year": 2016,
                "analysis": TimeAnalysis(),
            },
            "2016-09": {
                "label": "Sep",
                "month": 9,
                "year": 2016,
                "analysis": TimeAnalysis(),
            },
            "2016-10": {
                "label": "Oct",
                "month": 10,
                "year": 2016,
                "analysis": TimeAnalysis(),
            },
            "2016-11": {
                "label": "Nov",
                "month": 11,
                "year": 2016,
                "analysis": TimeAnalysis(fixed=10),
            },
            "2016-12": {
                "label": "Dec",
                "month": 12,
                "year": 2016,
                "analysis": TimeAnalysis(non_charge=15),
            },
            "2017-01": {
                "label": "Jan",
                "month": 1,
                "year": 2017,
                "analysis": TimeAnalysis(charge=30, pending=30),
            },
            "2017-02": {
                "label": "Feb",
                "month": 2,
                "year": 2017,
                "analysis": TimeAnalysis(),
            },
        }
    } == data
