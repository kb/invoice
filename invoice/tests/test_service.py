# -*- encoding: utf-8 -*-
import io
import pytest

from datetime import date
from decimal import Decimal

from contact.tests.factories import ContactAddressFactory, ContactFactory
from invoice.service import export_to_r, InvoicePrint
from invoice.tests.factories import InvoiceFactory, InvoiceLineFactory
from login.tests.factories import UserFactory
from stock.tests.factories import ProductCategoryFactory, ProductFactory


@pytest.mark.django_db
def test_export_to_r():
    invoice = InvoiceFactory(
        contact=ContactFactory(user=UserFactory(username="pat")),
        invoice_date=date(2018, 11, 20),
        source="ebay",
    )
    InvoiceLineFactory(
        invoice=invoice,
        product=ProductFactory(
            category=ProductCategoryFactory(name="fruit"),
            name="apple",
            description="Apple",
        ),
        quantity=Decimal("2"),
        net=Decimal("10.00"),
    )
    with io.StringIO() as response:
        result = export_to_r(response)
        response.seek(0)
        result = response.readlines()
    header = (
        "username\tinvoice_date\tsource\tcategory\t"
        "sku\tproduct\tquantity\tnet\r\n"
    )
    assert [
        header,
        "pat\t2018-11-20\tebay\tfruit\tapple\tApple\t2.00\t10.00\r\n",
    ] == result


@pytest.mark.parametrize(
    "company_name,title,first_name,last_name,expect",
    [
        ("", "Mr", "P", "Kimber", "Mr P Kimber<br />Abc<br />Xyz"),
        ("", "", "P", "Kimber", "P Kimber<br />Abc<br />Xyz"),
        ("", "", "", "Kimber", "Kimber<br />Abc<br />Xyz"),
        (
            "KB Software Ltd",
            "Mr",
            "P",
            "Kimber",
            "KB Software Ltd<br />Abc<br />Xyz",
        ),
    ],
)
@pytest.mark.django_db
def test_text_invoice_address(
    company_name, title, first_name, last_name, expect
):
    user = UserFactory(first_name=first_name, last_name=last_name)
    contact = ContactFactory(user=user, company_name=company_name, title=title)
    ContactAddressFactory(contact=contact, address_one="Abc", town="Xyz")
    invoice = InvoiceFactory(contact=contact)
    assert expect == InvoicePrint()._text_invoice_address(invoice)
