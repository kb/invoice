# -*- encoding: utf-8 -*-
import pytest

from datetime import date

from finance.models import Currency
from invoice.models import BatchInvoice
from invoice.tests.factories import (
    BatchFactory,
    BatchInvoiceFactory,
    InvoiceFactory,
)


@pytest.mark.django_db
def test_in_a_batch():
    invoice = InvoiceFactory(prefix="I", number=123, is_credit=False)
    BatchInvoiceFactory(invoice=invoice)
    assert BatchInvoice.objects.in_a_batch(invoice) is True


@pytest.mark.django_db
def test_in_a_batch_not():
    invoice = InvoiceFactory(prefix="I", number=123, is_credit=False)
    assert BatchInvoice.objects.in_a_batch(invoice) is False


@pytest.mark.django_db
def test_str():
    currency = Currency.objects.get(slug="GBP")
    batch = BatchFactory(batch_date=date(2018, 2, 22), currency=currency)
    batch_invoice = BatchInvoiceFactory(
        batch=batch, invoice=InvoiceFactory(prefix="I", number=123)
    )
    assert (
        "Invoice I000123 "
        "(batch {} 22/02/2018 currency 'GBP')".format(batch.pk)
    ) == str(batch_invoice)
