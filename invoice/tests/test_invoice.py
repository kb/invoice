# -*- encoding: utf-8 -*-
import pytest

from datetime import date
from decimal import Decimal
from django.db import IntegrityError, transaction
from django.utils import timezone

from contact.tests.factories import ContactFactory
from finance.models import Currency, VatCode
from finance.tests.factories import VatSettingsFactory
from invoice.models import Invoice, InvoiceLine, format_minutes
from invoice.service import InvoicePrint
from invoice.tests.factories import (
    InvoiceContactFactory,
    InvoiceCreditFactory,
    InvoiceFactory,
    InvoiceLineFactory,
    InvoiceSettingsFactory,
    PaymentProcessorFactory,
    TimeRecordFactory,
)
from login.tests.factories import UserFactory


@pytest.mark.django_db
def test_create():
    """Create a simple invoice"""
    invoice = InvoiceFactory()
    invoice.full_clean()
    invoice.save()
    assert invoice.pk > 0
    assert invoice.number > 0


@pytest.mark.django_db
def test_create_with_lines():
    """Create a simple invoice with lines"""
    VatSettingsFactory()
    invoice = InvoiceFactory()
    line = InvoiceLineFactory(
        invoice=invoice,
        quantity=Decimal("1.3"),
        units="hours",
        price=Decimal("300.00"),
    )
    line = InvoiceLineFactory(
        invoice=invoice,
        quantity=Decimal("2.4"),
        units="hours",
        price=Decimal("200.23"),
    )
    assert invoice.pk > 0
    assert Decimal("870.55") == invoice.net
    assert Decimal("1044.66") == invoice.gross
    assert line.is_credit is False


@pytest.mark.django_db
def test_create_with_lines_discount():
    """Create a simple invoice with a discount."""
    VatSettingsFactory()
    invoice = InvoiceFactory(discount=Decimal("50.00"))
    line = InvoiceLineFactory(
        invoice=invoice,
        quantity=Decimal("1.0"),
        units="hours",
        price=Decimal("300.00"),
    )
    line = InvoiceLineFactory(
        invoice=invoice,
        quantity=Decimal("2.0"),
        units="hours",
        price=Decimal("200.00"),
    )
    assert invoice.pk > 0
    assert Decimal("50.00") == invoice.discount
    assert Decimal("650.00") == invoice.net
    assert invoice.net + invoice.vat == invoice.gross
    assert Decimal("790.00") == invoice.gross, str(invoice.gross)
    assert line.is_credit is False


@pytest.mark.django_db
def test_credit_notes():
    invoice_date = date(2019, 3, 23)
    InvoiceFactory(invoice_date=invoice_date, source="a")
    InvoiceFactory(invoice_date=invoice_date, source="b", is_credit=True)
    InvoiceFactory(invoice_date=date(2019, 3, 28), source="c", is_credit=True)
    InvoiceFactory(invoice_date=invoice_date, source="d", is_credit=True)
    assert ["b", "d"] == [
        x.source for x in Invoice.objects.credit_notes(invoice_date)
    ]


@pytest.mark.django_db
def test_current():
    contact = ContactFactory()
    InvoiceFactory(contact=contact, number=1)
    invoice = InvoiceFactory(contact=contact, number=2)
    Invoice.objects.set_deleted(invoice, UserFactory())
    InvoiceFactory(contact=contact, number=3)
    InvoiceFactory(contact=ContactFactory(), number=4)
    assert [1, 3, 4] == [
        x.number for x in Invoice.objects.current().order_by("number")
    ]


@pytest.mark.django_db
def test_current_contact():
    contact = ContactFactory()
    InvoiceFactory(contact=contact, number=1)
    invoice = InvoiceFactory(contact=contact, number=2)
    Invoice.objects.set_deleted(invoice, UserFactory())
    InvoiceFactory(contact=contact, number=3)
    InvoiceFactory(contact=ContactFactory(), number=4)
    assert [1, 3] == [
        x.number for x in Invoice.objects.current(contact).order_by("number")
    ]


@pytest.mark.django_db
def test_current_invoice_date():
    contact = ContactFactory()
    invoice_date = date(2019, 3, 23)
    InvoiceFactory(invoice_date=invoice_date, contact=contact, number=1)
    invoice = InvoiceFactory(
        invoice_date=invoice_date, contact=contact, number=2
    )
    Invoice.objects.set_deleted(invoice, UserFactory())
    InvoiceFactory(invoice_date=date(2019, 1, 31), contact=contact, number=3)
    InvoiceFactory(
        invoice_date=invoice_date, contact=ContactFactory(), number=4
    )
    assert [1, 4] == [
        x.number
        for x in Invoice.objects.current(invoice_date=invoice_date).order_by(
            "number"
        )
    ]


@pytest.mark.django_db
def test_current_invoice_date_and_contact():
    contact = ContactFactory()
    invoice_date = date(2019, 3, 23)
    InvoiceFactory(invoice_date=invoice_date, contact=contact, number=1)
    invoice = InvoiceFactory(
        invoice_date=invoice_date, contact=contact, number=2
    )
    Invoice.objects.set_deleted(invoice, UserFactory())
    InvoiceFactory(invoice_date=date(2019, 1, 31), contact=contact, number=3)
    InvoiceFactory(
        invoice_date=invoice_date, contact=ContactFactory(), number=4
    )
    assert [1] == [
        x.number
        for x in Invoice.objects.current(
            contact=contact, invoice_date=invoice_date
        ).order_by("number")
    ]


@pytest.mark.django_db
def test_description():
    invoice = InvoiceFactory()
    assert "Invoice" == invoice.description


@pytest.mark.django_db
def test_exchange_rates():
    currency = Currency.objects.get(slug=Currency.EURO)
    payment_processor = PaymentProcessorFactory()
    invoice_date = date.today()
    InvoiceFactory(
        invoice_date=invoice_date,
        currency=currency,
        exchange_rate=Decimal(),
        upfront_payment=True,
        upfront_payment_processor=payment_processor,
    )
    InvoiceFactory(
        currency=currency,
        invoice_date=invoice_date,
        exchange_rate=Decimal("0.999"),
        upfront_payment=True,
        upfront_payment_processor=PaymentProcessorFactory(),
    )
    InvoiceFactory(
        currency=currency,
        invoice_date=invoice_date,
        exchange_rate=Decimal("0.888"),
        upfront_payment=True,
        upfront_payment_processor=payment_processor,
    )
    InvoiceFactory(
        currency=Currency.objects.get(slug=Currency.POUND),
        invoice_date=invoice_date,
        exchange_rate=Decimal("0.777"),
        upfront_payment=True,
        upfront_payment_processor=payment_processor,
    )
    InvoiceFactory(
        currency=currency,
        invoice_date=invoice_date,
        exchange_rate=Decimal("0.888"),
        upfront_payment=True,
        upfront_payment_processor=payment_processor,
    )
    InvoiceFactory(
        currency=currency,
        invoice_date=invoice_date,
        exchange_rate=Decimal("0.555"),
        upfront_payment=False,
        upfront_payment_processor=payment_processor,
    )
    assert set([Decimal(), Decimal("0.888")]) == Invoice.objects.exchange_rates(
        invoice_date, currency, payment_processor
    )


def test_format_minutes():
    assert "02:00" == format_minutes(120)


def test_format_minutes_2():
    assert "01:15" == format_minutes(75)


def test_format_minutes_3():
    assert "00:15" == format_minutes(15)


def test_format_minutes_decimal():
    assert "00:15" == format_minutes(Decimal(15.3))


@pytest.mark.django_db
def test_get_first_line_number():
    """get the number for the first invoice line"""
    invoice = InvoiceFactory()
    assert 1 == invoice.get_next_line_number()


@pytest.mark.django_db
def test_get_next_line_number():
    """get the number for the next invoice line"""
    invoice = InvoiceFactory()
    InvoiceLineFactory(invoice=invoice, line_number=1)
    InvoiceLineFactory(invoice=invoice, line_number=2)
    assert 3 == invoice.get_next_line_number()


@pytest.mark.django_db
def test_get_next_line_number_fill_gap():
    """get the number for the next invoice line"""
    invoice = InvoiceFactory()
    InvoiceLineFactory(invoice=invoice, line_number=1)
    InvoiceLineFactory(invoice=invoice, line_number=2)
    InvoiceLineFactory(invoice=invoice, line_number=4)
    assert 3 == invoice.get_next_line_number()


@pytest.mark.django_db
def test_get_next_line_number_two_invoices():
    """get the number for the next invoice line"""
    invoice_1 = InvoiceFactory()
    InvoiceLineFactory(invoice=invoice_1, line_number=1)
    InvoiceLineFactory(invoice=invoice_1, line_number=2)
    invoice_2 = InvoiceFactory()
    InvoiceLineFactory(invoice=invoice_2, line_number=1)
    assert 3 == invoice_1.get_next_line_number()
    assert 2 == invoice_2.get_next_line_number()


@pytest.mark.django_db
def test_has_lines():
    """does the invoice have any lines"""
    invoice = InvoiceFactory()
    InvoiceLineFactory(
        invoice=invoice,
        quantity=Decimal("1.3"),
        units="hours",
        price=Decimal("300.00"),
    )
    assert invoice.has_lines is True


@pytest.mark.django_db
def test_has_lines_not():
    invoice = InvoiceFactory()
    assert invoice.has_lines is False


@pytest.mark.django_db
def test_invoice_for_credit():
    invoice = InvoiceFactory()
    credit_note = InvoiceFactory(is_credit=True)
    InvoiceCreditFactory(invoice=invoice, credit_note=credit_note)
    assert [invoice] == credit_note.invoice_for_credit()


@pytest.mark.django_db
def test_invoice_for_credit_for_invoice():
    credit_note = InvoiceFactory(is_credit=False)
    assert credit_note.invoice_for_credit() is None


@pytest.mark.django_db
def test_invoice_for_credit_no_invoice():
    credit_note = InvoiceFactory(is_credit=True)
    assert [] == credit_note.invoice_for_credit()


@pytest.mark.parametrize(
    "number,prefix,expect",
    [
        (8, "", "000008"),
        (8, "C", "C000008"),
        (8, "I", "I000008"),
        (88888888, "", "88888888"),
        (88888888, "C", "C88888888"),
        (88888888, "I", "I88888888"),
    ],
)
@pytest.mark.django_db
def test_invoice_number(number, prefix, expect):
    contact = ContactFactory()
    currency = Currency.objects.get(slug=Currency.POUND)
    invoice_date = timezone.now().date()
    user = UserFactory()
    # invoice
    x = Invoice(
        number=number,
        prefix=prefix,
        contact=contact,
        user=user,
        invoice_date=invoice_date,
        currency=currency,
    )
    x.save()
    assert expect == x.invoice_number


@pytest.mark.django_db
def test_next_number():
    InvoiceFactory(number=99)
    assert 100 == Invoice.objects.next_number()


@pytest.mark.django_db
def test_next_number_2():
    InvoiceFactory(number=99, deleted=True)
    InvoiceFactory(number=98, deleted_version=1)
    assert 99 == Invoice.objects.next_number()


@pytest.mark.django_db
def test_next_number_set_deleted():
    user = UserFactory()
    # 1
    n1 = Invoice.objects.next_number()
    assert 1 == n1
    InvoiceFactory(number=n1)
    # 2
    n2 = Invoice.objects.next_number()
    assert 2 == n2
    i2 = InvoiceFactory(number=n2)
    # delete
    Invoice.objects.set_deleted(i2, user)
    # 3
    n3 = Invoice.objects.next_number()
    assert 2 == n3
    InvoiceFactory(number=n3)
    # 4
    n4 = Invoice.objects.next_number()
    assert 3 == n4
    InvoiceFactory(number=n4)


@pytest.mark.django_db
def test_next_number_set_deleted_2():
    user = UserFactory()
    # 1
    n1 = Invoice.objects.next_number()
    assert 1 == n1
    i1 = InvoiceFactory(number=n1)
    # 2
    n2 = Invoice.objects.next_number()
    assert 2 == n2
    i2 = InvoiceFactory(number=n2)
    # delete
    Invoice.objects.set_deleted(i2, user)
    Invoice.objects.set_deleted(i1, user)
    # 3
    n3 = Invoice.objects.next_number()
    assert 1 == n3


@pytest.mark.django_db
def test_next_number_set_deleted_fill_gap():
    user = UserFactory()
    # 1
    n1 = Invoice.objects.next_number()
    assert 1 == n1
    InvoiceFactory(number=n1)
    # 2
    n2 = Invoice.objects.next_number()
    assert 2 == n2
    i2 = InvoiceFactory(number=n2)
    # 3
    n3 = Invoice.objects.next_number()
    assert 3 == n3
    InvoiceFactory(number=n3)
    # delete
    Invoice.objects.set_deleted(i2, user)
    # 3
    n4 = Invoice.objects.next_number()
    assert 2 == n4


@pytest.mark.django_db
def test_number_prefix():
    """Check the invoice prefix (category) is working as expected.

    For more information, see ``TimedCreateModifyDeleteVersionModel``
    in `base/model_utils.py``.

    The prefix could be used to indicate the type e.g:

    - ``I`` for invoice
    - ``C`` for credit note

    Or to indicate the source of the invoice (or credit note) e.g:

    - ``I-COUK`` for an invoice from the ``.co.uk`` site
    - ``I-EU`` for an invoice from a Euro site
    - ``C-EU`` for a credit note from the Euro site

    """
    contact = ContactFactory()
    currency = Currency.objects.get(slug=Currency.POUND)
    invoice_date = timezone.now().date()
    user = UserFactory()
    # invoice
    Invoice(
        number=8,
        prefix="",
        contact=contact,
        user=user,
        invoice_date=invoice_date,
        currency=currency,
    ).save()
    Invoice(
        number=9,
        prefix="",
        contact=contact,
        user=user,
        invoice_date=invoice_date,
        currency=currency,
    ).save()
    # invoice
    Invoice(
        number=8,
        prefix="I",
        contact=contact,
        user=user,
        invoice_date=invoice_date,
        currency=currency,
    ).save()
    Invoice(
        number=9,
        prefix="I",
        contact=contact,
        user=user,
        invoice_date=invoice_date,
        currency=currency,
    ).save()
    # credit note
    Invoice(
        number=8,
        prefix="C",
        is_credit=True,
        contact=contact,
        user=user,
        invoice_date=invoice_date,
        currency=currency,
    ).save()
    Invoice(
        number=9,
        prefix="C",
        contact=contact,
        user=user,
        invoice_date=invoice_date,
        currency=currency,
    ).save()
    with transaction.atomic():
        with pytest.raises(IntegrityError):
            Invoice(
                number=9,
                contact=contact,
                user=user,
                invoice_date=invoice_date,
                currency=currency,
                prefix="",
            ).save()
    with transaction.atomic():
        with pytest.raises(IntegrityError):
            Invoice(
                number=9,
                contact=contact,
                user=user,
                invoice_date=invoice_date,
                currency=currency,
                prefix="I",
            ).save()
    with transaction.atomic():
        with pytest.raises(IntegrityError):
            Invoice(
                number=9,
                contact=contact,
                user=user,
                invoice_date=invoice_date,
                currency=currency,
                prefix="C",
            ).save()


@pytest.mark.django_db
def test_on_account_invoices():
    date_before = date(2019, 3, 10)
    invoice_date = date(2019, 3, 11)
    date_after = date(2019, 3, 12)
    invoice_1 = InvoiceFactory(number=1, invoice_date=invoice_date)
    InvoiceFactory(number=2, invoice_date=date_before)
    InvoiceFactory(number=3, invoice_date=invoice_date, upfront_payment=True)
    InvoiceFactory(number=4, invoice_date=invoice_date, is_credit=True)
    invoice_5 = InvoiceFactory(number=5, invoice_date=invoice_date)
    # on account
    on_account_contact = ContactFactory()
    InvoiceContactFactory(contact=on_account_contact, on_account=True)
    invoice_6 = InvoiceFactory(
        number=6,
        contact=on_account_contact,
        invoice_date=invoice_date,
        upfront_payment=True,
    )
    invoice_7 = InvoiceFactory(
        contact=on_account_contact,
        number=7,
        invoice_date=invoice_date,
        is_credit=True,
    )
    # the day after...
    InvoiceFactory(number=8, invoice_date=date_after)
    assert [invoice_1.number, invoice_5.number, invoice_6.number] == [
        x.number for x in Invoice.objects.on_account_invoices(invoice_date)
    ]


@pytest.mark.django_db
def test_user_can_edit():
    line = InvoiceLineFactory()
    assert line.user_can_edit is True


@pytest.mark.django_db
def test_user_can_edit_has_time():
    line = InvoiceLineFactory()
    TimeRecordFactory(invoice_line=line)
    assert line.user_can_edit is False


@pytest.mark.django_db
def test_user_can_edit_invoice():
    InvoiceSettingsFactory()
    VatSettingsFactory()
    invoice = InvoiceFactory()
    line = InvoiceLineFactory(invoice=invoice)
    TimeRecordFactory(invoice_line=line)
    InvoicePrint().create_pdf(invoice, None)
    # refresh
    line = InvoiceLine.objects.get(pk=line.pk)
    assert line.user_can_edit is False


@pytest.mark.django_db
def test_vat_analysis():
    """Calculate summary totals for an invoice.

    Find the total net and vat for an invoice - grouped by VAT code.

    """
    vat_code_standard = VatCode.objects.get(slug=VatCode.STANDARD)
    vat_code_zero = VatCode.objects.get(slug=VatCode.EXEMPT)
    invoice = InvoiceFactory()
    InvoiceLineFactory(
        invoice=invoice,
        quantity=Decimal("1"),
        price=Decimal("15"),
        vat_code=vat_code_standard,
    ).save_and_calculate()
    InvoiceLineFactory(
        invoice=invoice,
        quantity=Decimal("1"),
        price=Decimal("5"),
        vat_code=vat_code_standard,
    ).save_and_calculate()
    InvoiceLineFactory(
        invoice=invoice,
        quantity=Decimal("2"),
        price=Decimal("40"),
        vat_code=vat_code_zero,
    ).save_and_calculate()
    InvoiceLineFactory(
        invoice=invoice,
        quantity=Decimal("1"),
        price=Decimal("20"),
        vat_code=vat_code_zero,
    ).save_and_calculate()
    assert {
        "S": {"count": 2, "net": Decimal("20"), "vat": Decimal("4.00")},
        "E": {"count": 2, "net": Decimal("100"), "vat": Decimal("0.00")},
    } == invoice.vat_analysis()
