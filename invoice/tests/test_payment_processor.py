# -*- encoding: utf-8 -*-
import pytest

from invoice.models import PaymentProcessor
from .factories import PaymentProcessorFactory


@pytest.mark.django_db
def test_create_payment_processor():
    PaymentProcessor.objects.create_payment_processor("Orange")
    PaymentProcessor.objects.create_payment_processor("Apple")
    assert ["Apple", "Orange"] == [
        x.description for x in PaymentProcessor.objects.all()
    ]


@pytest.mark.django_db
def test_str():
    assert "Apple" == str(PaymentProcessorFactory(description="Apple"))


@pytest.mark.django_db
def test_str_deleted():
    assert "Apple (deleted)" == str(
        PaymentProcessorFactory(description="Apple", deleted=True)
    )


@pytest.mark.django_db
def test_str_no_description():
    assert "" == str(PaymentProcessorFactory(description=""))
