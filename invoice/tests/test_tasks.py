# -*- encoding: utf-8 -*-
import csv
import json
import pytest

from datetime import date, time
from freezegun import freeze_time

from contact.tests.factories import ContactFactory
from crm.tests.factories import TicketFactory
from invoice.tasks import mail_time_summary, time_summary_by_user
from invoice.tests.factories import InvoiceUserFactory, TimeRecordFactory
from login.tests.factories import UserFactory
from report.models import ReportSchedule


@pytest.mark.django_db
def test_mail_time_summary():
    user = UserFactory()
    InvoiceUserFactory(user=user, mail_time_summary=True)
    d = date.today()
    contact = ContactFactory(user=user)
    TimeRecordFactory(
        ticket=TicketFactory(contact=contact),
        date_started=d,
        start_time=time(11, 0),
        end_time=time(11, 30),
        user=user,
    )
    TimeRecordFactory(
        ticket=TicketFactory(contact=contact),
        date_started=d,
        start_time=time(10, 0),
        end_time=time(10, 15),
        user=user,
    )
    mail_time_summary()


@pytest.mark.django_db
def test_time_summary_by_user():
    user = UserFactory(username="green", first_name="P", last_name="Kimber")
    contact = ContactFactory(user=user)
    TimeRecordFactory(
        ticket=TicketFactory(contact=contact),
        date_started=date(2017, 1, 16),
        start_time=time(11, 0),
        end_time=time(11, 30),
        user=user,
    )
    TimeRecordFactory(
        billable=False,
        ticket=TicketFactory(contact=contact),
        date_started=date(2016, 12, 1),
        start_time=time(11, 0),
        end_time=time(11, 15),
        user=user,
    )
    TimeRecordFactory(
        ticket=TicketFactory(contact=contact, fixed_price=True),
        date_started=date(2016, 11, 30),
        start_time=time(11, 0),
        end_time=time(11, 10),
        user=user,
    )
    assert 0 == ReportSchedule.objects.count()
    # test the task
    with freeze_time("2018-06-22"):
        time_summary_by_user()
    assert 1 == ReportSchedule.objects.count()
    report_schedule = ReportSchedule.objects.first()
    reader = csv.reader(open(report_schedule.output_file.path), "excel")
    first_row = None
    result = []
    for row in reader:
        if not first_row:
            first_row = row
        else:
            # check the values in the last three columns
            result.append(row[4:])
    assert [
        "user_name",
        "year",
        "month",
        "label",
        "non_minutes",
        "fixed_minutes",
        "charge_minutes",
    ] == first_row
    print(json.dumps(result, indent=4))
    assert [
        ["0", "0", "0"],
        ["0", "0", "0"],
        ["0", "0", "0"],
        ["0", "0", "0"],
        ["0", "0", "0"],
        ["0", "10.0", "0"],
        ["15.0", "0", "0"],
        ["0", "0", "30.0"],
        ["0", "0", "0"],
        ["0", "0", "0"],
        ["0", "0", "0"],
        ["0", "0", "0"],
        ["0", "0", "0"],
        ["0", "0", "0"],
        ["0", "0", "0"],
        ["0", "0", "0"],
        ["0", "0", "0"],
        ["0", "0", "0"],
        ["0", "0", "0"],
        ["0", "0", "0"],
        ["0", "0", "0"],
        ["0", "0", "0"],
        ["0", "0", "0"],
        ["0", "0", "0"],
    ] == result
