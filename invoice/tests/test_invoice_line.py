# -*- encoding: utf-8 -*-
import pytest

from datetime import date
from decimal import Decimal

from invoice.models import InvoiceLine
from .factories import InvoiceFactory, InvoiceLineFactory, TimeRecordFactory


@pytest.mark.django_db
def test_has_time_record():
    invoice_line = InvoiceLineFactory()
    TimeRecordFactory(invoice_line=invoice_line)
    assert invoice_line.has_time_record is True


@pytest.mark.django_db
def test_has_time_record_not():
    invoice_line = InvoiceLineFactory()
    assert invoice_line.has_time_record is False


@pytest.mark.django_db
def test_net_and_vat():
    invoice = InvoiceFactory(invoice_date=date(2019, 2, 23))
    InvoiceLineFactory(invoice=invoice, net=Decimal(100), vat=Decimal(20))
    InvoiceLineFactory(invoice=invoice, net=Decimal(18), vat=Decimal(108))
    invoice = InvoiceFactory(invoice_date=date(2019, 2, 24))
    InvoiceLineFactory(invoice=invoice, net=Decimal(100), vat=Decimal(20))
    assert (Decimal("118"), Decimal("128")) == InvoiceLine.objects.net_and_vat(
        date(2019, 2, 23)
    )


@pytest.mark.django_db
def test_net_and_vat_all():
    invoice = InvoiceFactory(invoice_date=date(2019, 2, 23))
    InvoiceLineFactory(invoice=invoice, net=Decimal(100), vat=Decimal(20))
    InvoiceLineFactory(invoice=invoice, net=Decimal(18), vat=Decimal(108))
    invoice = InvoiceFactory(invoice_date=date(2019, 2, 24))
    InvoiceLineFactory(invoice=invoice, net=Decimal(100), vat=Decimal(20))
    assert (Decimal("218"), Decimal("148")) == InvoiceLine.objects.net_and_vat()
