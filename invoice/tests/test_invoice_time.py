# -*- encoding: utf-8 -*-
import pytest

from datetime import date, time
from decimal import Decimal

from contact.tests.factories import ContactFactory
from crm.tests.factories import TicketFactory
from finance.models import Currency
from finance.tests.factories import VatSettingsFactory
from invoice.service import InvoiceCreate, InvoiceError, InvoicePrint
from invoice.tests.factories import (
    InvoiceContactFactory,
    InvoiceSettingsFactory,
    TimeRecordFactory,
)
from stock.tests.factories import ProductFactory


@pytest.mark.django_db
def test_invoice_with_time_records():
    """Invoice time records."""
    time_record_product = ProductFactory(price=Decimal(20.00))
    InvoiceSettingsFactory(time_record_product=time_record_product)
    VatSettingsFactory()
    currency = Currency.objects.get(slug=Currency.POUND)
    contact = ContactFactory()
    InvoiceContactFactory(contact=contact)
    ticket = TicketFactory(contact=contact)
    tr1 = TimeRecordFactory(
        ticket=ticket, start_time=time(12, 0, 0), end_time=time(13, 0, 0)
    )
    TimeRecordFactory(
        ticket=ticket, start_time=time(12, 0, 0), end_time=time(13, 0, 0)
    )
    invoice = InvoiceCreate().create(tr1.user, contact, currency, date.today())
    assert invoice.is_draft
    InvoicePrint().create_pdf(invoice, None)
    assert invoice.is_draft is False
    assert Decimal("40.00") == invoice.net


@pytest.mark.django_db
def test_invoice_with_time_records_invalid():
    """One of the time records has no end time, so cannot be invoiced."""
    InvoiceSettingsFactory()
    VatSettingsFactory()
    currency = Currency.objects.get(slug=Currency.POUND)
    contact = ContactFactory()
    InvoiceContactFactory(contact=contact)
    ticket = TicketFactory(contact=contact)
    tr1 = TimeRecordFactory(ticket=ticket)
    TimeRecordFactory(
        ticket=ticket, start_time=time(10, 30), end_time=time(10, 15)
    )
    TimeRecordFactory(ticket=ticket)
    with pytest.raises(InvoiceError) as ex:
        InvoiceCreate().create(tr1.user, contact, currency, date.today())
    message = str(ex.value)
    assert "ends before it starts" in message
    assert "are you running tests near midnight" in message
