# -*- encoding: utf-8 -*-
import factory

from datetime import date
from dateutil.relativedelta import relativedelta
from decimal import Decimal

from django.utils import timezone

from contact.tests.factories import ContactFactory
from crm.tests.factories import TicketFactory
from finance.models import Currency, VatCode
from finance.tests.factories import CurrencyFactory
from login.tests.factories import UserFactory
from invoice.models import (
    Batch,
    BatchInvoice,
    Invoice,
    InvoiceContact,
    InvoiceCredit,
    InvoiceIssue,
    InvoiceLine,
    InvoiceSettings,
    InvoiceUser,
    PaymentProcessor,
    quantize,
    QuickTimeRecord,
    TimeCode,
    TimeRecord,
)
from stock.tests.factories import ProductFactory


class PaymentProcessorFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = PaymentProcessor

    @factory.sequence
    def description(n):
        return "description_{}".format(n)


class BatchFactory(factory.django.DjangoModelFactory):
    batch_date = date.today()
    currency = factory.SubFactory(CurrencyFactory)
    exchange_rate = Decimal()
    payment_processor = factory.SubFactory(PaymentProcessorFactory)

    class Meta:
        model = Batch


class InvoiceContactFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = InvoiceContact

    contact = factory.SubFactory(ContactFactory)
    hourly_rate = Decimal("20")

    @factory.sequence
    def vat_number(n):
        return "vat_number_{}".format(n)


class InvoiceFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Invoice

    contact = factory.SubFactory(ContactFactory)
    invoice_date = date.today()
    user = factory.SubFactory(UserFactory)
    exchange_rate = Decimal()

    @factory.lazy_attribute
    def currency(self):
        return Currency.objects.get(slug=Currency.POUND)

    @factory.sequence
    def number(n):
        return n + 1


class BatchInvoiceFactory(factory.django.DjangoModelFactory):
    batch = factory.SubFactory(BatchFactory)
    invoice = factory.SubFactory(InvoiceFactory)

    class Meta:
        model = BatchInvoice


class InvoiceCreditFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = InvoiceCredit

    invoice = factory.SubFactory(InvoiceFactory)
    credit_note = factory.SubFactory(InvoiceFactory)


class InvoiceIssueFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = InvoiceIssue

    invoice = factory.SubFactory(InvoiceFactory)


class InvoiceLineFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = InvoiceLine

    invoice = factory.SubFactory(InvoiceFactory)
    price = Decimal("0")
    product = factory.SubFactory(ProductFactory)
    quantity = 1
    units = "each"

    @factory.sequence
    def description(n):
        return "description_{}".format(n)

    @factory.sequence
    def line_number(n):
        return n

    @factory.lazy_attribute
    def user(self):
        return self.invoice.user

    @factory.lazy_attribute
    def vat_code(self):
        return VatCode.objects.get(slug=VatCode.STANDARD)

    @factory.lazy_attribute
    def vat_rate(self):
        vat_code = VatCode.objects.get(slug=VatCode.STANDARD)
        return vat_code.rate

    @factory.lazy_attribute
    def net(self):
        return quantize(self.price * self.quantity)

    @factory.lazy_attribute
    def vat(self):
        vat_code = VatCode.objects.get(slug=VatCode.STANDARD)
        return quantize(self.price * self.quantity * vat_code.rate)


class InvoiceSettingsFactory(factory.django.DjangoModelFactory):
    time_record_product = factory.SubFactory(ProductFactory)

    class Meta:
        model = InvoiceSettings


class InvoiceUserFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = InvoiceUser

    user = factory.SubFactory(UserFactory)


class TimeCodeFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = TimeCode

    @factory.sequence
    def description(n):
        return "description_{}".format(n)


class QuickTimeRecordFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = QuickTimeRecord

    time_code = factory.SubFactory(TimeCodeFactory)
    user = factory.SubFactory(UserFactory)

    @factory.sequence
    def description(n):
        return "description_{}".format(n)


class TimeRecordFactory(factory.django.DjangoModelFactory):
    billable = True
    date_started = date.today()
    ticket = factory.SubFactory(TicketFactory)

    class Meta:
        model = TimeRecord

    @factory.sequence
    def title(n):
        return "Time record {}".format(n)

    @factory.lazy_attribute
    def end_time(self):
        return (timezone.now() + relativedelta(hours=1)).time()

    @factory.lazy_attribute
    def start_time(self):
        return timezone.now().time()

    @factory.lazy_attribute
    def user(self):
        return self.ticket.user
