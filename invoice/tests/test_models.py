# -*- encoding: utf-8 -*-
import pytest

from crm.tests.factories import NoteFactory, TicketFactory
from invoice.models import ticket_activity
from invoice.tests.factories import TimeRecordFactory


@pytest.mark.django_db
def test_ticket_activity(django_assert_max_num_queries):
    ticket = TicketFactory()
    NoteFactory(ticket=ticket, title="a.n", description="A.n")
    TimeRecordFactory(ticket=ticket, title="b.t", description="B.t")
    NoteFactory(ticket=ticket, title="c.n", description="C.n")
    # MailTicketFactory(
    #    mail=GoogleMailFactory(subject="Fruit"),
    #    ticket=TicketFactory(title="Apple"),
    # )
    with django_assert_max_num_queries(3):
        activity = ticket_activity(ticket)
    # assert ["c.n", "b.t", "a.n"] == [x["title"] for x in activity]
    # assert ["C.n", "B.t", "A.n"] == [x["description"] for x in activity]
    assert ["note", "time", "note"] == [x["model"] for x in activity]
    # check we have the correct ``object``
    assert ["Note", "TimeRecord", "Note"] == [
        x["object"].__class__.__name__ for x in activity
    ]
    assert ["c.n", "b.t", "a.n"] == [x["object"].title for x in activity]
