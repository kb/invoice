# -*- encoding: utf-8 -*-
import pytest

from datetime import date
from decimal import Decimal
from freezegun import freeze_time
from http import HTTPStatus
from django.urls import reverse
from django.utils import timezone

from base.url_utils import url_with_querystring
from contact.tests.factories import ContactFactory
from crm.tests.factories import (
    FundedFactory,
    KanbanCardFactory,
    KanbanColumnFactory,
    KanbanLaneFactory,
    TicketFactory,
)
from finance.tests.factories import VatSettingsFactory
from invoice.models import (
    Currency,
    Invoice,
    InvoiceContact,
    InvoiceSettings,
    InvoiceUser,
)
from invoice.tests.factories import (
    InvoiceContactFactory,
    InvoiceFactory,
    InvoiceIssueFactory,
    InvoiceSettingsFactory,
    PaymentProcessorFactory,
    TimeRecordFactory,
)
from login.tests.factories import TEST_PASSWORD, UserFactory
from report.models import ReportSchedule
from report.tests.factories import ReportSpecificationFactory
from stock.tests.factories import ProductFactory


@pytest.mark.django_db
def test_contact_report(client):
    contact = ContactFactory(
        user=UserFactory(first_name="P", last_name="Kimber"), company_name=""
    )
    report_specification = ReportSpecificationFactory(
        title="Fruit",
        slug="invoice-time-analysis-contact",
        app="invoice",
        report_class="TimeAnalysisByContact",
    )
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    url = reverse(
        "invoice.contact.report",
        kwargs={"pk": contact.pk, "slug": report_specification.slug},
    )
    response = client.post(url)
    assert HTTPStatus.FOUND == response.status_code
    # make sure the report is scheduled
    qs = ReportSchedule.objects.current()
    assert 1 == qs.count()
    schedule = qs.first()
    assert (
        reverse("report.schedule.csv.view", args=[schedule.pk]) == response.url
    )
    assert report_specification.slug == schedule.report.slug
    assert timezone.now().date() == schedule.created.date()
    assert "from_date" in schedule.parameters
    assert "to_date" in schedule.parameters
    assert "contact_pk" in schedule.parameters
    assert contact.pk == schedule.parameters["contact_pk"]
    assert "Fruit - P Kimber from" in schedule.title


@pytest.mark.django_db
def test_contact_report_get(client):
    contact = ContactFactory()
    report_specification = ReportSpecificationFactory()
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    url = reverse(
        "invoice.contact.report",
        kwargs={"pk": contact.pk, "slug": report_specification.slug},
    )
    response = client.get(url)
    assert HTTPStatus.OK == response.status_code
    assert "contact" in response.context
    assert contact == response.context["contact"]
    assert "report_specification" in response.context
    assert report_specification == response.context["report_specification"]


@pytest.mark.django_db
def test_contact_report_get_invalid_report(client):
    contact = ContactFactory()
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    url = reverse(
        "invoice.contact.report",
        kwargs={"pk": contact.pk, "slug": "report-does-not-exist"},
    )
    response = client.get(url)
    assert HTTPStatus.OK == response.status_code
    assert "contact" in response.context
    assert contact == response.context["contact"]
    assert "report_specification" not in response.context
    assert "report_slug" in response.context
    assert "report-does-not-exist" == response.context["report_slug"]


@pytest.mark.django_db
def test_invoice_contact_create(client):
    user = UserFactory(username="staff", is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    contact = ContactFactory()
    url = reverse("invoice.contact.create", kwargs={"pk": contact.pk})
    data = {"hourly_rate": Decimal("12.34")}
    response = client.post(url, data)
    assert HTTPStatus.FOUND == response.status_code
    expect = reverse("contact.detail", args=[contact.pk])
    assert expect == response["Location"]
    invoice_contact = InvoiceContact.objects.get(contact=contact)
    assert Decimal("12.34") == invoice_contact.hourly_rate


@pytest.mark.django_db
def test_invoice_contact_update(client):
    user = UserFactory(username="staff", is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    contact = ContactFactory()
    invoice_contact = InvoiceContactFactory(contact=contact)
    url = reverse("invoice.contact.update", kwargs={"pk": invoice_contact.pk})
    data = {"hourly_rate": Decimal("12.34")}
    response = client.post(url, data)
    assert HTTPStatus.FOUND == response.status_code
    expect = reverse("contact.detail", args=[contact.pk])
    assert expect == response["Location"]
    invoice_contact = InvoiceContact.objects.get(contact=contact)
    assert Decimal("12.34") == invoice_contact.hourly_rate


@pytest.mark.django_db
def test_invoice_create_time(client):
    InvoiceSettingsFactory()
    VatSettingsFactory()
    contact = ContactFactory()
    InvoiceContactFactory(contact=contact)
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    url = reverse("invoice.create.time", kwargs={"pk": contact.pk})
    response = client.get(url)
    assert HTTPStatus.OK == response.status_code
    assert "form" in response.context
    form = response.context["form"]
    assert ["currency", "iteration_end"] == [x for x in form.fields]


@pytest.mark.django_db
def test_invoice_create_time_post(client):
    InvoiceSettingsFactory()
    VatSettingsFactory()
    contact = ContactFactory()
    ticket = TicketFactory(contact=contact)
    TimeRecordFactory(
        ticket=ticket, date_started=date(2019, 3, 29), billable=True
    )
    InvoiceContactFactory(contact=contact)
    assert 0 == Invoice.objects.count()
    user = UserFactory(is_staff=True)
    pound = Currency.objects.get(slug=Currency.POUND)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    url = reverse("invoice.create.time", kwargs={"pk": contact.pk})
    data = {"currency": pound.pk, "iteration_end": date(2019, 3, 30)}
    with freeze_time("2019-03-30"):
        response = client.post(url, data)
    assert HTTPStatus.FOUND == response.status_code, response.context[
        "form"
    ].errors
    assert reverse("invoice.list") == response.url
    assert 1 == Invoice.objects.count()
    invoice = Invoice.objects.first()
    assert pound == invoice.currency
    assert date(2019, 3, 30) == invoice.invoice_date


@pytest.mark.django_db
def test_invoice_create_time_post_missing(client):
    InvoiceSettingsFactory()
    VatSettingsFactory()
    contact = ContactFactory()
    InvoiceContactFactory(contact=contact)
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    url = reverse("invoice.create.time", kwargs={"pk": contact.pk})
    response = client.post(url)
    assert HTTPStatus.OK == response.status_code
    assert "form" in response.context
    form = response.context["form"]
    assert {
        "currency": ["This field is required."],
        "iteration_end": ["This field is required."],
    } == form.errors


@pytest.mark.django_db
def test_invoice_refresh_time_records(client):
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    invoice = InvoiceFactory()
    url = reverse("invoice.refresh.time.records", kwargs={"pk": invoice.pk})
    response = client.get(url)
    assert HTTPStatus.OK == response.status_code
    assert "form" in response.context
    form = response.context["form"]
    assert ["iteration_end"] == [x for x in form.fields]


@pytest.mark.django_db
def test_invoice_user_update(client):
    user = UserFactory(username="staff", is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    url = reverse("invoice.user.update")
    data = {"mail_time_summary": True}
    response = client.post(url, data)
    assert HTTPStatus.FOUND == response.status_code
    expect = reverse("project.settings")
    assert expect == response["Location"]
    obj = InvoiceUser.objects.get(user=user)
    assert True == obj.mail_time_summary


@pytest.mark.django_db
def test_issue_list(client):
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    InvoiceIssueFactory(comment="a")
    InvoiceIssueFactory(comment="b")
    response = client.get(reverse("invoice.issue.list"))
    assert HTTPStatus.OK == response.status_code
    assert "invoiceissue_list" in response.context
    invoice_issue_list = response.context["invoiceissue_list"]
    assert ["b", "a"] == [x.comment for x in invoice_issue_list]


@pytest.mark.django_db
def test_issue_update(client):
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    issue = InvoiceIssueFactory()
    url = reverse("invoice.issue.update", kwargs={"pk": issue.pk})
    response = client.post(url, {"notes": "apple pie", "confirmed": True})
    assert HTTPStatus.FOUND == response.status_code


@pytest.mark.django_db
def test_timerecord_outstanding_bill_free_list(client):
    user = UserFactory(is_staff=True)
    # time record 1
    contact_1 = ContactFactory(user=UserFactory(username="a"))
    ticket_1 = TicketFactory(contact=contact_1)
    TimeRecordFactory(ticket=ticket_1, billable=False)
    # not funded - so include
    contact_2 = ContactFactory(user=UserFactory(username="b"))
    ticket_2 = TicketFactory(
        contact=contact_2, funded=FundedFactory(free_of_charge=True)
    )
    TimeRecordFactory(ticket=ticket_2, billable=True)
    # kanban lane - not funded - so include
    contact_3 = ContactFactory(user=UserFactory(username="c"))
    ticket_3 = TicketFactory(contact=contact_3)
    lane = KanbanLaneFactory(funded=False)
    column = KanbanColumnFactory(lane=lane)
    KanbanCardFactory(column=column, ticket=ticket_3)
    TimeRecordFactory(title="t10", ticket=ticket_3)
    # test
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    response = client.get(reverse("invoice.time.outstanding.bill.free.list"))
    assert HTTPStatus.OK == response.status_code
    assert "ticket_list" in response.context
    ticket_list = response.context["ticket_list"]
    assert ["b", "c"] == [x.contact.user.username for x in ticket_list]


@pytest.mark.django_db
def test_timerecord_outstanding_list(client):
    user = UserFactory(is_staff=True)
    # time record 1
    contact_1 = ContactFactory(user=UserFactory(username="a"))
    ticket_1 = TicketFactory(contact=contact_1)
    TimeRecordFactory(ticket=ticket_1, billable=False)
    # time record 2
    contact_2 = ContactFactory(user=UserFactory(username="b"))
    ticket_2 = TicketFactory(contact=contact_2)
    TimeRecordFactory(ticket=ticket_2, billable=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    response = client.get(reverse("invoice.time.outstanding.list"))
    assert HTTPStatus.OK == response.status_code
    assert "contact_list" in response.context
    contact_list = response.context["contact_list"]
    assert ["b"] == [x.user.username for x in contact_list]


@pytest.mark.django_db
def test_timerecord_summary(client):
    user = UserFactory(username="staff", is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    url = reverse("invoice.time.summary")
    response = client.get(url)
    assert HTTPStatus.OK == response.status_code


@pytest.mark.django_db
def test_invoice_list(client):
    user = UserFactory(is_staff=True)
    InvoiceFactory()
    InvoiceFactory()
    InvoiceFactory()
    PaymentProcessorFactory()
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    response = client.get(reverse("invoice.list"))
    assert HTTPStatus.OK == response.status_code
    assert "net_total" in response.context
    assert "gross_total" in response.context
    assert "invoice_list" in response.context
    assert 3 == len(response.context["invoice_list"])


@pytest.mark.parametrize(
    "invoice_date,invoice_number,currency_slug,result",
    [
        (date(2019, 1, 21), None, Currency.POUND, [1]),
        (None, None, Currency.EURO, [3]),
        (None, None, Currency.POUND, [2, 1]),
        (None, None, None, [3, 2, 1]),
    ],
)
@pytest.mark.django_db
def test_invoice_list_search(
    client, invoice_date, invoice_number, currency_slug, result
):
    user = UserFactory(is_staff=True)
    InvoiceFactory(invoice_date=date(2019, 1, 21), number=1)
    InvoiceFactory(invoice_date=date(2019, 1, 22), number=2)
    InvoiceFactory(
        invoice_date=date(2019, 1, 23),
        number=3,
        currency=Currency.objects.get(slug=Currency.EURO),
    )
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    parameters = {}
    if currency_slug:
        parameters["currency"] = Currency.objects.get(slug=currency_slug).pk
    if invoice_date:
        parameters["invoice_date"] = invoice_date.strftime("%d/%m/%Y")
    if invoice_number:
        parameters["number"] = invoice_number
    url = url_with_querystring(reverse("invoice.list"), **parameters)
    response = client.get(url)
    assert HTTPStatus.OK == response.status_code
    assert "invoice_list" in response.context
    assert result == [x.number for x in response.context["invoice_list"]]


@pytest.mark.django_db
def test_settings(client):
    user = UserFactory(is_superuser=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    product = ProductFactory()
    response = client.post(
        reverse("invoice.settings.update"),
        {
            "name_and_address": "Hatherleigh",
            "phone_number": "01837",
            "footer": "KB",
            "time_record_product": product.pk,
        },
    )
    assert HTTPStatus.FOUND == response.status_code
    assert reverse("project.settings") == response.url
    invoice_settings = InvoiceSettings.load()
    assert "01837" == invoice_settings.phone_number
    assert "Hatherleigh" == invoice_settings.name_and_address
    assert "KB" == invoice_settings.footer
    assert product == invoice_settings.time_record_product


@pytest.mark.django_db
def test_timerecord_summary_stop_time(client):
    user = UserFactory(username="staff", is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    time_record = TimeRecordFactory(end_time=None)
    url = reverse("invoice.time.summary")
    data = {"pk": time_record.pk}
    assert time_record.end_time is None
    response = client.post(url, data)
    assert HTTPStatus.FOUND == response.status_code
    assert url == response["Location"]
    time_record.refresh_from_db()
    assert time_record.end_time is not None


@pytest.mark.django_db
def test_timerecord_summary_user(client):
    user = UserFactory(username="staff", is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    url = reverse("invoice.time.summary.user", args=[UserFactory().pk])
    response = client.get(url)
    assert HTTPStatus.OK == response.status_code
