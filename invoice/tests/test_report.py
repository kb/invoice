# -*- encoding: utf-8 -*-
"""
Test report.
"""
import csv
import pytest
import pytz

from datetime import date, datetime, time
from dateutil.relativedelta import relativedelta
from decimal import Decimal
from django.core.management import call_command
from django.http.response import HttpResponse
from django.utils import timezone

from contact.tests.factories import ContactFactory
from crm.tests.factories import TicketFactory
from invoice.models import TimeAnalysis, TimeRecord
from invoice.report import (
    ReportInvoiceTimeAnalysis,
    ReportInvoiceTimeAnalysisCSV,
    TicketReport,
    TicketSummary,
    time_summary,
)
from invoice.tests.factories import (
    InvoiceContactFactory,
    InvoiceFactory,
    InvoiceLineFactory,
    InvoiceSettingsFactory,
    TimeRecordFactory,
)
from login.tests.factories import UserFactory
from report.models import ReportSchedule, ReportSpecification


@pytest.mark.django_db
def test_report_charge_non_charge():
    to_date = timezone.now()
    from_date = to_date + relativedelta(months=-1)
    d = from_date + relativedelta(days=7)
    TimeRecordFactory(
        billable=True,
        date_started=d,
        start_time=time(11, 0),
        end_time=time(11, 30),
    )
    TimeRecordFactory(
        billable=False,
        date_started=d,
        start_time=time(10, 0),
        end_time=time(10, 15),
    )
    # do not include this record - no end time
    TimeRecordFactory(
        billable=False, date_started=d, start_time=time(11, 0), end_time=None
    )
    # do not include this record - outside of time
    TimeRecordFactory(
        billable=False,
        date_started=timezone.now() + relativedelta(months=+1),
        start_time=time(11, 0),
        end_time=time(11, 30),
    )
    data = TimeRecord.objects.report_charge_non_charge(from_date, to_date)
    assert TimeAnalysis(charge=30, non_charge=15, pending=30) == data


@pytest.mark.django_db
def test_report_charge_non_charge_user():
    to_date = timezone.now()
    from_date = to_date + relativedelta(months=-1)
    d = from_date + relativedelta(days=7)
    user = UserFactory()
    TimeRecordFactory(
        billable=True,
        date_started=d,
        start_time=time(11, 0),
        end_time=time(11, 10),
        user=user,
    )
    TimeRecordFactory(
        billable=False,
        date_started=d,
        start_time=time(11, 0),
        end_time=time(11, 5),
        user=user,
    )
    TimeRecordFactory(
        ticket=TicketFactory(fixed_price=True),
        billable=False,
        date_started=d,
        start_time=time(11, 0),
        end_time=time(11, 7),
        user=user,
    )
    # records for other users
    TimeRecordFactory(
        billable=True,
        date_started=d,
        start_time=time(11, 0),
        end_time=time(11, 30),
    )
    TimeRecordFactory(
        billable=False,
        date_started=d,
        start_time=time(10, 0),
        end_time=time(10, 15),
    )
    # do not include this record
    TimeRecordFactory(
        billable=False, date_started=d, start_time=time(11, 0), end_time=None
    )
    data = TimeRecord.objects.report_charge_non_charge(from_date, to_date, user)
    assert TimeAnalysis(charge=10, fixed=7, non_charge=5, pending=10) == data


@pytest.mark.django_db
def test_report_time_analysis():
    user = UserFactory()
    contact = ContactFactory(user=user)
    invoice = InvoiceFactory(contact=contact, is_draft=False)
    InvoiceContactFactory(contact=contact, hourly_rate=Decimal("20.00"))
    InvoiceLineFactory(invoice=invoice)
    report = ReportInvoiceTimeAnalysis()
    report.report(invoice, user, HttpResponse())


@pytest.mark.django_db
def test_report_time_analysis_by_contact():
    """This report is written using the 'report' app."""
    call_command("init_app_invoice")
    user = UserFactory(username="pat", first_name="P", last_name="Kimber")
    contact = ContactFactory(user=user)
    ticket = TicketFactory(pk=88, contact=contact, title="Apple")
    TimeRecordFactory(
        ticket=ticket,
        billable=True,
        date_started=date(2017, 1, 31),
        start_time=time(11, 0),
        end_time=time(11, 30),
        user=user,
    )
    report_specification = ReportSpecification.objects.get(
        slug="invoice-time-analysis-contact"
    )
    parameters = {
        "contact_pk": contact.pk,
        "from_date": datetime(2017, 1, 1, 6, 0, 0, tzinfo=pytz.utc),
        "to_date": datetime(2017, 2, 22, 6, 0, 0, tzinfo=pytz.utc),
    }
    report_specification.schedule(user, parameters=parameters)
    schedule_pks = ReportSchedule.objects.process()
    assert 1 == len(schedule_pks)
    schedule_pk = schedule_pks[0]
    schedule = ReportSchedule.objects.get(pk=schedule_pk)
    reader = csv.reader(open(schedule.output_file.path), "excel")
    first_row = None
    result = []
    for row in reader:
        if not first_row:
            first_row = row
        else:
            result.append(row)
    assert [
        "Ticket",
        "Description",
        "Charge",
        "Fixed",
        "Non-Charge",
    ] == first_row
    assert [
        ["{}".format(ticket.pk), "Apple", "00:30", "00:00", "00:00"]
    ] == result


@pytest.mark.django_db
def test_report_time_analysis_csv():
    user = UserFactory()
    contact = ContactFactory(user=user)
    invoice = InvoiceFactory(contact=contact, is_draft=False)
    InvoiceContactFactory(contact=contact, hourly_rate=Decimal("20.00"))
    InvoiceLineFactory(invoice=invoice)
    report = ReportInvoiceTimeAnalysisCSV()
    with open("temp.csv", "w", newline="") as out:
        report.report(invoice, user, out)


@pytest.mark.django_db
def test_report_time_analysis_csv_no_invoice_contact():
    user = UserFactory()
    contact = ContactFactory(user=user)
    invoice = InvoiceFactory(contact=contact, is_draft=False)
    InvoiceLineFactory(invoice=invoice)
    report = ReportInvoiceTimeAnalysisCSV()
    with open("temp.csv", "w", newline="") as out:
        report.report(invoice, user, out)


@pytest.mark.django_db
def test_report_time_by_crm_contact():
    user = UserFactory(username="pat", first_name="P", last_name="Kimber")
    contact = ContactFactory(user=user, company_name="")
    TimeRecordFactory(
        ticket=TicketFactory(pk=88, contact=contact, title="Apple"),
        billable=True,
        date_started=date(2017, 1, 31),
        start_time=time(11, 0),
        end_time=time(11, 30),
        user=user,
    )
    data = TimeRecord.objects.report_time_by_crm_contact(
        contact, date(2017, 1, 1), date(2017, 1, 31)
    )
    assert [
        TicketSummary(
            pk=88,
            description="Apple",
            contact="P Kimber",
            user_name="pat",
            data=TimeAnalysis(charge=30, invoiced=0, pending=30),
        )
    ] == data


@pytest.mark.django_db
def test_report_time_by_user():
    green = UserFactory(username="green")
    red = UserFactory(username="red")
    to_date = timezone.now()
    from_date = to_date + relativedelta(months=-1)
    d = from_date + relativedelta(days=7)
    TimeRecordFactory(
        billable=True,
        date_started=d,
        start_time=time(11, 0),
        end_time=time(11, 30),
        user=red,
    )
    TimeRecordFactory(
        ticket=TicketFactory(fixed_price=True),
        billable=False,
        date_started=d,
        start_time=time(9, 0),
        end_time=time(9, 7),
        user=green,
    )
    TimeRecordFactory(
        billable=False,
        date_started=d,
        start_time=time(10, 0),
        end_time=time(10, 15),
        user=green,
    )
    data = TimeRecord.objects.report_time_by_user(from_date, to_date)
    assert {
        "red": TimeAnalysis(charge=30, pending=30),
        "green": TimeAnalysis(fixed=7, non_charge=15),
    } == data


@pytest.mark.django_db
def test_report_time_by_ticket():
    user = UserFactory(username="green")
    d = timezone.now().date()
    t1 = TicketFactory(pk=1, contact=ContactFactory())
    TimeRecordFactory(
        ticket=t1,
        date_started=d,
        start_time=time(11, 0),
        end_time=time(11, 30),
        user=user,
    )
    TimeRecordFactory(
        ticket=TicketFactory(pk=2, contact=ContactFactory()),
        date_started=d,
        start_time=time(10, 0),
        end_time=time(10, 15),
        user=user,
    )
    # another date (so not included)
    TimeRecordFactory(
        ticket=t1,
        date_started=d + relativedelta(days=-1),
        start_time=time(12, 0),
        end_time=time(12, 10),
        user=UserFactory(),
    )
    # another user (so not included)
    TimeRecordFactory(
        ticket=t1,
        date_started=d,
        start_time=time(12, 0),
        end_time=time(12, 10),
        user=UserFactory(),
    )
    TimeRecordFactory(
        ticket=t1,
        date_started=d,
        start_time=time(12, 0),
        end_time=time(12, 10),
        user=user,
    )
    data = TimeRecord.objects.report_time_by_ticket(user, d)
    assert {
        1: TimeAnalysis(charge=40, pending=40),
        2: TimeAnalysis(charge=15, pending=15),
    } == data


@pytest.mark.django_db
def test_report_time_by_user_by_week():
    user = UserFactory(username="green")
    from_date = datetime(2015, 12, 20, 0, 0, 0, tzinfo=pytz.utc)
    to_date = datetime(2016, 1, 7, 0, 0, 0, tzinfo=pytz.utc)
    TimeRecordFactory(
        billable=True,
        date_started=datetime(2015, 12, 21, 6, 0, 0, tzinfo=pytz.utc),
        start_time=time(11, 0),
        end_time=time(11, 30),
        user=user,
    )
    TimeRecordFactory(
        billable=True,
        date_started=datetime(2016, 1, 5, 6, 0, 0, tzinfo=pytz.utc),
        start_time=time(10, 0),
        end_time=time(10, 3),
        user=user,
    )
    TimeRecordFactory(
        billable=True,
        date_started=datetime(2016, 1, 7, 6, 0, 0, tzinfo=pytz.utc),
        start_time=time(10, 0),
        end_time=time(10, 4),
        user=UserFactory(),
    )
    TimeRecordFactory(
        billable=False,
        date_started=datetime(2016, 1, 6, 6, 0, 0, tzinfo=pytz.utc),
        start_time=time(10, 0),
        end_time=time(10, 15),
        user=user,
    )
    data = TimeRecord.objects.report_time_by_user_by_week(
        from_date, to_date, user
    )
    assert {
        "2015_51": TimeAnalysis(charge=30, pending=30),
        "2015_52": TimeAnalysis(),
        "2016_01": TimeAnalysis(charge=3, non_charge=15, pending=3),
        "2016_02": TimeAnalysis(),
    } == data


@pytest.mark.django_db
def test_report_time_for_no_user():
    to_date = timezone.now()
    from_date = to_date + relativedelta(months=-1)
    d = from_date + relativedelta(days=7)
    contact = ContactFactory(user=UserFactory(username="bob"))
    ticket = TicketFactory(contact=contact)
    TimeRecordFactory(
        ticket=ticket,
        billable=True,
        date_started=d,
        start_time=time(11, 0),
        end_time=time(11, 30),
    )
    contact = ContactFactory(user=UserFactory(username="sam"))
    ticket = TicketFactory(contact=contact)
    TimeRecordFactory(
        ticket=ticket,
        billable=False,
        date_started=d,
        start_time=time(10, 0),
        end_time=time(10, 15),
    )
    data = TimeRecord.objects.report_time_for_user(from_date, to_date)
    assert {
        "bob": TimeAnalysis(charge=30, pending=30),
        "sam": TimeAnalysis(non_charge=15),
    } == data


@pytest.mark.django_db
def test_report_time_for_user_user():
    user = UserFactory()
    to_date = timezone.now()
    from_date = to_date + relativedelta(months=-1)
    d = from_date + relativedelta(days=7)
    contact = ContactFactory(user=UserFactory(username="bob"))
    bob = TicketFactory(contact=contact)
    contact = ContactFactory(user=UserFactory(username="sam"))
    sam = TicketFactory(contact=contact)
    # these time records are for a different user, so exclude them
    TimeRecordFactory(
        ticket=bob,
        billable=True,
        date_started=d,
        start_time=time(11, 0),
        end_time=time(11, 30),
    )
    TimeRecordFactory(
        ticket=sam,
        billable=False,
        date_started=d,
        start_time=time(10, 0),
        end_time=time(10, 15),
    )
    # include these time records
    TimeRecordFactory(
        ticket=bob,
        billable=True,
        date_started=d,
        start_time=time(11, 0),
        end_time=time(11, 30),
        user=user,
    )
    TimeRecordFactory(
        ticket=sam,
        billable=False,
        date_started=d,
        start_time=time(10, 0),
        end_time=time(10, 15),
        user=user,
    )
    data = TimeRecord.objects.report_time_for_user(from_date, to_date, user)
    assert {
        "bob": TimeAnalysis(charge=30, pending=30),
        "sam": TimeAnalysis(non_charge=15),
    } == data


@pytest.mark.django_db
def test_report_total_by_user():
    contact = ContactFactory()
    invoice = InvoiceFactory(contact=contact)
    InvoiceSettingsFactory()
    # no time records
    InvoiceLineFactory(invoice=invoice)
    # u1's time records
    invoice_line = InvoiceLineFactory(invoice=invoice)
    t1 = TicketFactory(contact=contact)
    TimeRecordFactory(
        ticket=t1, user=UserFactory(username="u1"), invoice_line=invoice_line
    )
    # u2's time records
    invoice_line = InvoiceLineFactory(invoice=invoice)
    u2 = UserFactory(username="u2")
    t2 = TicketFactory(contact=contact)
    TimeRecordFactory(ticket=t2, user=u2, invoice_line=invoice_line)
    invoice_line = InvoiceLineFactory(invoice=invoice)
    t3 = TicketFactory(contact=contact)
    TimeRecordFactory(ticket=t3, user=u2, invoice_line=invoice_line)
    result = invoice.time_analysis()
    # invoice has a line with no time records
    assert "" in result
    # fred recorded time on one ticket
    assert "u1" in result
    u1 = result["u1"]
    assert 1 == len(u1)
    assert t1.pk in u1
    # sara recorded time on two tickets
    assert "u2" in result
    u2 = result["u2"]
    assert 2 == len(u2)
    assert t2.pk in u2
    assert t3.pk in u2
    # web user added an invoice line, but didn't record time
    assert "web" not in result
    # check net total matches invoice
    net = Decimal()
    for user, tickets in result.items():
        for ticket_pk, totals in tickets.items():
            net = net + totals["net"]
    assert invoice.net == net


@pytest.mark.django_db
def test_time_summary():
    user = UserFactory(username="green", first_name="P", last_name="Kimber")
    today = date.today()
    contact = ContactFactory(
        user=UserFactory(username="orange", first_name="O", last_name="Rind"),
        company_name="",
    )
    t1 = TicketFactory(
        pk=1, contact=ContactFactory(user=user, company_name=""), title="Apple"
    )
    TimeRecordFactory(
        ticket=t1,
        date_started=today,
        start_time=time(11, 0),
        end_time=time(11, 30),
        user=user,
    )
    contact = ContactFactory(
        user=UserFactory(username="blue", first_name="A", last_name="Teal"),
        company_name="",
    )
    TimeRecordFactory(
        ticket=TicketFactory(pk=2, contact=contact, title="Orange"),
        date_started=today,
        start_time=time(10, 0),
        end_time=time(10, 15),
        user=user,
    )
    # another date (so not included)
    TimeRecordFactory(
        ticket=t1,
        date_started=today + relativedelta(days=-1),
        start_time=time(12, 0),
        end_time=time(12, 10),
        user=UserFactory(),
    )
    # another user (so not included)
    TimeRecordFactory(
        ticket=t1,
        date_started=today,
        start_time=time(12, 0),
        end_time=time(12, 10),
        user=UserFactory(),
    )
    TimeRecordFactory(
        ticket=t1,
        date_started=today,
        start_time=time(12, 0),
        end_time=time(12, 10),
        user=user,
    )
    data = time_summary(user)
    assert {
        today: TicketReport(
            tickets=[
                TicketSummary(
                    contact="P Kimber",
                    description="Apple",
                    pk=1,
                    user_name="green",
                    data=TimeAnalysis(charge=40, pending=40),
                ),
                TicketSummary(
                    contact="A Teal",
                    description="Orange",
                    pk=2,
                    user_name="blue",
                    data=TimeAnalysis(charge=15, pending=15),
                ),
            ],
            totals=TimeAnalysis(charge=55),
        )
    } == data
    totals = data[today].totals
    assert 55.0 == totals.total
    assert "00:55" == totals.total_fmt
    assert "00:55" == totals.charge_fmt
    assert "00:00" == totals.fixed_fmt
    assert "00:00" == totals.non_charge_fmt


@pytest.mark.django_db
def test_time_summary_with_current():
    user = UserFactory(username="green", first_name="P", last_name="Kimber")
    today = date.today()
    contact = ContactFactory(user=user, company_name="")
    ticket = TicketFactory(contact=contact, title="Colours")
    TimeRecordFactory(
        ticket=ticket,
        title="Apple",
        date_started=today,
        start_time=time(9, 0),
        end_time=time(10, 30),
        billable=True,
        user=user,
    )
    TimeRecordFactory(
        ticket=ticket,
        title="Orange",
        date_started=today,
        start_time=time(11, 0),
        end_time=None,
        billable=True,
        user=user,
    )
    data = time_summary(user)
    assert {
        today: TicketReport(
            tickets=[
                TicketSummary(
                    contact="P Kimber",
                    description="Colours",
                    pk=ticket.pk,
                    user_name="green",
                    data=TimeAnalysis(charge=90.0, pending=90.0),
                ),
            ],
            totals=TimeAnalysis(charge=90.0),
        )
    } == data
    totals = data[today].totals
    assert 90.0 == totals.total
    assert "01:30" == totals.total_fmt
    assert "01:30" == totals.charge_fmt
    assert "00:00" == totals.fixed_fmt
    assert "00:00" == totals.non_charge_fmt
