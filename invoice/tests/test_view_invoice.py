# -*- encoding: utf-8 -*-
import pytest

from decimal import Decimal
from django.urls import reverse
from django.utils import timezone
from http import HTTPStatus

from contact.tests.factories import ContactFactory
from invoice.models import Invoice, InvoiceLine
from finance.models import Currency
from finance.tests.factories import VatCode, VatSettingsFactory
from login.tests.factories import TEST_PASSWORD, UserFactory
from stock.tests.factories import ProductFactory
from .factories import (
    InvoiceContactFactory,
    InvoiceFactory,
    InvoiceSettingsFactory,
)


@pytest.mark.django_db
def test_invoice_create_draft(client):
    user = UserFactory(username="staff", is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    InvoiceSettingsFactory()
    VatSettingsFactory()
    currency = Currency.objects.get(slug=Currency.POUND)
    contact = ContactFactory()
    InvoiceContactFactory(contact=contact)
    assert 0 == Invoice.objects.count()
    url = reverse("invoice.create.draft", kwargs={"pk": contact.pk})
    data = {"invoice_date": timezone.now().date(), "currency": currency.pk}
    response = client.post(url, data)
    assert HTTPStatus.FOUND == response.status_code, response.context[
        "form"
    ].errors
    assert 1 == Invoice.objects.count()
    invoice = Invoice.objects.first()
    assert invoice.number == 1
    assert invoice.invoice_number == "000001"
    assert invoice.is_credit is False


@pytest.mark.django_db
def test_invoice_create_draft_credit(client):
    user = UserFactory(username="staff", is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    InvoiceSettingsFactory()
    VatSettingsFactory()
    currency = Currency.objects.get(slug=Currency.POUND)
    contact = ContactFactory()
    InvoiceContactFactory(contact=contact)
    assert 0 == Invoice.objects.count()
    url = reverse("invoice.create.draft", kwargs={"pk": contact.pk})
    data = {
        "invoice_date": timezone.now().date(),
        "currency": currency.pk,
        "is_credit": True,
    }
    response = client.post(url, data)
    assert HTTPStatus.FOUND == response.status_code, response.context[
        "form"
    ].errors
    assert 1 == Invoice.objects.count()
    invoice = Invoice.objects.first()
    assert invoice.number == 1
    assert invoice.invoice_number == "000001"
    assert invoice.is_credit is True


@pytest.mark.django_db
def test_invoice_create_draft_no_invoice_contact_get(client):
    user = UserFactory(username="staff", is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    InvoiceSettingsFactory()
    VatSettingsFactory()
    contact = ContactFactory(company_name="KB")
    url = reverse("invoice.create.draft", kwargs={"pk": contact.pk})
    response = client.get(url)
    assert HTTPStatus.OK == response.status_code
    assert "messages" in response.context
    messages = response.context["messages"]
    assert 1 == len(messages)
    assert ["Cannot create invoice - no hourly rate for KB"] == [
        str(x) for x in messages
    ]


@pytest.mark.django_db
def test_invoice_create_draft_no_invoice_contact_post(client):
    user = UserFactory(username="staff", is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    InvoiceSettingsFactory()
    VatSettingsFactory()
    contact = ContactFactory(company_name="KB")
    currency = Currency.objects.get(slug=Currency.POUND)
    url = reverse("invoice.create.draft", kwargs={"pk": contact.pk})
    data = {"invoice_date": timezone.now().date(), "currency": currency.pk}
    response = client.post(url, data)
    assert HTTPStatus.OK == response.status_code
    form = response.context["form"]
    assert {
        "__all__": ["Cannot create invoice - no hourly rate for KB"]
    } == form.errors


@pytest.mark.django_db
def test_invoice_line_create(client):
    user = UserFactory(username="staff", is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    InvoiceSettingsFactory()
    VatSettingsFactory()
    invoice = InvoiceFactory()
    assert 0 == InvoiceLine.objects.count()
    url = reverse("invoice.line.create", args=[invoice.pk])
    data = {
        "product": ProductFactory().pk,
        "description": "Apple",
        "price": Decimal("3"),
        "quantity": Decimal("1"),
        "units": "Each",
        "vat_code": VatCode.objects.get(slug=VatCode.STANDARD).pk,
    }
    response = client.post(url, data)
    assert HTTPStatus.FOUND == response.status_code, response.context[
        "form"
    ].errors
    assert 1 == InvoiceLine.objects.count()
