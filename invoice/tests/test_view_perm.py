# -*- encoding: utf-8 -*-
import pytest

from datetime import date
from django.urls import reverse
from http import HTTPStatus

from base.tests.test_utils import PermTestCase
from contact.tests.factories import ContactFactory
from crm.tests.factories import TicketFactory
from finance.models import Currency
from finance.tests.factories import VatSettingsFactory
from invoice.service import InvoiceCreate, InvoicePrint
from invoice.tests.factories import (
    InvoiceContactFactory,
    InvoiceFactory,
    InvoiceIssueFactory,
    InvoiceLineFactory,
    InvoiceSettingsFactory,
    QuickTimeRecordFactory,
    TimeRecordFactory,
)
from login.tests.factories import TEST_PASSWORD, UserFactory
from login.tests.fixture import perm_check
from report.tests.factories import ReportSpecificationFactory


@pytest.mark.django_db
def test_batch_list(perm_check):
    url = reverse("invoice.batch.list")
    perm_check.staff(url)


@pytest.mark.django_db
def test_batchless_list(perm_check):
    url = reverse("invoice.batch.batchless.list")
    perm_check.staff(url)


@pytest.mark.django_db
def test_contact_invoice_list(perm_check):
    contact = ContactFactory()
    url = reverse("invoice.contact.list", kwargs={"pk": contact.pk})
    perm_check.staff(url)


@pytest.mark.django_db
def test_contact_report(perm_check):
    contact = ContactFactory()
    report_specification = ReportSpecificationFactory()
    url = reverse(
        "invoice.contact.report",
        kwargs={"pk": contact.pk, "slug": report_specification.slug},
    )
    perm_check.staff(url)


@pytest.mark.django_db
def test_contact_time_record_analysis(perm_check):
    contact = ContactFactory()
    url = reverse("invoice.time.contact.ticket.time", kwargs={"pk": contact.pk})
    perm_check.staff(url)


@pytest.mark.django_db
def test_contact_time_record_list(perm_check):
    contact = ContactFactory()
    url = reverse("invoice.time.contact.list", kwargs={"pk": contact.pk})
    perm_check.staff(url)


@pytest.mark.django_db
def test_contact_time_record_list_outstanding(perm_check):
    contact = ContactFactory()
    url = reverse(
        "invoice.time.contact.outstanding.list", kwargs={"pk": contact.pk}
    )
    perm_check.staff(url)


@pytest.mark.django_db
def test_invoice_contact_create(perm_check):
    contact = ContactFactory()
    url = reverse("invoice.contact.create", kwargs={"pk": contact.pk})
    perm_check.staff(url)


@pytest.mark.django_db
def test_invoice_contact_update(perm_check):
    contact = ContactFactory()
    invoice_contact = InvoiceContactFactory(contact=contact)
    url = reverse("invoice.contact.update", kwargs={"pk": invoice_contact.pk})
    perm_check.staff(url)


@pytest.mark.django_db
def test_invoice_create_draft(perm_check):
    InvoiceSettingsFactory()
    VatSettingsFactory()
    contact = ContactFactory()
    InvoiceContactFactory(contact=contact)
    url = reverse("invoice.create.draft", kwargs={"pk": contact.pk})
    perm_check.staff(url)


@pytest.mark.django_db
def test_invoice_create_time(perm_check):
    InvoiceSettingsFactory()
    VatSettingsFactory()
    contact = ContactFactory()
    InvoiceContactFactory(contact=contact)
    url = reverse("invoice.create.time", kwargs={"pk": contact.pk})
    perm_check.staff(url)


@pytest.mark.django_db
def test_invoice_credit_list(perm_check):
    url = reverse("invoice.credit.list")
    perm_check.staff(url)


@pytest.mark.django_db
def test_invoice_issue_list(perm_check):
    url = reverse("invoice.issue.list")
    perm_check.staff(url)


@pytest.mark.django_db
def test_invoice_list(perm_check):
    url = reverse("invoice.list")
    perm_check.staff(url)


@pytest.mark.django_db
def test_invoice_download(perm_check):
    InvoiceSettingsFactory()
    VatSettingsFactory()
    contact = ContactFactory()
    currency = Currency.objects.get(slug=Currency.POUND)
    InvoiceContactFactory(contact=contact)
    ticket = TicketFactory(contact=contact)
    TimeRecordFactory(ticket=ticket, date_started=date(2013, 12, 1))
    invoice = InvoiceCreate().create(
        UserFactory(), contact, currency, date(2013, 12, 31)
    )
    InvoicePrint().create_pdf(invoice, header_image=None)
    url = reverse("invoice.download", kwargs={"pk": invoice.pk})
    perm_check.staff(url)


@pytest.mark.django_db
def test_invoice_detail(perm_check):
    invoice = InvoiceFactory()
    url = reverse("invoice.detail", kwargs={"pk": invoice.pk})
    perm_check.staff(url)


@pytest.mark.django_db
def test_invoice_line_create(perm_check):
    invoice = InvoiceFactory()
    url = reverse("invoice.line.create", kwargs={"pk": invoice.pk})
    perm_check.staff(url)


@pytest.mark.django_db
def test_invoice_line_update(perm_check):
    invoice = InvoiceFactory()
    invoice_line = InvoiceLineFactory(invoice=invoice)
    url = reverse("invoice.line.update", kwargs={"pk": invoice_line.pk})
    perm_check.staff(url)


@pytest.mark.django_db
def test_invoice_create_pdf(perm_check):
    invoice = InvoiceFactory()
    url = reverse("invoice.create.pdf", kwargs={"pk": invoice.pk})
    perm_check.staff(url)


@pytest.mark.django_db
def test_invoice_refresh_time_records(perm_check):
    invoice = InvoiceFactory()
    url = reverse("invoice.refresh.time.records", kwargs={"pk": invoice.pk})
    perm_check.staff(url)


@pytest.mark.django_db
def test_invoice_update(perm_check):
    invoice = InvoiceFactory()
    url = reverse("invoice.update", kwargs={"pk": invoice.pk})
    perm_check.staff(url)


@pytest.mark.django_db
def test_invoice_user_update(perm_check):
    url = reverse("invoice.user.update")
    perm_check.staff(url)


@pytest.mark.django_db
def test_quick_time_record(perm_check):
    perm_check.auth(reverse("invoice.quick.time.record.list"))


@pytest.mark.django_db
def test_quick_time_record_create(perm_check):
    perm_check.auth(reverse("invoice.quick.time.record.create"))


@pytest.mark.django_db
def test_quick_time_record_delete(client):
    user = UserFactory(is_staff=False)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    quick_time_record = QuickTimeRecordFactory(user=user)
    response = client.get(
        reverse("invoice.quick.time.record.delete", args=[quick_time_record.pk])
    )
    assert HTTPStatus.OK == response.status_code


@pytest.mark.django_db
def test_quick_time_record_delete_another_user(client):
    user = UserFactory(is_staff=False)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    quick_time_record = QuickTimeRecordFactory(user=UserFactory())
    response = client.get(
        reverse("invoice.quick.time.record.delete", args=[quick_time_record.pk])
    )
    assert HTTPStatus.FOUND == response.status_code
    assert reverse("login") in response.url


@pytest.mark.django_db
def test_quick_time_record_update(client):
    user = UserFactory(is_staff=False)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    quick_time_record = QuickTimeRecordFactory(user=user)
    response = client.get(
        reverse("invoice.quick.time.record.update", args=[quick_time_record.pk])
    )
    assert HTTPStatus.OK == response.status_code


@pytest.mark.django_db
def test_quick_time_record_update_another_user(client):
    user = UserFactory(is_staff=False)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    quick_time_record = QuickTimeRecordFactory(user=UserFactory())
    response = client.get(
        reverse("invoice.quick.time.record.update", args=[quick_time_record.pk])
    )
    assert HTTPStatus.FOUND == response.status_code
    assert reverse("login") in response.url


@pytest.mark.django_db
def test_settings(perm_check):
    url = reverse("invoice.settings.update")
    perm_check.superuser(url)


@pytest.mark.django_db
def test_timerecord_list(perm_check):
    url = reverse("invoice.time")
    perm_check.staff(url)


@pytest.mark.django_db
def test_timerecord_summary(perm_check):
    url = reverse("invoice.time.summary")
    perm_check.auth(url)


@pytest.mark.django_db
def test_timerecord_summary_for_user(client):
    user = UserFactory(is_staff=False)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    url = reverse("invoice.time.summary")
    response = client.get(url)
    assert HTTPStatus.OK == response.status_code


@pytest.mark.django_db
def test_timerecord_summary_user(perm_check):
    user = UserFactory()
    url = reverse("invoice.time.summary.user", args=[user.pk])
    perm_check.staff(url)


@pytest.mark.django_db
def test_timerecord_create(perm_check):
    ticket = TicketFactory()
    url = reverse("invoice.time.create", kwargs={"pk": ticket.pk})
    perm_check.staff(url)


@pytest.mark.django_db
def test_timerecord_create_for_user(client):
    user = UserFactory(is_staff=False)
    contact = ContactFactory(user=user)
    ticket = TicketFactory(contact=contact)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    url = reverse("invoice.time.create", kwargs={"pk": ticket.pk})
    response = client.get(url)
    assert HTTPStatus.OK == response.status_code


@pytest.mark.django_db
def test_ticket_list_month(perm_check):
    url = reverse("invoice.ticket.list.month", args=[2000, 3])
    perm_check.staff(url)


@pytest.mark.django_db
def test_ticket_timerecord_list(perm_check):
    ticket = TicketFactory()
    url = reverse("invoice.time.ticket.list", kwargs={"pk": ticket.pk})
    perm_check.staff(url)


@pytest.mark.django_db
def test_user_timerecord_list(perm_check):
    url = reverse("invoice.time.user.list")
    perm_check.staff(url)


@pytest.mark.django_db
def test_timerecord_outstanding_list(perm_check):
    url = reverse("invoice.time.outstanding.list")
    perm_check.staff(url)


@pytest.mark.django_db
def test_timerecord_outstanding_list_bill_free(perm_check):
    url = reverse("invoice.time.outstanding.bill.free.list")
    perm_check.staff(url)


@pytest.mark.django_db
def test_timerecord_update(perm_check):
    time_record = TimeRecordFactory()
    url = reverse("invoice.time.update", kwargs={"pk": time_record.pk})
    perm_check.staff(url)


@pytest.mark.django_db
def test_issue_update(perm_check):
    issue = InvoiceIssueFactory()
    url = reverse("invoice.issue.update", kwargs={"pk": issue.pk})
    perm_check.staff(url)


@pytest.mark.django_db
def test_timerecord_update_for_user(client):
    user = UserFactory(is_staff=False)
    contact = ContactFactory(user=user)
    ticket = TicketFactory(contact=contact)
    time_record = TimeRecordFactory(ticket=ticket)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    url = reverse("invoice.time.update", kwargs={"pk": time_record.pk})
    response = client.get(url)
    assert HTTPStatus.OK == response.status_code
