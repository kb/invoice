# -*- encoding: utf-8 -*-
import pandas
import pytest

from datetime import date, time
from decimal import Decimal

from contact.tests.factories import ContactFactory
from crm.tests.factories import TicketFactory
from finance.models import Currency
from finance.tests.factories import VatSettingsFactory
from invoice.analysis import sales_order_invoice, sales_order_time
from invoice.models import InvoiceError
from invoice.service import InvoiceCreate
from invoice.tests.factories import (
    InvoiceContactFactory,
    InvoiceLineFactory,
    InvoiceSettingsFactory,
    TimeCodeFactory,
    TimeRecordFactory,
)
from login.tests.factories import UserFactory
from stock.tests.factories import ProductFactory
from sales_order.models import SalesOrder
from sales_order.tests.factories import SalesOrderFactory
from sales_order_ticket.tests.factories import SalesOrderTicketFactory


@pytest.mark.django_db
def test_sales_order_invoice():
    contact = ContactFactory(user=UserFactory(username="patrick"))
    ticket_1 = TicketFactory(contact=contact, title="ticket-1")
    ticket_2 = TicketFactory(contact=contact, title="ticket-2")
    ticket_3 = TicketFactory(contact=contact, title="ticket-3")
    sales_order_1 = SalesOrderFactory(
        contact=contact, hours_type=SalesOrder.HOURS_TOTAL
    )
    sales_order_2 = SalesOrderFactory(
        contact=contact, hours_type=SalesOrder.HOURS_TOTAL
    )
    time_code = TimeCodeFactory(description="Talk")
    SalesOrderTicketFactory(ticket=ticket_1, sales_order=sales_order_1)
    SalesOrderTicketFactory(ticket=ticket_2, sales_order=sales_order_1)
    SalesOrderTicketFactory(ticket=ticket_3, sales_order=sales_order_2)
    time_record_product = ProductFactory(price=Decimal(20.00))
    InvoiceSettingsFactory(time_record_product=time_record_product)
    InvoiceContactFactory(contact=contact, hourly_rate=Decimal("20"))
    VatSettingsFactory()
    TimeRecordFactory(
        billable=True,
        date_started=date(2012, 7, 2),
        start_time=time(13, 0),
        end_time=time(13, 30),
        ticket=ticket_2,
        time_code=time_code,
    )
    # time record has already been invoiced
    TimeRecordFactory(
        billable=True,
        invoice_line=InvoiceLineFactory(),
        date_started=date(2012, 7, 2),
        start_time=time(11, 0),
        end_time=time(12, 0),
        ticket=ticket_1,
        time_code=time_code,
    )
    TimeRecordFactory(
        billable=True,
        date_started=date(2012, 7, 1),
        start_time=time(13, 0),
        end_time=time(15, 0),
        ticket=ticket_1,
        time_code=time_code,
    )
    TimeRecordFactory(
        billable=True,
        date_started=date(2012, 7, 1),
        start_time=time(13, 0),
        end_time=time(14, 0),
        ticket=ticket_2,
        time_code=time_code,
    )
    TimeRecordFactory(
        billable=True,
        date_started=date(2012, 7, 5),
        start_time=time(13, 0),
        end_time=time(14, 0),
        ticket=ticket_3,
        time_code=time_code,
    )
    from rich.pretty import pprint
    from rich import print as rprint

    time_data = sales_order_time()
    # pprint(time_data, expand_all=True)
    invoice_data = sales_order_invoice(time_data)
    # pprint(invoice_data, expand_all=True)
    # sales_order, ticket, minutes
    # 26/07/2023, Remove ref WIP
    # assert [
    #     (1457, 218, 120),
    #     (1458, 218, 90),
    #     (1459, 219, 60),
    # ] == invoice_data


@pytest.mark.django_db
def test_sales_order_time():
    contact = ContactFactory(user=UserFactory(username="patrick"))
    ticket_1 = TicketFactory(contact=contact, title="ticket-1")
    # ticket_2 = TicketFactory(contact=contact, title="ticket-2")
    sales_order = SalesOrderFactory(
        contact=contact, hours_type=SalesOrder.HOURS_TOTAL
    )
    time_code = TimeCodeFactory(description="Talk")
    SalesOrderTicketFactory(ticket=ticket_1, sales_order=sales_order)
    # SalesOrderTicketFactory(ticket=ticket_2, sales_order=sales_order)
    # time record has already been invoiced
    TimeRecordFactory(
        billable=True,
        invoice_line=InvoiceLineFactory(),
        date_started=date(2012, 7, 2),
        start_time=time(11, 0),
        end_time=time(11, 20),
        ticket=ticket_1,
        time_code=time_code,
    )
    # time record is complete, so include
    TimeRecordFactory(
        billable=True,
        date_started=date(2012, 7, 1),
        start_time=time(11, 0),
        end_time=time(11, 10),
        ticket=ticket_1,
        time_code=time_code,
    )
    # time record not complete, so do not include
    TimeRecordFactory(
        billable=True,
        date_started=date(2012, 7, 1),
        start_time=time(11, 0),
        end_time=None,
        ticket=ticket_1,
        time_code=time_code,
    )
    data = sales_order_time()
    assert [
        [
            "sales-order",
            # "sales-order-hours-type",
            # "sales-order-hours",
            "ticket",
            "title",
            "user",
            "time-code",
            "date-started",
            "invoiced",
            "minutes",
        ],
        [
            sales_order.pk,
            # "hours-total",
            ticket_1.pk,
            "ticket-1",
            "patrick",
            "Talk",
            date(2012, 7, 1),
            False,
            10.0,
        ],
        [
            sales_order.pk,
            # "hours-total",
            ticket_1.pk,
            "ticket-1",
            "patrick",
            "Talk",
            date(2012, 7, 2),
            True,
            20.0,
        ],
    ] == data


@pytest.mark.django_db
def test_outstanding_time_missing_sales_order():
    ticket = TicketFactory(contact=ContactFactory())
    TimeRecordFactory(billable=True, ticket=ticket, user=UserFactory())
    with pytest.raises(InvoiceError) as e:
        sales_order_time()
    assert (
        "The following tickets need to be "
        f"linked to a sales order: {ticket.pk}"
    ) in str(e.value)


@pytest.mark.django_db
def test_sales_order_time_vs_legacy_invoice():
    contact = ContactFactory(user=UserFactory(username="patrick"))
    ticket_1 = TicketFactory(contact=contact, title="ticket-1")
    ticket_2 = TicketFactory(contact=contact, title="ticket-2")
    ticket_3 = TicketFactory(contact=contact, title="ticket-3")
    sales_order_1 = SalesOrderFactory(
        contact=contact, hours_type=SalesOrder.HOURS_TOTAL
    )
    sales_order_2 = SalesOrderFactory(
        contact=contact, hours_type=SalesOrder.HOURS_TOTAL
    )
    time_code = TimeCodeFactory(description="Talk")
    SalesOrderTicketFactory(ticket=ticket_1, sales_order=sales_order_1)
    SalesOrderTicketFactory(ticket=ticket_2, sales_order=sales_order_1)
    SalesOrderTicketFactory(ticket=ticket_3, sales_order=sales_order_2)
    currency = Currency.objects.get(slug=Currency.POUND)
    time_record_product = ProductFactory(price=Decimal(20.00))
    InvoiceSettingsFactory(time_record_product=time_record_product)
    InvoiceContactFactory(contact=contact, hourly_rate=Decimal("20"))
    VatSettingsFactory()
    TimeRecordFactory(
        billable=True,
        date_started=date(2012, 7, 2),
        start_time=time(13, 0),
        end_time=time(13, 30),
        ticket=ticket_2,
        time_code=time_code,
    )
    # time record has already been invoiced
    TimeRecordFactory(
        billable=True,
        invoice_line=InvoiceLineFactory(),
        date_started=date(2012, 7, 2),
        start_time=time(11, 0),
        end_time=time(12, 0),
        ticket=ticket_1,
        time_code=time_code,
    )
    TimeRecordFactory(
        billable=True,
        date_started=date(2012, 7, 1),
        start_time=time(13, 0),
        end_time=time(15, 0),
        ticket=ticket_1,
        time_code=time_code,
    )
    TimeRecordFactory(
        billable=True,
        date_started=date(2012, 7, 1),
        start_time=time(13, 0),
        end_time=time(14, 0),
        ticket=ticket_2,
        time_code=time_code,
    )
    TimeRecordFactory(
        billable=True,
        date_started=date(2012, 7, 5),
        start_time=time(13, 0),
        end_time=time(14, 0),
        ticket=ticket_3,
        time_code=time_code,
    )
    data = sales_order_time()
    from rich.pretty import pprint
    from rich import print as rprint

    pprint(data, expand_all=True)
    df = pandas.DataFrame(data[1:], columns=data[0])
    pprint(df, expand_all=True)
    rprint("[yellow]calculate total using conditions")
    # 'minutes' where 'invoiced is False'
    total_to_invoice = df.loc[df["invoiced"] == False, "minutes"].sum()
    pprint({"total_to_invoice": total_to_invoice}, expand_all=True)
    #
    rprint("[yellow]calculate total using a new dataframe")
    # can we create the invoice from this dataframe?
    to_invoice = df.loc[df["invoiced"] == False]
    pprint(to_invoice, expand_all=True)

    rprint("[yellow]calculate invoice total using a pivot table")
    to_invoice_grouped = pandas.pivot_table(
        to_invoice,
        values=["minutes"],
        index=["ticket"],
        columns=["sales-order"],
        aggfunc="sum",
    )
    pprint(to_invoice_grouped, expand_all=True)
    rprint("[yellow]calculate invoice total using a pivot table ('stack')")
    pprint(to_invoice_grouped.stack(), expand_all=True)

    rprint("[yellow]sales order - already invoiced")
    sales_orders_invoiced = df.loc[df["invoiced"] == True]
    pprint(
        pandas.pivot_table(
            sales_orders_invoiced,
            values=["minutes"],
            index=["sales-order"],
            aggfunc="sum",
        ).stack(),
        expand_all=True,
    )
    rprint("[yellow]sales order - to invoice")
    pprint(
        pandas.pivot_table(
            to_invoice,
            values=["minutes"],
            index=["sales-order"],
            aggfunc="sum",
        ).stack(),
        expand_all=True,
    )

    # check total vs legacy invoice
    total_to_invoice = to_invoice["minutes"].sum()
    pprint({"total_to_invoice": total_to_invoice}, expand_all=True)
    # check our new total matches the legacy invoice total
    invoice = InvoiceCreate().create(
        UserFactory(), contact, currency, date.today()
    )

    hours = Decimal(total_to_invoice.item()) / Decimal("60")
    assert invoice.net == hours * time_record_product.price
