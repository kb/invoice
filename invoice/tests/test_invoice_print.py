# -*- encoding: utf-8 -*-
import pytest

from datetime import date
from decimal import Decimal
from freezegun import freeze_time

from contact.tests.factories import ContactFactory, ContactAddressFactory
from crm.tests.factories import TicketFactory
from finance.models import Currency
from finance.tests.factories import VatSettingsFactory
from invoice.models import InvoiceError
from invoice.service import InvoiceCreate, InvoicePrint
from invoice.tests.factories import (
    InvoiceContactFactory,
    InvoiceFactory,
    InvoiceLineFactory,
    InvoiceSettingsFactory,
    TimeRecordFactory,
)
from login.tests.factories import UserFactory


@pytest.mark.django_db
def test_text_invoice_address():
    contact = ContactFactory(company_name="KB")
    ContactAddressFactory(
        contact=contact,
        address_one="1",
        address_two="2",
        locality="3",
        town="T",
        admin_area="C",
        postal_code="P",
        country="UK",
    )
    invoice = InvoiceFactory(contact=contact)
    assert (
        "KB<br />1<br />2<br />3<br />T<br />" "C<br />P<br />UK"
    ) == InvoicePrint()._text_invoice_address(invoice)


@pytest.mark.django_db
def test_text_invoice_address_missing_data():
    contact = ContactFactory(company_name="KB")
    ContactAddressFactory(
        contact=contact,
        address_one="",
        address_two="2",
        locality="",
        town="T",
        admin_area="",
        postal_code="P",
        country="",
    )
    invoice = InvoiceFactory(contact=contact)
    assert ("KB<br />2<br />T<br />P") == InvoicePrint()._text_invoice_address(
        invoice
    )


@pytest.mark.django_db
def test_text_invoice_address_no_company_name():
    contact = ContactFactory(company_name="KB")
    ContactAddressFactory(
        contact=contact,
        address_one="1",
        address_two="2",
        locality="3",
        town="T",
        admin_area="C",
        postal_code="P",
        country="UK",
    )
    invoice = InvoiceFactory(contact=contact)
    assert (
        "KB<br />1<br />2<br />3<br />T<br />" "C<br />P<br />UK"
    ) == InvoicePrint()._text_invoice_address(invoice)


@pytest.mark.django_db
def test_invoice_create_pdf():
    InvoiceSettingsFactory()
    VatSettingsFactory()
    contact = ContactFactory()
    currency = Currency.objects.get(slug=Currency.POUND)
    InvoiceContactFactory(contact=contact)
    ticket = TicketFactory(contact=contact)
    TimeRecordFactory(ticket=ticket, date_started=date(2013, 12, 1))
    invoice = InvoiceCreate().create(
        UserFactory(), contact, currency, date(2013, 12, 31)
    )
    assert invoice.is_draft is True
    with freeze_time("2017-05-21"):
        InvoicePrint().create_pdf(invoice, None)
    invoice.refresh_from_db()
    assert "invoice/2017/05/21/" in invoice.pdf.name
    assert invoice.is_draft is False
    assert invoice.is_credit is False


@pytest.mark.django_db
def test_invoice_create_pdf_credit_note():
    InvoiceSettingsFactory()
    VatSettingsFactory()
    credit_note = InvoiceFactory(is_credit=True)
    credit_note.full_clean()
    line = InvoiceLineFactory(
        invoice=credit_note, price=Decimal("1.01"), quantity=Decimal("-1")
    )
    line.full_clean()
    assert credit_note.is_draft is True
    InvoicePrint().create_pdf(credit_note, None)
    credit_note.refresh_from_db()
    assert credit_note.is_draft is False
    assert credit_note.is_credit is True


@pytest.mark.django_db
def test_invoice_create_pdf_no_lines():
    """Cannot create a PDF if the invoice has no lines"""
    invoice = InvoiceFactory()
    with pytest.raises(InvoiceError) as e:
        InvoicePrint().create_pdf(invoice, None)
    assert "has no lines - cannot create PDF" in str(e.value)
    invoice.refresh_from_db()
    assert "" == invoice.pdf.name
    assert invoice.is_draft is True


@pytest.mark.django_db
def test_invoice_create_pdf_not_draft():
    """Cannot create a PDF if the invoice has already been printed"""
    InvoiceSettingsFactory()
    VatSettingsFactory()
    invoice = InvoiceFactory()
    InvoiceLineFactory(invoice=invoice)
    InvoicePrint().create_pdf(invoice, None)
    with pytest.raises(InvoiceError) as e:
        InvoicePrint().create_pdf(invoice, None)
    assert "is not a draft invoice - cannot create a PDF" in str(e.value)
