# -*- encoding: utf-8 -*-
from reportlab import platypus
from reportlab.lib import colors
from reportlab.lib.pagesizes import A4
from report.pdf import PDFReport, NumberedCanvas
from reportlab.platypus import Paragraph

from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
from django.utils import timezone

from contact.models import Contact
from invoice.models import TimeAnalysis, TimeRecord


class TicketTimeReport(PDFReport):
    def _get_column_styles(self, column_widths):
        # style - add vertical grid lines
        style = []
        for idx in range(len(column_widths) - 1):
            style.append(
                (
                    "LINEAFTER",
                    (idx, 0),
                    (idx, -1),
                    self.GRID_LINE_WIDTH,
                    colors.gray,
                )
            )
        return style

    def _table(self, data):
        column_widths = [30, 270, 70, 70]
        style = [
            ("ALIGN", (1, 0), (-1, -1), "RIGHT"),
            ("LINEBELOW", (0, 0), (-1, 0), self.GRID_LINE_WIDTH, colors.gray),
            ("LINEABOVE", (0, 0), (-1, 0), 1, colors.black),
        ]
        style = style + self._get_column_styles(column_widths)
        return platypus.Table(
            data,
            colWidths=column_widths,
            repeatRows=1,
            style=style,
        )

    def _totaltable(self, totaldata):
        column_widths = [30]
        style = [
            ("ALIGN", (1, 0), (-1, -1), "RIGHT"),
            ("LINEBELOW", (0, 0), (-1, 0), self.GRID_LINE_WIDTH, colors.gray),
            ("LINEABOVE", (0, 0), (-1, 0), 1, colors.black),
        ]
        style = style + self._get_column_styles(column_widths)
        return platypus.Table(
            totaldata,
            colWidths=column_widths,
            repeatRows=1,
            style=style,
        )

    def create_pdf(self, title, name):
        doc = platypus.SimpleDocTemplate(
            "ticket-time-report.pdf", title=title, pagesize=A4
        )
        elements = []
        report_title = Paragraph(name + "'s Weekly Work Hours")
        elements.append(report_title)
        elements.append(self._table(self.ticketdata(name)))
        doc.build(elements, canvasmaker=NumberedCanvas)

    def ticketdata(self, name):
        result = TicketTimeReport().data(name)
        totaltime = self.totaldata(name)
        data = [["No", self._para("Description"), "Date", "Non", "Fix", "Chg"]]
        lines = []
        ticket_dict = result["data"]
        total = result["total"]
        # for data, dates in result.items():
        for ticket_date, tickets_data in ticket_dict.items():
            lines.append(
                [
                    "",
                    self._bold(ticket_date.strftime("%A")),
                    self._bold(ticket_date.strftime("%d/%m/%Y")),
                    "",
                ]
            )
            tickets = tickets_data["tickets"]
            # day_total = tickets_data["tickets"]["total"]
            # totaldaytime = TimeAnalysis()
            # Tickets + Ticket information
            for ticket_number, ticket_data in tickets.items():
                # for ticket_number, ticket_data in info.items():
                # totaldaytime.add
                # timetotal = str(timedelta(minutes=info["total"]))[:-3]
                time_analysis = ticket_data["total"]
                lines.append(
                    [
                        ticket_number,
                        self._para(ticket_data["title"]),
                        "",
                        time_analysis.non_charge_fmt,
                        time_analysis.fixed_fmt,
                        time_analysis.charge_fmt,
                    ]
                )
                # day_time = str(timedelta(minutes=totaldaytime))[:-3]
            lines.append(
                [
                    "",
                    "",
                    self._bold("Total Time"),
                    self._bold(
                        tickets_data["total"].non_charge_fmt,
                        align=PDFReport.RIGHT,
                    ),
                    self._bold(
                        tickets_data["total"].fixed_fmt, align=PDFReport.RIGHT
                    ),
                    self._bold(
                        tickets_data["total"].charge_fmt, align=PDFReport.RIGHT
                    ),
                ]
            )
        lines.append(
            [
                "",
                "",
                self._bold("Combined Total Time"),
                self._bold(total.non_charge_fmt, align=PDFReport.RIGHT),
                self._bold(total.fixed_fmt, align=PDFReport.RIGHT),
                self._bold(total.charge_fmt, align=PDFReport.RIGHT),
            ]
        )
        # return lines
        return data + lines

    def data(self, username):
        result = {}
        to_date = timezone.now()
        from_date = to_date + relativedelta(weeks=-3)
        contact = Contact.objects.get(user__username=username)
        qs = TimeRecord.objects.filter(
            date_started__gte=from_date,
            date_started__lte=to_date,
            user=contact.user,
        ).order_by("date_started")
        result = {"data": {}, "total": TimeAnalysis()}
        for time_record in qs:
            if time_record.end_time:
                # grand total
                result["total"].add(time_record)
                if time_record.date_started in result["data"]:
                    pass
                else:
                    result["data"][time_record.date_started] = {
                        "tickets": {},
                        "total": TimeAnalysis(),
                    }
                # total for date
                result["data"][time_record.date_started]["total"].add(
                    time_record
                )
                row = result["data"][time_record.date_started]["tickets"]
                if time_record.ticket.pk in row:
                    pass
                else:
                    row[time_record.ticket.pk] = {
                        "title": time_record.ticket.title,
                        "total": TimeAnalysis(),  # time_analysis
                    }
                # total for ticket
                row[time_record.ticket.pk]["total"].add(time_record)
        return result

    def totaldata(self, name):
        table_data = []
        to_date = timezone.now()
        from_date = to_date + relativedelta(weeks=-1)
        contact = Contact.objects.get(user__username=name)
        qs = TimeRecord.objects.filter(
            date_started__gte=from_date,
            date_started__lte=to_date,
            user=contact.user,
        )
        week_time = 0
        for time_record in qs:
            if time_record.end_time:
                total_time = time_record.minutes
                total = datetime.combine(
                    time_record.date_started, time_record.end_time
                ) - datetime.combine(
                    time_record.date_started, time_record.start_time
                )

                week_time = week_time + total.total_seconds()
        week_time = timedelta(seconds=week_time)
        totalstr = str(week_time).split(".")[0]
        table_data.append({"totaltime": totalstr})

        return table_data
