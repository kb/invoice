# -*- encoding: utf-8 -*-
from django.urls import path, re_path


from .views import (
    BatchlessInvoiceListView,
    BatchListView,
    ContactInvoiceListView,
    ContactReportUpdateView,
    ContactTicketTimeRecordListView,
    ContactTimeRecordListView,
    ContactTimeRecordOutstandingListView,
    invoice_download,
    InvoiceContactCreateView,
    InvoiceContactUpdateView,
    InvoiceCreditListView,
    InvoiceDraftCreateView,
    InvoiceIssueListView,
    InvoiceIssueUpdateView,
    InvoiceLineCreateView,
    InvoiceLineUpdateView,
    InvoicePdfUpdateView,
    InvoiceRefreshTimeRecordsUpdateView,
    InvoiceRemoveTimeRecordsUpdateView,
    InvoiceSettingsUpdateView,
    InvoiceSetToDraftUpdateView,
    InvoiceTimeCreateView,
    InvoiceUpdateView,
    InvoiceUserUpdateView,
    QuickTimeRecordCreateView,
    QuickTimeRecordDeleteView,
    QuickTimeRecordListView,
    QuickTimeRecordUpdateView,
    ReconcileDayView,
    report_invoice_time_analysis,
    report_invoice_time_analysis_csv,
    TicketListMonthView,
    TicketTimeRecordListView,
    TimeRecordCreateView,
    TimeRecordListView,
    TimeRecordOutstandingBillFreeListView,
    TimeRecordOutstandingListView,
    TimeRecordSummaryUserView,
    TimeRecordSummaryView,
    TimeRecordUpdateView,
    UserTimeRecordListView,
)


urlpatterns = [
    re_path(
        r"^batch/$",
        view=BatchListView.as_view(),
        name="invoice.batch.list",
    ),
    re_path(
        r"^batch/batchless/invoices/$",
        view=BatchlessInvoiceListView.as_view(),
        name="invoice.batch.batchless.list",
    ),
    re_path(
        r"^contact/(?P<pk>\d+)/create/$",
        view=InvoiceContactCreateView.as_view(),
        name="invoice.contact.create",
    ),
    re_path(
        r"^contact/(?P<pk>\d+)/update/$",
        view=InvoiceContactUpdateView.as_view(),
        name="invoice.contact.update",
    ),
    re_path(
        r"^contact/(?P<pk>\d+)/invoice/$",
        view=ContactInvoiceListView.as_view(),
        name="invoice.contact.list",
    ),
    re_path(
        r"^contact/(?P<pk>\d+)/report/(?P<slug>[-\w\d]+)/$",
        view=ContactReportUpdateView.as_view(),
        name="invoice.contact.report",
    ),
    re_path(
        r"^contact/(?P<pk>\d+)/ticket/time/$",
        view=ContactTicketTimeRecordListView.as_view(),
        name="invoice.time.contact.ticket.time",
    ),
    re_path(
        r"^contact/(?P<pk>\d+)/time/$",
        view=ContactTimeRecordListView.as_view(),
        name="invoice.time.contact.list",
    ),
    re_path(
        r"^contact/(?P<pk>\d+)/time/outstanding/$",
        view=ContactTimeRecordOutstandingListView.as_view(),
        name="invoice.time.contact.outstanding.list",
    ),
    re_path(
        r"^credit/$",
        view=InvoiceCreditListView.as_view(),
        name="invoice.credit.list",
    ),
    re_path(
        r"^create/(?P<pk>\d+)/draft/$",
        view=InvoiceDraftCreateView.as_view(),
        name="invoice.create.draft",
    ),
    re_path(
        r"^create/(?P<pk>\d+)/time/$",
        view=InvoiceTimeCreateView.as_view(),
        name="invoice.create.time",
    ),
    re_path(
        r"^download/(?P<pk>\d+)/$",
        view=invoice_download,
        name="invoice.download",
    ),
    re_path(
        r"^(?P<pk>\d+)/line/create/$",
        view=InvoiceLineCreateView.as_view(),
        name="invoice.line.create",
    ),
    re_path(
        r"^(?P<pk>\d+)/line/update/$",
        view=InvoiceLineUpdateView.as_view(),
        name="invoice.line.update",
    ),
    re_path(
        r"^(?P<pk>\d+)/pdf/$",
        view=InvoicePdfUpdateView.as_view(),
        name="invoice.create.pdf",
    ),
    re_path(
        r"^(?P<pk>\d+)/refresh-time-records/$",
        view=InvoiceRefreshTimeRecordsUpdateView.as_view(),
        name="invoice.refresh.time.records",
    ),
    re_path(
        r"^(?P<pk>\d+)/remove-time-records/$",
        view=InvoiceRemoveTimeRecordsUpdateView.as_view(),
        name="invoice.remove.time.records",
    ),
    re_path(
        r"^(?P<pk>\d+)/report/$",
        view=report_invoice_time_analysis,
        name="invoice.report.time.analysis",
    ),
    re_path(
        r"^(?P<pk>\d+)/csv/$",
        view=report_invoice_time_analysis_csv,
        name="invoice.report.time.analysis_csv",
    ),
    re_path(
        r"^(?P<pk>\d+)/set-to-draft/$",
        view=InvoiceSetToDraftUpdateView.as_view(),
        name="invoice.set.to.draft",
    ),
    re_path(
        r"^(?P<pk>\d+)/update/$",
        view=InvoiceUpdateView.as_view(),
        name="invoice.update",
    ),
    re_path(
        r"^issue/$",
        view=InvoiceIssueListView.as_view(),
        name="invoice.issue.list",
    ),
    # quick time record
    re_path(
        r"^quick/time/record/$",
        view=QuickTimeRecordListView.as_view(),
        name="invoice.quick.time.record.list",
    ),
    re_path(
        r"^quick/time/record/create/$",
        view=QuickTimeRecordCreateView.as_view(),
        name="invoice.quick.time.record.create",
    ),
    re_path(
        r"^quick/time/record/(?P<pk>\d+)/update/$",
        view=QuickTimeRecordUpdateView.as_view(),
        name="invoice.quick.time.record.update",
    ),
    re_path(
        r"^quick/time/record/(?P<pk>\d+)/delete/$",
        view=QuickTimeRecordDeleteView.as_view(),
        name="invoice.quick.time.record.delete",
    ),
    path(
        "reconcile/<int:year>/<int:month>/<int:day>/",
        ReconcileDayView.as_view(),
        name="invoice.reconcile.day",
    ),
    re_path(
        r"^issue/(?P<pk>\d+)/update/$",
        view=InvoiceIssueUpdateView.as_view(),
        name="invoice.issue.update",
    ),
    # settings
    re_path(
        r"^settings/update/$",
        view=InvoiceSettingsUpdateView.as_view(),
        name="invoice.settings.update",
    ),
    re_path(
        r"^user/update/$",
        view=InvoiceUserUpdateView.as_view(),
        name="invoice.user.update",
    ),
    # ticket
    re_path(
        r"^ticket/(?P<year>\d{4})/(?P<month>\d+)/$",
        view=TicketListMonthView.as_view(),
        name="invoice.ticket.list.month",
    ),
    re_path(
        r"^ticket/(?P<pk>\d+)/time/$",
        view=TicketTimeRecordListView.as_view(),
        name="invoice.time.ticket.list",
    ),
    re_path(
        r"^user/time/$",
        view=UserTimeRecordListView.as_view(),
        name="invoice.time.user.list",
    ),
    re_path(
        r"^ticket/(?P<pk>\d+)/time/add/$",
        view=TimeRecordCreateView.as_view(),
        name="invoice.time.create",
    ),
    re_path(r"^time/$", view=TimeRecordListView.as_view(), name="invoice.time"),
    re_path(
        r"^time/outstanding/$",
        view=TimeRecordOutstandingListView.as_view(),
        name="invoice.time.outstanding.list",
    ),
    re_path(
        r"^time/outstanding/bill/free/$",
        view=TimeRecordOutstandingBillFreeListView.as_view(),
        name="invoice.time.outstanding.bill.free.list",
    ),
    re_path(
        r"^time/summary/$",
        view=TimeRecordSummaryView.as_view(),
        name="invoice.time.summary",
    ),
    re_path(
        r"^time/summary/(?P<pk>\d+)/$",
        view=TimeRecordSummaryUserView.as_view(),
        name="invoice.time.summary.user",
    ),
    re_path(
        r"^time/(?P<pk>\d+)/update/$",
        view=TimeRecordUpdateView.as_view(),
        name="invoice.time.update",
    ),
]
