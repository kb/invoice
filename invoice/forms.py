# -*- encoding: utf-8 -*-
from datetime import date
from django import forms

from base.form_utils import RequiredFieldForm
from finance.models import Currency
from .models import (
    Batch,
    get_contact_model,
    Invoice,
    InvoiceContact,
    InvoiceIssue,
    InvoiceLine,
    InvoiceSettings,
    InvoiceUser,
    PaymentProcessor,
    QuickTimeRecord,
    TimeRecord,
)
from .service import InvoiceCreate


class BatchEmptyForm(forms.ModelForm):
    class Meta:
        model = Batch
        fields = ()


class ContactEmptyForm(forms.ModelForm):
    class Meta:
        model = get_contact_model()
        fields = ()


class InvoiceContactForm(forms.ModelForm):
    class Meta:
        model = InvoiceContact
        fields = ("hourly_rate",)


class InvoiceBlankTodayForm(RequiredFieldForm):
    iteration_end = forms.DateField()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        iteration_end = self.fields["iteration_end"]
        iteration_end.initial = date.today()
        # if we are updating an invoice, then hide the currency field
        if self.instance.pk:
            self.fields.pop("currency")

    class Meta:
        model = Invoice
        fields = ("currency", "iteration_end")


class InvoiceIssueForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["comment"].widget.attrs.update(
            {"class": "pure-input-1", "rows": 5}
        )
        self.fields["confirmed"].label = "Issue resolved"

    class Meta:
        model = InvoiceIssue
        fields = ("confirmed", "comment")


class InvoiceEmptyForm(forms.ModelForm):
    class Meta:
        model = Invoice
        fields = ()


class InvoiceLineForm(forms.ModelForm):
    class Meta:
        model = InvoiceLine
        fields = (
            "product",
            "description",
            "quantity",
            "units",
            "price",
            "vat_code",
        )


class InvoiceSettingsForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        f = self.fields["time_record_product"]
        f.widget.attrs.update({"class": "chosen-select"})
        for name in ("name_and_address", "footer"):
            self.fields[name].widget.attrs.update(
                {"class": "pure-input-1", "rows": 6}
            )

    class Meta:
        model = InvoiceSettings
        fields = (
            "name_and_address",
            "phone_number",
            "footer",
            "time_record_product",
        )


class InvoiceUpdateForm(RequiredFieldForm):
    def __init__(self, *args, **kwargs):
        self.contact = kwargs.pop("contact")
        super().__init__(*args, **kwargs)

    class Meta:
        model = Invoice
        fields = ("invoice_date", "is_credit", "currency")

    def clean(self):
        cleaned_data = super().clean()
        warnings = InvoiceCreate().is_valid(self.contact)
        for message in warnings:
            raise forms.ValidationError(
                message, code="invoice_update__is_valid"
            )
        return cleaned_data


class InvoiceUserUpdateForm(forms.ModelForm):
    class Meta:
        model = InvoiceUser
        fields = ("mail_time_summary",)


class QuickTimeRecordEmptyForm(forms.ModelForm):
    class Meta:
        model = QuickTimeRecord
        fields = ()


class QuickTimeRecordForm(RequiredFieldForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for name in ("description",):
            self.fields[name].widget.attrs.update({"class": "pure-input-2-3"})

    class Meta:
        model = QuickTimeRecord
        fields = ("description", "time_code", "chargeable")


class TimeRecordEmptyForm(forms.ModelForm):
    class Meta:
        model = TimeRecord
        fields = ()


class TimeRecordForm(RequiredFieldForm):
    paginate_by = 20

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["title"].widget.attrs.update({"class": "pure-input-1"})
        self.fields["description"].widget.attrs.update(
            {"class": "pure-input-1", "rows": 10}
        )

    class Meta:
        model = TimeRecord
        fields = (
            "title",
            "description",
            "time_code",
            "date_started",
            "start_time",
            "end_time",
            "billable",
        )


class SlugModelChoiceField(forms.ModelChoiceField):
    def label_from_instance(self, x):
        return x.slug


class SearchForm(forms.Form):
    invoice_date = forms.DateField(required=False)
    number = forms.IntegerField(required=False)
    currency = SlugModelChoiceField(
        label="Currency", queryset=Currency.objects.none(), required=False
    )
    payment_processor = forms.ModelChoiceField(
        label="Payment",
        queryset=PaymentProcessor.objects.none(),
        required=False,
    )

    def __init__(self, *args, **kwargs):
        currency_qs = kwargs.pop("currency_qs")
        display_payment_processor = kwargs.pop("display_payment_processor")
        payment_processor_qs = kwargs.pop("payment_processor_qs")
        super().__init__(*args, **kwargs)
        # invoice date
        f = self.fields["invoice_date"]
        f.label = ""
        f.widget.attrs.update({"class": "datepicker pure-u-23-24"})
        # invoice number
        f = self.fields["number"]
        f.label = ""
        f.widget.attrs.update(
            {"class": "pure-u-23-24", "placeholder": "Number"}
        )
        # currency
        f = self.fields["currency"]
        f.empty_label = "All currencies..."
        f.label = ""
        f.queryset = currency_qs
        f.widget.attrs.update({"class": "chosen-select pure-u-23-24"})
        # payment processor
        f = self.fields["payment_processor"]
        if display_payment_processor:
            f.empty_label = "All processors..."
            f.label = ""
            f.queryset = payment_processor_qs
            f.widget.attrs.update({"class": "chosen-select pure-u-23-24"})
        else:
            f.widget = forms.HiddenInput()
