# -*- encoding: utf-8 -*-
import attr
import collections

from datetime import date, datetime
from dateutil.relativedelta import relativedelta
from dateutil.rrule import SU, WEEKLY, rrule
from decimal import Decimal
from django.apps import apps
from django.conf import settings
from django.core.exceptions import ValidationError
from django.db import models, transaction
from django.db.models import Q
from django.urls import reverse
from django.utils import timezone
from django.utils.timesince import timeuntil
from reversion import revisions as reversion

from base.model_utils import (
    TimedCreateModifyDeleteModel,
    TimedCreateModifyDeleteVersionModel,
    TimedCreateModifyDeleteVersionModelManager,
    TimeStampedModel,
    private_file_store,
)
from base.singleton import SingletonModel
from crm.models import KanbanCard, Note, Ticket
from finance.models import Country, Currency, VatCode, VatSettings
from sales_order.models import SalesOrder
from sales_order_ticket.models import SalesOrderTicket
from stock.models import Product


def format_minutes(minutes):
    """Convert minutes into formatted hours and minutes.

    e.g. assert '01:15' == format_minutes(75)

    From:
    http://stackoverflow.com/questions/20291883/converting-minutes-to-hhmm-format-in-python

    """
    return "{:02d}:{:02d}".format(*divmod(int(minutes), 60))


def free_of_charge_ticket_pks():
    """Tickets which are free of charge.

    A ticket can be free of charge for two reasons:

    1. The *lane* for the ``KanbanCard`` is *not* ``funded``.
    2. The ``Funded`` field on the ticket is a *free-of-charge* type.

    .. note:: For more information, see the docs on the ``Ticket`` model.

    """
    ticket_card_pks = KanbanCard.objects.filter(
        column__lane__funded=False
    ).values_list("ticket__pk", flat=True)
    ticket_pks = Ticket.objects.filter(funded__free_of_charge=True).values_list(
        "pk", flat=True
    )
    return [x for x in ticket_pks] + list(ticket_card_pks)


def get_contact_model():
    return apps.get_model(settings.CONTACT_MODEL)


def quantize(value):
    return value.quantize(Decimal(".01"))


def ticket_activity(ticket):
    """

    From, ``Building a combined stream of recent additions`` by Simon Willison,
    https://simonwillison.net/2018/Mar/25/combined-recent-additions/

    """
    fields = (
        "pk",
        "user__username",
        # "title",
        "created",
        # "description",
        "model",
    )
    # email = ticket.mail_ticket.annotate(
    #    model=models.Value("mail", output_field=models.CharField())
    # ).values(*fields)
    notes = ticket.notes.annotate(
        model=models.Value("note", output_field=models.CharField())
    ).values(*fields)
    time_records = ticket.time_records.annotate(
        model=models.Value("time", output_field=models.CharField())
    ).values(*fields)
    # qs = email.union(notes.union(time_records)).order_by("-created")
    qs = notes.union(time_records).order_by("-created")
    # all the types
    type_to_queryset = {
        # "mail": MailTicket.objects.all(),
        "note": Note.objects.all(),
        "time": TimeRecord.objects.all(),
    }
    # collect the pks we need to load for each type:
    to_load = {}
    for row in qs:
        to_load.setdefault(row["model"], []).append(row["pk"])
    # fetch them
    fetched = {}
    for model, pks in to_load.items():
        for x in type_to_queryset[model].filter(pk__in=pks):
            fetched[(model, x.pk)] = x
    # annotate 'qs' with loaded objects
    for row in qs:
        key = (row["model"], row["pk"])
        row["object"] = fetched[key]
    return qs


@attr.s
class TicketSummary:
    pk = attr.ib()
    description = attr.ib()
    contact = attr.ib()
    user_name = attr.ib()
    data = attr.ib()


@attr.s
class TimeAnalysis:
    """Utility class used for calculating time by type e.g. chargeable.

    To use this class, create an instance, then ``add`` one or more time
    records e.g::

      from invoice.models import TimeAnalysis
      analysis = TimeAnalysis()
      analysis.add(time_record)

    You can then get the time by type e.g::

      analysis.charge
      analysis.fixed
      analysis.non_charge

    .. note:: The totals are in **minutes**.

    """

    charge = attr.ib(default=0)
    fixed = attr.ib(default=0)
    non_charge = attr.ib(default=0)
    invoiced = attr.ib(default=0)
    pending = attr.ib(default=0)

    def add(self, time_record):
        if time_record.is_complete:
            minutes = time_record.minutes
            has_invoice_line = time_record.has_invoice_line
            if time_record.ticket.fixed_price:
                self.fixed = self.fixed + minutes
            elif time_record.billable:
                self.charge = self.charge + minutes
                if has_invoice_line:
                    self.invoiced = self.invoiced + minutes
                else:
                    self.pending = self.pending + minutes
            else:
                self.non_charge = self.non_charge + minutes

    @property
    def charge_fmt(self):
        return format_minutes(self.charge)

    @property
    def invoiced_fmt(self):
        """How much of the added time has been invoiced?"""
        return format_minutes(self.invoiced)

    @property
    def fixed_fmt(self):
        return format_minutes(self.fixed)

    @property
    def non_charge_fmt(self):
        return format_minutes(self.non_charge)

    @property
    def pending_fmt(self):
        """How much of the added time can be invoiced?"""
        return format_minutes(self.pending)

    @property
    def total(self):
        return self.charge + self.fixed + self.non_charge

    @property
    def total_fmt(self):
        return format_minutes(self.charge + self.fixed + self.non_charge)


class InvoiceContactManager(models.Manager):
    def _create_invoice_contact(
        self, contact, country, hourly_rate, vat_number, on_account
    ):
        x = self.model(
            contact=contact,
            country=country,
            hourly_rate=hourly_rate,
            on_account=on_account,
            vat_number=vat_number or "",
        )
        x.save()
        return x

    def init_invoice_contact(
        self,
        contact,
        country=None,
        hourly_rate=None,
        vat_number=None,
        on_account=None,
    ):
        """Initialise the invoice details for the contact.

        .. note:: The on account flag (``on_account``) can only be switched on.
                  It can't be switched off.
                  If we are posting to an accounts package e.g. Xero, then we
                  should always post on-account if we ever posted on-account.

        """
        if on_account is None:
            on_account = False
        try:
            x = self.model.objects.get(contact=contact)
            update = False
            if country:
                x.country = country
                update = True
            if hourly_rate:
                x.hourly_rate = hourly_rate
                update = True
            if on_account is True and x.on_account is False:
                x.on_account = on_account
                update = True
            if vat_number:
                x.vat_number = vat_number
                update = True
            if update:
                x.save()
        except self.model.DoesNotExist:
            x = self._create_invoice_contact(
                contact, country, hourly_rate, vat_number, on_account
            )
        return x


class InvoiceContact(TimeStampedModel):
    contact = models.OneToOneField(
        settings.CONTACT_MODEL, on_delete=models.CASCADE
    )
    hourly_rate = models.DecimalField(
        blank=True, null=True, max_digits=8, decimal_places=2
    )
    country = models.ForeignKey(
        Country,
        help_text="Invoicing country (for VAT)",
        blank=True,
        null=True,
        on_delete=models.CASCADE,
        related_name="+",
    )
    on_account = models.BooleanField(
        default=False, help_text="Post the invoice on-account (do not batch)"
    )
    # Maximum of 12 characters?
    # https://www.gov.uk/guidance/vat-eu-country-codes-vat-numbers-and-vat-in-other-languages
    # 10/09/2019 The Xero 'TaxNumber' has a maximum length of 50 characters.
    vat_number = models.CharField(max_length=50, blank=True)
    objects = InvoiceContactManager()

    class Meta:
        verbose_name = "Invoice Contact"
        verbose_name_plural = "Invoice Contacts"

    def __str__(self):
        result = "{}".format(self.contact.full_name)
        if self.hourly_rate:
            result = "{} @ {}".format(result, self.hourly_rate)
        if self.vat_number:
            result = "{}, VAT Number {}".format(result, self.vat_number)
        return result

    def get_absolute_url(self):
        return self.contact.get_absolute_url()

    def is_european_union_vat_transaction(self):
        result = False
        if self.country and self.vat_number:
            if VatSettings.objects.european_union_countries_initialised():
                if VatSettings.objects.is_european_union_country(self.country):
                    result = True
        return result


reversion.register(InvoiceContact)


class InvoiceError(Exception):
    def __init__(self, value):
        Exception.__init__(self)
        self.value = value

    def __str__(self):
        return repr("%s, %s" % (self.__class__.__name__, self.value))


class PaymentProcessorManager(models.Manager):
    def create_payment_processor(self, description):
        x = self.model(description=description)
        x.save()
        return x


class PaymentProcessor(TimeStampedModel):
    description = models.CharField(max_length=100, unique=True)
    deleted = models.BooleanField(default=False)
    objects = PaymentProcessorManager()

    class Meta:
        ordering = ("description",)
        verbose_name = "Payment Processor"

    def __str__(self):
        return "{}{}".format(
            self.description, " (deleted)" if self.deleted else ""
        )


class InvoiceManager(TimedCreateModifyDeleteVersionModelManager):
    def _batch_invoices(self, invoice_date):
        """List of invoices which can be included in a batch.

        .. tip:: For details, see ``on_account_invoices``.

        """
        on_account_contact_pks = InvoiceContact.objects.filter(
            on_account=True
        ).values_list("contact__pk", flat=True)
        return (
            self.model.objects.current(invoice_date=invoice_date)
            .exclude(is_credit=True)
            .filter(
                upfront_payment=True,
            )
            .exclude(contact__pk__in=on_account_contact_pks)
        )

    def batch_invoices(self, invoice_date, currency, payment_processor):
        """Invoices which can be included in a batch.

        Invoices can be included in a batch if they are paid, but not on-account
        and have the same invoice date, currency and payment processor.

        .. tip:: For details, see ``on_account_invoices`` and
                ``_batch_invoices``.

        .. note:: Moved from ``Batch.objects._batch_invoices``.

        """
        qs = self._batch_invoices(invoice_date)
        return qs.filter(
            currency=currency,
            upfront_payment_processor=payment_processor,
        )

    def credit_notes(self, invoice_date):
        return self.current(invoice_date=invoice_date).filter(is_credit=True)

    def current(self, contact=None, invoice_date=None):
        qs = self.model.objects.exclude(deleted=True)
        if contact:
            qs = qs.filter(contact=contact)
        if invoice_date:
            qs = qs.filter(invoice_date=invoice_date)
        return qs

    def exchange_rates(self, invoice_date, currency, payment_processor):
        """A set of the exchange rates for invoices on this date."""
        qs = (
            self.batch_invoices(invoice_date, currency, payment_processor)
            .order_by("exchange_rate")
            .distinct("exchange_rate")
        )
        return {x.exchange_rate for x in qs}

    def on_account_invoices(self, invoice_date):
        """List of on-account invoices (which cannot be added to a batch).

        - A ``Batch`` is used to group invoices which were paid for *on order*
          (``upfront_payment=True``).
        - ``on_account`` customers should not be included in a batch.
          (``InvoiceContact``).
        - Exclude credit notes - they are handled differently.

        .. tip:: For details, see ``batch_invoices``.

        .. note:: 22/04/2020, Renamed from ``not_upfront_payment``.

        """
        batch_invoice_pks = self._batch_invoices(invoice_date).values_list(
            "pk", flat=True
        )
        return (
            self.model.objects.current(invoice_date=invoice_date)
            .exclude(is_credit=True)
            .exclude(pk__in=batch_invoice_pks)
        )


class Invoice(TimedCreateModifyDeleteVersionModel):
    """An invoice...

    From Notice 700 The VAT Guide, 16.3.1 General

    VAT invoices must show:
    - an identifying number, which is from a series that is unique and
      sequential;
    - your name, address and VAT registration number;
    - the time of supply (tax point);
    - date of issue (if different to the time of supply);
    - your customer's name (or trading name) and address;
    - a description which identifies the goods or services supplied; and
    - the unit price (see paragraph 16.3.2).

    For each description, you must show the:
    - quantity of goods or extent of the services;
    - charge made, excluding VAT;
    - rate of VAT;
    - total charge made, excluding VAT;
    - rate of any cash discount offered; and
    - total amount of VAT charged, shown in sterling.

    The ``discount`` field is currently only used by an API invoice import
    routine.  The discount field is copied from Sage and is applied to the net
    value before the VAT is calculated.

    The ``source`` field keeps a copy of where the invoice came from e.g.
    Amazon, eBay etc.  The ``source`` is used for data analysis using *R* etc.

    To make a credit note:

    1. Set ``is_credit`` to ``True``
    2. Set the line quantity to a value less than ``0``.
       For details, see the ``is_credit`` method in the ``InvoiceLine`` model.

    """

    UNIQUE_FIELD_NAME = "number"
    UNIQUE_FIELD_NAME_CATEGORY = "prefix"

    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    invoice_date = models.DateField()
    prefix = models.CharField(max_length=10, blank=True)
    number = models.IntegerField(default=0)
    upfront_payment = models.BooleanField(default=False)
    upfront_payment_processor = models.ForeignKey(
        PaymentProcessor,
        blank=True,
        null=True,
        on_delete=models.CASCADE,
        related_name="+",
    )
    contact = models.ForeignKey(
        settings.CONTACT_MODEL,
        related_name="invoice_contact",
        on_delete=models.CASCADE,
    )
    currency = models.ForeignKey(
        Currency, on_delete=models.CASCADE, related_name="+"
    )
    exchange_rate = models.DecimalField(
        max_digits=5, decimal_places=3, default=Decimal()
    )
    is_credit = models.BooleanField(default=False)
    is_draft = models.BooleanField(default=True)
    pdf = models.FileField(
        upload_to="invoice/%Y/%m/%d", storage=private_file_store, blank=True
    )
    discount = models.DecimalField(
        blank=True, null=True, max_digits=8, decimal_places=2
    )
    source = models.CharField(max_length=100, blank=True)
    objects = InvoiceManager()

    class Meta:
        ordering = ["pk"]
        unique_together = ("number", "deleted_version", "prefix")
        verbose_name = "Invoice"
        verbose_name_plural = "Invoices"

    def __str__(self):
        return "{} {}".format(self.pk, self.invoice_date)

    def get_absolute_url(self):
        return reverse("invoice.detail", args=[self.pk])

    @property
    def can_set_to_draft(self):
        """Can we set this invoice back to a draft state?"""
        result = False
        if not self.is_draft:
            if self.invoice_date == date.today():
                result = True
        return result

    def get_next_line_number(self):
        try:
            self.line_number = self.line_number
        except AttributeError:
            self.line_number = 1
        while True:
            try:
                self.invoiceline_set.get(line_number=self.line_number)
            except InvoiceLine.DoesNotExist:
                break

            self.line_number = self.line_number + 1
        return self.line_number

    def invoice_for_credit(self):
        result = None
        if self.is_credit:
            result = [
                x.invoice
                for x in InvoiceCredit.objects.filter(credit_note=self)
            ]
        return result

    def time_analysis(self):
        """Time analysis by user and ticket for an invoice.

        A dictionary will be returned.  The keys are 'user.username' and
        primary key of the ticket (or zero for invoice lines which do not have
        a time record:

        result = {
            '': {
                0: {'net': Decimal('200'), 'quantity': Decimal('2')}
            },
            'fred': {
                1: {'net': Decimal('1000'), 'quantity': Decimal('10')}
            },
            'sara': {
                1: {'net': Decimal('600'), 'quantity': Decimal('6')},
                3: {'net': Decimal('1400'), 'quantity': Decimal('14')}
            }
        }

        """
        result = {}
        qs = self.invoiceline_set.all()
        for line in qs:
            user_name = ""
            quantity = line.quantity
            if line.has_time_record:
                user_name = line.timerecord.user.username
                # line.quantity does not have sufficient precision
                tx = datetime.combine(
                    line.timerecord.date_started, line.timerecord.end_time
                ) - datetime.combine(
                    line.timerecord.date_started, line.timerecord.start_time
                )
                quantity = Decimal((tx.seconds / 3600))
            if not user_name in result:
                result[user_name] = {}
            tickets = result[user_name]
            if line.has_time_record:
                pk = line.timerecord.ticket.pk
                start_date = line.timerecord.date_started
                end_date = line.timerecord.date_started
            else:
                pk = 0
                start_date = line.created
                end_date = line.created
            if not pk in tickets:
                tickets[pk] = dict(
                    start_date=start_date,
                    end_date=end_date,
                    quantity=Decimal(),
                    net=Decimal(),
                )
            totals = tickets[pk]
            if totals["start_date"] > start_date:
                totals["start_date"] = start_date
            if totals["end_date"] < end_date:
                totals["end_date"] = end_date
            totals["net"] = totals["net"] + line.net
            totals["quantity"] = totals["quantity"] + quantity
        return result

    @property
    def description(self):
        if self.is_credit:
            return "Credit Note"

        else:
            return "Invoice"

    @property
    def gross(self):
        totals = self.invoiceline_set.aggregate(
            models.Sum("net"), models.Sum("vat")
        )
        discount = self.discount or Decimal()
        net = totals["net__sum"] or Decimal()
        return (net - discount) + (totals["vat__sum"] or Decimal())

    @property
    def has_lines(self):
        return bool(self.invoiceline_set.count())

    @property
    def invoice_number(self):
        return "{}{:06d}".format(self.prefix, self.number)

    @property
    def net(self):
        """The discount is taken off the net value of the invoice.

        The discount field is copied from Sage and is applied to the net value
        before the VAT is calculated.

        """
        totals = self.invoiceline_set.aggregate(models.Sum("net"))
        discount = self.discount or Decimal()
        net = totals["net__sum"] or Decimal()
        return net - discount

    def remove_time_lines(self):
        if not self.is_draft:
            raise InvoiceError(
                "Time records can only be removed from a draft invoice."
            )

        pks = [i.pk for i in self.invoiceline_set.all()]
        with transaction.atomic():
            for pk in pks:
                line = InvoiceLine.objects.get(pk=pk)
                try:
                    time_record = line.timerecord
                    time_record.invoice_line = None
                    time_record.save()
                    line.delete()
                except TimeRecord.DoesNotExist:
                    pass

    def set_to_draft(self):
        """Set the invoice back to a draft state."""
        if self.can_set_to_draft:
            self.is_draft = True
            self.save()
        else:
            raise InvoiceError(
                "You can only set an invoice back to draft "
                "on the day it was created."
            )

    @property
    def vat(self):
        totals = self.invoiceline_set.aggregate(models.Sum("vat"))
        return totals["vat__sum"] or Decimal()

    def vat_analysis(self):
        """Calculate summary totals for an invoice.

        Find the total net and vat for an invoice - grouped by VAT code.

        """
        qs = (
            self.invoiceline_set.order_by("vat_code__slug")
            .values("vat_code__slug")
            .annotate(models.Count("line_number"))
            .annotate(models.Sum("net"))
            .annotate(models.Sum("vat"))
        )
        result = {}
        for x in qs:
            result[x["vat_code__slug"]] = {
                "count": x["line_number__count"],
                "net": x["net__sum"],
                "vat": x["vat__sum"],
            }
        return result


reversion.register(Invoice)


class InvoiceIssueManager(models.Manager):
    def init_invoice_issue(self, invoice, description):
        """Initialise an invoice issue.

        .. note:: This method returns the line for the issue (not the issue).

        """
        try:
            invoice_issue = self.model.objects.get(invoice=invoice)
        except self.model.DoesNotExist:
            invoice_issue = self.model(invoice=invoice)
            invoice_issue.save()
        try:
            invoice_issue_line = InvoiceIssueLine.objects.get(
                invoice_issue=invoice_issue, description=description
            )
        except InvoiceIssueLine.DoesNotExist:
            invoice_issue_line = InvoiceIssueLine(
                invoice_issue=invoice_issue, description=description
            )
            invoice_issue_line.save()
        return invoice_issue_line

    def issues(self, invoice_date=None):
        if invoice_date is None:
            qs = self.model.objects.all()
        else:
            qs = self.model.objects.filter(invoice__invoice_date=invoice_date)
        return qs

    def issues_outstanding(self, invoice_date=None):
        qs = self.issues(invoice_date)
        return qs.exclude(confirmed=True)


class InvoiceIssue(models.Model):
    """Invoices issues.

    - We have a maximum of one issue for each invoice for each day.
    - A single invoice issue can have multiple lines (``InvoiceIssueLine``)

    """

    invoice = models.OneToOneField(Invoice, on_delete=models.CASCADE)
    confirmed = models.BooleanField(default=False)
    confirmed_by_user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        blank=True,
        null=True,
        on_delete=models.CASCADE,
    )
    comment = models.TextField(blank=True, null=True)
    objects = InvoiceIssueManager()

    class Meta:
        ordering = ("-invoice__invoice_date", "-invoice__pk")
        verbose_name = "Invoice Issue"
        verbose_name_plural = "Invoice Issues"

    def __str__(self):
        return "Invoice issue {} for {} dated {}".format(
            self.pk,
            self.invoice.invoice_number,
            self.invoice.invoice_date.strftime("%d/%m/%Y"),
        )

    def lines(self):
        return [x.description for x in self.invoiceissueline_set.all()]


class InvoiceIssueLine(TimeStampedModel):
    invoice_issue = models.ForeignKey(InvoiceIssue, on_delete=models.CASCADE)
    description = models.TextField()

    class Meta:
        ordering = (
            "-invoice_issue__invoice__invoice_date",
            "-invoice_issue__invoice__pk",
            "-pk",
        )
        verbose_name = "Invoice Issue Line"
        verbose_name_plural = "Invoice Issue Lines"

    def __str__(self):
        return "Invoice issue {} for {} dated {}: {}".format(
            self.invoice_issue.pk,
            self.invoice_issue.invoice.invoice_number,
            self.created.strftime("%d/%m/%Y %H:%M"),
            self.description,
        )


class BatchManager(models.Manager):
    def create_batch(
        self, batch_date, currency, exchange_rate, payment_processor
    ):
        result = None
        # qs = self._batch_invoices(batch_date, currency, payment_processor)
        qs = Invoice.objects.batch_invoices(
            batch_date, currency, payment_processor
        )
        invoices = qs.filter(exchange_rate=exchange_rate)
        # only create the batch if we have invoices to go in it!
        if invoices.count():
            with transaction.atomic():
                result = self.model(
                    batch_date=batch_date,
                    currency=currency,
                    exchange_rate=exchange_rate,
                    payment_processor=payment_processor,
                )
                result.save()
                invoice_pks = [x.pk for x in invoices]
                for pk in invoice_pks:
                    invoice = Invoice.objects.get(pk=pk)
                    BatchInvoice(batch=result, invoice=invoice).save()
        return result

    def for_date(self, batch_date):
        return self.model.objects.filter(batch_date=batch_date)


class Batch(TimeStampedModel):
    """Header record for a batch of invoices.

    Each batch is unique by:

    1. Day
    2. Currency Code
    3. Exchange Rate
    4. Payment Processor

    We are using this to post batches of invoices to the Xero accounts package.
    Xero has limited capacity for posting individual invoices, so we are
    batching up a group of invoices where the ``upfront_payment`` field is set
    to ``True``.

    An ``exchange_rate`` of 0 (zero) will be used for a default batch
    (an empty / ``null`` / ``None`` value for the ``exchange_rate`` does not
    seem to work with ``unique_together``).

    """

    batch_date = models.DateField()
    currency = models.ForeignKey(
        Currency, on_delete=models.CASCADE, related_name="+"
    )
    exchange_rate = models.DecimalField(
        max_digits=5, decimal_places=3, default=Decimal()
    )
    payment_processor = models.ForeignKey(
        PaymentProcessor, on_delete=models.CASCADE, related_name="+"
    )
    objects = BatchManager()

    class Meta:
        ordering = (
            "batch_date",
            "currency",
            "exchange_rate",
            "payment_processor",
        )
        unique_together = (
            "batch_date",
            "currency",
            "exchange_rate",
            "payment_processor",
        )
        verbose_name = "Invoice Batch"
        verbose_name_plural = "Invoice Batches"

    def __str__(self):
        exchange = ""
        if self.exchange_rate:
            exchange = "({}) ".format(self.exchange_rate)
        return (
            "Invoice batch {} for {} currency '{}' {}"
            "payment processor '{}'".format(
                self.pk,
                self.batch_date.strftime("%d/%m/%Y"),
                self.currency.slug,
                exchange,
                self.payment_processor.description,
            )
        )

    @property
    def batch_number(self):
        """Prefix with zeros (to make a longer value for clicking)."""
        return "{:06d}".format(self.pk)

    def get_absolute_url(self):
        return reverse("invoice.batch.invoice.list", args=[self.pk])

    def invoice_lines(self):
        invoice_pks = [x.invoice.pk for x in self.invoices()]
        return InvoiceLine.objects.filter(invoice__pk__in=invoice_pks)

    def invoices(self):
        return self.batchinvoice_set.all()

    def net_and_vat(self):
        totals = self.invoice_lines().aggregate(
            models.Sum("net"), models.Sum("vat")
        )
        net = totals["net__sum"] or Decimal()
        vat = totals["vat__sum"] or Decimal()
        return net, vat

    def vat_analysis(self):
        """Calculate summary totals for a batch.

        Find the total net and vat for a batch - grouped by VAT code.

        """
        qs = (
            self.invoice_lines()
            .order_by("vat_code__slug")
            .values("vat_code__slug")
            .annotate(models.Count("line_number"))
            .annotate(models.Sum("net"))
            .annotate(models.Sum("vat"))
        )
        result = {}
        for x in qs:
            result[x["vat_code__slug"]] = {
                "count": x["line_number__count"],
                "net": x["net__sum"],
                "vat": x["vat__sum"],
            }
        return result


class BatchInvoiceManager(models.Manager):
    def in_a_batch(self, invoice):
        """Is this invoice in a batch?"""
        return self.model.objects.filter(invoice=invoice).exists()


class BatchInvoice(TimeStampedModel):
    """One of the invoices in a batch."""

    batch = models.ForeignKey(Batch, on_delete=models.CASCADE)
    invoice = models.OneToOneField(Invoice, on_delete=models.CASCADE)
    objects = BatchInvoiceManager()

    class Meta:
        unique_together = ("batch", "invoice")
        verbose_name = "Batch Invoice"
        verbose_name_plural = "Batch Invoices"

    def __str__(self):
        return "Invoice {} (batch {} {} currency '{}')".format(
            self.invoice.invoice_number,
            self.batch.pk,
            self.batch.batch_date.strftime("%d/%m/%Y"),
            self.batch.currency.slug,
        )


# class ExportToRManager(models.Manager):
#    def export_invoice_lines(self):
#        """Export invoice data to this model (for data analysis using R).
#
#        Documentation at:
#        https://www.kbsoftware.co.uk/docs/app-invoice.html#export-to-r-shiny
#
#        """
#        count = 0
#        with transaction.atomic():
#            self.model.objects.all().delete()
#            qs = InvoiceLine.objects.all().order_by("-pk")
#            for line in qs:
#                count = count + 1
#                contact = line.invoice.contact
#                self.model(
#                    username=contact.user.username,
#                    invoice_date=line.invoice.invoice_date.isoformat(),
#                    source=line.invoice.source,
#                    category=line.product.category.name,
#                    sku=line.product.name,
#                    product=line.product.description,
#                    quantity=line.quantity,
#                    net=line.net,
#                ).save()
#        return count
#
#
# class ExportToR(models.Model):
#    username = models.CharField("username", max_length=150)
#    invoice_date = models.DateField()
#    source = models.CharField(max_length=100, blank=True)
#    category = models.CharField(max_length=100)
#    sku = models.CharField(max_length=150)
#    product = models.TextField(blank=True)
#    quantity = models.DecimalField(max_digits=6, decimal_places=2)
#    net = models.DecimalField(max_digits=8, decimal_places=2)
#    objects = ExportToRManager()
#
#    class Meta:
#        verbose_name = "Export to R"
#
#    def __str__(self):
#        return "{}".format(self.pk)


class InvoiceCreditManager(models.Manager):
    def create_invoice_credit(self, invoice, credit_note):
        x = self.model(invoice=invoice, credit_note=credit_note)
        x.save()
        return x

    def init_invoice_credit(self, invoice, credit_note):
        try:
            x = self.model.objects.get(invoice=invoice, credit_note=credit_note)
            if x.is_deleted:
                x.undelete()
        except self.model.DoesNotExist:
            x = self.create_invoice_credit(invoice, credit_note)
        return x


class InvoiceCredit(TimedCreateModifyDeleteModel):
    invoice = models.ForeignKey(Invoice, on_delete=models.CASCADE)
    credit_note = models.ForeignKey(
        Invoice, related_name="credit", on_delete=models.CASCADE
    )
    objects = InvoiceCreditManager()

    class Meta:
        unique_together = ("invoice", "credit_note")
        verbose_name = "Invoice Credit"
        verbose_name_plural = "Invoice Credit"

    def __str__(self):
        return "Credit {} for Invoice {}".format(
            self.credit_note.invoice_number, self.invoice.invoice_number
        )


reversion.register(InvoiceCredit)


class InvoiceSettingsManager(models.Manager):
    def settings(self):
        try:
            return self.model.objects.get()

        except self.model.DoesNotExist:
            raise InvoiceError("Invoice settings have not been set-up in admin")


class InvoiceSettings(SingletonModel):
    name_and_address = models.TextField()
    phone_number = models.CharField(max_length=100)
    footer = models.TextField()
    time_record_product = models.ForeignKey(
        Product,
        blank=True,
        null=True,
        on_delete=models.CASCADE,
        help_text="Product used for time records.",
    )
    objects = InvoiceSettingsManager()

    class Meta:
        verbose_name = "Invoice print settings"

    def __str__(self):
        result = "{}, Phone: {}".format(
            " ".join(self.name_and_address.split("\n")), self.phone_number
        )
        if self.time_record_product:
            result = "{}, Time record: {}".format(
                result, self.time_record_product.name
            )
        return result


reversion.register(InvoiceSettings)


class InvoiceLineManager(models.Manager):
    def net_and_vat(self, invoice_date=None):
        if invoice_date:
            qs = self.model.objects.filter(invoice__invoice_date=invoice_date)
        else:
            qs = self.model.objects.all()
        totals = qs.aggregate(models.Sum("net"), models.Sum("vat"))
        net = totals["net__sum"] or Decimal()
        vat = totals["vat__sum"] or Decimal()
        return net, vat


class InvoiceLine(TimeStampedModel):
    """Invoice line.

    Line numbers for each invoice increment from 1
    Line total can be calculated by adding the net and vat amounts

    23/05/2019 PJK, I added the discount field so we can keep a record of the
    value.
    In Magento (I think) the price and quantity are non-discounted, but the net
    amount is i.e. net = (price * quantity) - discount.

    """

    invoice = models.ForeignKey(Invoice, on_delete=models.CASCADE)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    line_number = models.IntegerField()
    description = models.TextField(blank=True, null=True)
    product = models.ForeignKey(
        Product, related_name="+", on_delete=models.CASCADE
    )
    quantity = models.DecimalField(max_digits=6, decimal_places=2)
    units = models.CharField(max_length=5)
    price = models.DecimalField(max_digits=8, decimal_places=2)
    discount = models.DecimalField(
        blank=True, null=True, max_digits=8, decimal_places=2
    )
    net = models.DecimalField(max_digits=8, decimal_places=2)
    vat_code = models.ForeignKey(
        VatCode,
        # default=legacy_vat_code,
        related_name="+",
        on_delete=models.CASCADE,
    )
    vat_rate = models.DecimalField(
        max_digits=5,
        decimal_places=3,
        help_text="VAT rate when the line was saved.",
    )
    vat = models.DecimalField(max_digits=8, decimal_places=2)
    objects = InvoiceLineManager()

    class Meta:
        ordering = ["line_number"]
        verbose_name = "Invoice line"
        verbose_name_plural = "Invoice lines"
        unique_together = ("invoice", "line_number")

    def __str__(self):
        return "{} {} {} @{}".format(
            self.line_number, self.quantity, self.description, self.price
        )

    def clean(self):
        if self.price < Decimal():
            raise ValidationError(
                "Price must always be greater than zero. "
                "To make a credit note, use a negative quantity."
            )

    def get_absolute_url(self):
        return reverse("invoice.detail", args=[self.invoice.pk])

    def save_and_calculate(self, *args, **kwargs):
        self.vat_rate = self.vat_code.rate
        self.net = quantize(self.price * self.quantity)
        self.vat = quantize(self.price * self.quantity * self.vat_rate)
        # Call the "real" save() method.
        super().save(*args, **kwargs)

    @property
    def gross(self):
        return self.net + self.vat

    @property
    def is_credit(self):
        return self.quantity < Decimal()

    @property
    def user_can_edit(self):
        return self.invoice.is_draft and not self.has_time_record

    @property
    def has_time_record(self):
        try:
            self.timerecord
            return True

        except TimeRecord.DoesNotExist:
            return False


reversion.register(InvoiceLine)


class InvoiceUser(models.Model):
    user = models.OneToOneField(
        settings.AUTH_USER_MODEL, unique=True, on_delete=models.CASCADE
    )
    mail_time_summary = models.BooleanField(
        default=False, help_text="email a daily summary of time recorded"
    )

    class Meta:
        ordering = ["user__username"]
        verbose_name = "Invoice user"
        verbose_name_plural = "Invoice user"

    def __str__(self):
        message = ""
        if self.mail_time_summary:
            message = ": mail time summary"
        return "{}{}".format(self.user.username, message)


reversion.register(InvoiceUser)


class TimeCodeManager(models.Manager):
    def time_codes(self):
        return self.model.objects.exclude(deleted=True)


class TimeCode(TimeStampedModel):
    description = models.CharField(max_length=100)
    deleted = models.BooleanField(default=False)
    objects = TimeCodeManager()

    class Meta:
        ordering = ["description"]
        verbose_name = "Time Code"
        verbose_name_plural = "Time Codes"

    def __str__(self):
        return self.description


class QuickTimeRecordManager(models.Manager):
    def quick(self, user):
        return self.model.objects.filter(user=user).exclude(deleted=True)

    def quick_chargeable(self, user):
        return self.quick(user).filter(chargeable=True).order_by("description")

    def quick_non_chargeable(self, user):
        return self.quick(user).filter(chargeable=False).order_by("description")


class QuickTimeRecord(TimeStampedModel):
    """Pass this record to ``create_time_record`` to auto-start time."""

    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    time_code = models.ForeignKey(TimeCode, on_delete=models.CASCADE)
    chargeable = models.BooleanField(default=False)
    description = models.CharField(max_length=100)
    deleted = models.BooleanField(default=False)
    objects = QuickTimeRecordManager()

    class Meta:
        ordering = ["-chargeable", "description"]
        verbose_name = "Quick Time Recording"
        verbose_name_plural = "Quick Time Recording"

    def __str__(self):
        return self.description


class TimeRecordManager(models.Manager):
    def exclude_fixed_price_free_of_charge(
        self, qs, exclude_fixed_price=None, exclude_free_of_charge=None
    ):
        """Exclude time records as requested.

        Option to ``exclude_fixed_price`` and ``exclude_free_of_charge``
        tickets.

        .. tip:: Used by ``to_invoice`` and ``to_report``.

        """
        if exclude_fixed_price:
            # stop checking 'fixed_price' on the ticket after May 2024
            qs = qs.exclude(
                Q(ticket__fixed_price=True)
                & Q(date_started__lte=self.model.LEGACY_FIXED_PRICE_UNTIL)
            )
            fixed_price_ticket_pks = self.fixed_price_ticket_pks()
            qs = qs.exclude(ticket__pk__in=fixed_price_ticket_pks)
        if exclude_free_of_charge:
            free_of_charge_tickets = free_of_charge_ticket_pks()
            qs = qs.exclude(ticket__pk__in=free_of_charge_tickets)
        return qs

    def fixed_price_ticket_pks(self):
        """Fixed price tickets (using the sales order)."""
        fixed_price_sales_order = SalesOrder.objects.filter(
            hours_type__in=(
                SalesOrder.FIXED_PRICE,
                SalesOrder.SUPPORT_ANNUAL,
                SalesOrder.SUPPORT_MONTHLY,
            )
        )
        return SalesOrderTicket.objects.filter(
            sales_order__in=fixed_price_sales_order
        ).values_list("ticket__pk", flat=True)

    def create_time_record(self, ticket, quick_time_record, start_time):
        obj = self.model(
            billable=quick_time_record.chargeable,
            date_started=date.today(),
            start_time=start_time,
            ticket=ticket,
            time_code=quick_time_record.time_code,
            title=quick_time_record.description,
            user=quick_time_record.user,
        )
        obj.save()
        return obj

    def report_charge_non_charge(self, from_date, to_date, user=None):
        """Report of chargeable and non-chargeable time."""
        qs = TimeRecord.objects.filter(
            date_started__gte=from_date, date_started__lte=to_date
        )
        if user:
            qs = qs.filter(user=user)
        result = TimeAnalysis()
        for row in qs:
            result.add(row)
        return result

    def report_time_by_crm_contact(self, contact, from_date, to_date):
        """Time for a CRM contact - grouped by ticket."""
        qs = TimeRecord.objects.filter(
            ticket__contact=contact,
            date_started__gte=from_date,
            date_started__lte=to_date,
        ).order_by("-start_time")
        result = collections.OrderedDict()
        for row in qs:
            # only add tickets with completed time records
            if row.is_complete:
                ticket_pk = row.ticket.pk
                if not ticket_pk in result:
                    result[ticket_pk] = TicketSummary(
                        pk=ticket_pk,
                        description=row.ticket.title,
                        contact=row.ticket.contact.full_name,
                        user_name=row.ticket.contact.user.username,
                        data=TimeAnalysis(),
                    )
                result[ticket_pk].data.add(row)
        return list(result.values())

    def report_time_by_ticket(self, user, day):
        """Group time by ticket for a user for a day.

        Return an ordered dictionary containing analysis of chargeable,
        non-chargeable and fixed price time.

        """
        qs = TimeRecord.objects.filter(user=user, date_started=day).order_by(
            "-start_time"
        )
        result = collections.OrderedDict()
        for row in qs:
            # only add tickets with completed time records
            if row.is_complete:
                ticket_pk = row.ticket.pk
                if not ticket_pk in result:
                    result[ticket_pk] = TimeAnalysis()
                result[ticket_pk].add(row)
        return result

    def report_time_by_user(self, from_date, to_date):
        qs = TimeRecord.objects.filter(
            date_started__gte=from_date, date_started__lte=to_date
        )
        result = {}
        for row in qs:
            # only add completed time records
            if row.is_complete:
                user_name = row.user.username
                if not user_name in result:
                    result[user_name] = TimeAnalysis()
                result[user_name].add(row)
        return result

    def report_time_by_user_by_week_date(self, from_date, to_date, user):
        start_date = from_date + relativedelta(weekday=SU(-1))
        end_date = to_date + relativedelta(weekday=SU(1))
        result = collections.OrderedDict()
        for d in rrule(WEEKLY, dtstart=start_date, until=end_date):
            result[d.date()] = TimeAnalysis()
        qs = TimeRecord.objects.filter(
            date_started__gte=from_date, date_started__lte=to_date, user=user
        )
        for row in qs:
            if row.is_complete:
                item = row.date_started + relativedelta(weekday=SU(-1))
                result[item].add(row)
        return result

    def report_time_by_user_by_week(self, from_date, to_date, user):
        data = self.report_time_by_user_by_week_date(from_date, to_date, user)
        result = collections.OrderedDict()
        for key, value in data.items():
            result[key.strftime("%Y_%U")] = value
        return result

    def report_time_for_user(self, from_date, to_date, user=None):
        qs = TimeRecord.objects.filter(
            date_started__gte=from_date, date_started__lte=to_date
        )
        if user:
            qs = qs.filter(user=user)
        result = {}
        for row in qs:
            if row.is_complete:
                slug = row.ticket.contact.user.username
                if not slug in result:
                    result[slug] = TimeAnalysis()
                result[slug].add(row)
        return result

    def running(self, user):
        return self.model.objects.filter(user=user, end_time__isnull=True)

    def running_today(self, user):
        return self.running(user).filter(date_started=date.today())

    def start(self, ticket, quick_time_record):
        running = self.running_today(quick_time_record.user)
        count = running.count()
        with transaction.atomic():
            start_time = timezone.localtime(timezone.now()).time()
            if count == 1:
                to_stop = running[0]
                to_stop.stop(start_time)
            elif count > 1:
                raise InvoiceError(
                    "Cannot start a time record when {} are already "
                    "running for '{}'"
                    ".".format(count, quick_time_record.user.username)
                )

            obj = self.create_time_record(ticket, quick_time_record, start_time)
        return obj

    def ticket_totals(self, ticket):
        time_analysis = TimeAnalysis()
        qs = self.model.objects.filter(ticket=ticket)
        for time_record in qs:
            time_analysis.add(time_record)
        return time_analysis

    def tickets(self, from_date, to_date, user):
        """List of tickets which have been worked on by this user."""
        qs = self.model.objects.filter(
            date_started__gte=from_date, date_started__lte=to_date, user=user
        ).exclude(end_time__isnull=True)
        tickets = set()
        for x in qs:
            tickets.add(x.ticket.pk)
        return Ticket.objects.filter(pk__in=tickets)

    def to_invoice(self, exclude_fixed_price=None, exclude_free_of_charge=None):
        """Time records waiting to be invoiced.

        Option to ``exclude_fixed_price`` and ``exclude_free_of_charge``
        tickets.

        .. tip:: Individual time records which are marked FOC
                 (i.e. not ``billable``) are always excluded.

        .. tip:: Very similar to ``to_report``, but only includes ``billable``
                 time and excludes invoiced time (has an ``invoice_line``).

        """
        qs = self.model.objects.filter(
            billable=True,
            invoice_line__isnull=True,
            date_started__isnull=False,
            start_time__isnull=False,
            end_time__isnull=False,
        )
        return self.exclude_fixed_price_free_of_charge(
            qs, exclude_fixed_price, exclude_free_of_charge
        )

    def to_invoice_contact(self, contact, iteration_end):
        """Time records waiting to be invoiced for a contact and before a date.

        - before iteration ended
        - which have not been included on a previous invoice
        - which are chargeable
        - which are not fixed price
        - which are not free of charge

        """
        qs = self.to_invoice(
            exclude_fixed_price=True, exclude_free_of_charge=True
        )
        qs = qs.filter(ticket__contact=contact, date_started__lte=iteration_end)
        return qs.order_by("ticket__pk", "date_started", "start_time")

    def to_report(self, exclude_fixed_price=None, exclude_free_of_charge=None):
        """Time records to analyse (using Pandas).

        Option to ``exclude_fixed_price`` and ``exclude_free_of_charge``
        tickets.

        .. tip:: Very similar to ``to_invoice``, but includes ``billable``
                 time and invoiced time (``invoice_line``).

        """
        qs = self.model.objects.filter(
            date_started__isnull=False,
            start_time__isnull=False,
            end_time__isnull=False,
        )
        return self._exclude_fixed_price_free_of_charge(
            qs, exclude_fixed_price, exclude_free_of_charge
        )


class TimeRecord(TimeStampedModel):
    """Simple time recording"""

    CHARGE = "Chargeable"
    FIXED_PRICE = "Fixed-Price"
    NON_CHARGE = "Non-Chargeable"
    # stop checking 'fixed_price' on the ticket after May 2024
    LEGACY_FIXED_PRICE_UNTIL = date(2024, 5, 31)

    ticket = models.ForeignKey(Ticket, on_delete=models.CASCADE)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    title = models.CharField(max_length=100)
    description = models.TextField(blank=True, null=True)
    date_started = models.DateField()
    start_time = models.TimeField()
    end_time = models.TimeField(blank=True, null=True)
    billable = models.BooleanField(default=False)
    invoice_line = models.OneToOneField(
        InvoiceLine, blank=True, null=True, on_delete=models.CASCADE
    )
    time_code = models.ForeignKey(
        TimeCode, blank=True, null=True, on_delete=models.CASCADE
    )
    objects = TimeRecordManager()

    class Meta:
        ordering = ["-date_started", "-start_time"]
        verbose_name = "Time record"
        verbose_name_plural = "Time records"

    def __str__(self):
        return "Ticket {}, {}: {} from {} to {}".format(
            self.ticket.pk,
            self.title,
            self.date_started,
            self.start_time.strftime("%H:%M:%S") if self.start_time else "",
            self.end_time.strftime("%H:%M:%S") if self.end_time else "",
        )

    def clean(self):
        if (
            self.start_time
            and self.end_time
            and self.start_time >= self.end_time
        ):
            raise ValidationError("End time must be after the start time")

    def _date_started_time(self):
        return datetime.combine(self.date_started, self.start_time)

    @property
    def deleted(self):
        """No actual delete (yet), so just return 'False'."""
        return False

    def delta(self):
        if self.start_time and self.end_time:
            return self._end_date_time() - self._date_started_time()

        else:
            from datetime import timedelta

            return timedelta()

    def delta_as_string(self):
        if self.start_time and self.end_time:
            return timeuntil(self._end_date_time(), self._date_started_time())

        else:
            return ""

    def get_absolute_url(self):
        return reverse(
            "invoice.time.ticket.list", kwargs={"pk": self.ticket.pk}
        )

    def get_update_url(self):
        return reverse("invoice.time.update", args=[self.pk])

    def get_summary_description(self):
        return filter(None, (self.title, self.description))

    @property
    def is_complete(self):
        """Check the time record is set-up correctly for invoicing."""
        if self.date_started and self.start_time and self.end_time:
            result = True
        else:
            result = False
        return result

    def is_legacy_fixed_price(self):
        """Ignore 'fixed_price' on the ticket after May 2024.

        https://www.kbsoftware.co.uk/crm/ticket/1017/

        """
        result = False
        if self.date_started <= self.LEGACY_FIXED_PRICE_UNTIL:
            result = self.ticket.fixed_price is True
        return result

    def is_today(self):
        return timezone.now().date() == self.date_started

    def _end_date_time(self):
        return datetime.combine(self.date_started, self.end_time)

    def _has_invoice_line(self):
        """is this time record attached to an invoice"""
        return bool(self.invoice_line)

    has_invoice_line = property(_has_invoice_line)

    @property
    def invoice_quantity(self):
        """
        Convert the time in minutes into hours expressed as a decimal
        e.g. 1 hour, 30 minutes = 1.5.  This figure will be used on invoices.
        """
        return Decimal(self.minutes) / Decimal("60")

    @property
    def minutes(self):
        """Convert the time difference into minutes"""
        td = self.delta()
        return td.days * 1440 + td.seconds / 60

    @property
    def modified_today(self):
        return self.created > timezone.now() - relativedelta(hours=24)

    def stop(self, end_time=None):
        """Stop recording time on this record."""
        if not end_time:
            end_time = timezone.localtime(timezone.now()).time()
        if self.end_time:
            raise InvoiceError(
                "Time record '{}' has already been stopped".format(self.pk)
            )
        self.end_time = end_time
        self.save()

    @property
    def user_can_edit(self):
        return not self.has_invoice_line


reversion.register(TimeRecord)
