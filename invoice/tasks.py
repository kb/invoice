# -*- encoding: utf-8 -*-
import dramatiq
import logging

from django.conf import settings
from django.utils import timezone

from invoice.models import InvoiceUser
from invoice.report import time_summary
from mail.service import queue_mail_message
from mail.tasks import process_mail
from report.models import ReportSchedule, ReportSpecification


logger = logging.getLogger(__name__)


# https://dramatiq.io/cookbook.html#binding-worker-groups-to-queues
@dramatiq.actor(queue_name=settings.DRAMATIQ_QUEUE_NAME)
def mail_time_summary():
    users = []
    for item in InvoiceUser.objects.all():
        if item.mail_time_summary and item.user.email:
            users.append(item.user)
    for user in users:
        logger.info("mail_time_summary: {}".format(user.username))
        report = time_summary(user, days=1)
        message = ""
        for d, ticket_report in report.items():
            message = message + "\n\n{}, total time {}".format(
                d.strftime("%d/%m/%Y %A"), ticket_report.totals.total_fmt
            )
            for ticket in ticket_report.tickets:
                message = message + "\n{}: {}, {} ({})".format(
                    ticket.pk,
                    ticket.contact,
                    ticket.description,
                    ticket.data.total_fmt,
                )
        queue_mail_message(
            user,
            [user.email],
            "Time Summary for {}".format(timezone.now().strftime("%d/%m/%Y")),
            message,
        )
    if users:
        process_mail.send()


# https://dramatiq.io/cookbook.html#binding-worker-groups-to-queues
@dramatiq.actor(queue_name=settings.DRAMATIQ_QUEUE_NAME)
def time_summary_by_user():
    report = ReportSpecification.objects.init_report_specification(
        slug="time_summary_by_user",
        title="Time Summary by User",
        app="invoice",
        report_class="TimeSummaryByUserReport",
    )
    schedule = ReportSchedule.objects.create_report_schedule(report=report)
    ReportSchedule.objects.process()
    logger.info("time_summary_by_user: {}".format(schedule.pk))
