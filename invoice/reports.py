# -*- encoding: utf-8 -*-
import attr
import collections
import urllib.parse

from datetime import date
from dateutil.parser import parse
from dateutil.relativedelta import relativedelta
from dateutil.rrule import MONTHLY, rrule
from django.conf import settings
from django.contrib.auth import get_user_model
from django.utils import timezone

from crm.models import Ticket
from report.forms import ReportParametersEmptyForm
from report.models import ReportError
from report.service import ReportMixin
from .models import format_minutes, get_contact_model, TimeAnalysis, TimeRecord


@attr.s(hash=True)
class UserTicketYearMonth:
    user_name = attr.ib()
    year_month = attr.ib()
    ticket_pk = attr.ib()


def time_summary_by_user(today=None):
    result = {}
    if today is None:
        today = date.today()
    # 12 whole months
    from_date = today + relativedelta(months=-24, day=1)
    to_date = today + relativedelta(day=1, days=-1)
    qs = TimeRecord.objects.filter(
        date_started__gte=from_date, date_started__lte=to_date
    )
    for row in qs:
        if row.is_complete:
            user_name = row.user.username
            if not user_name in result:
                result[user_name] = collections.OrderedDict()
                x = from_date
                while x < to_date:
                    key = x.strftime("%Y-%m")
                    result[user_name][key] = {
                        "label": x.strftime("%b"),
                        "month": x.month,
                        "year": x.year,
                        "analysis": TimeAnalysis(),
                    }
                    x = x + relativedelta(months=+1, day=1)
            key = row.date_started.strftime("%Y-%m")
            result[user_name][key]["analysis"].add(row)
    return result


class TimeAnalysisByUserTicketReport(ReportMixin):
    """Time analysis by user.

    .. note:: The report will expand the time range to the first and last day of
              the month.

    02/12/2020, Quickly assembled report with no testing...

    To Do

    1. Write tests
    2. Add parameters - user, from date and to date
       (reintroduce the ``_check_parameters`` method).

    """

    REPORT_SLUG = "invoice-time-analysis-user-ticket"
    REPORT_TITLE = "Time Analysis by User / Ticket (last 3 months)"
    form_class = ReportParametersEmptyForm

    # def _check_parameters(self, parameters):
    #    if not parameters:
    #        raise ReportError("Cannot run report without any parameters")
    #    user_pk = parameters.get("user_pk")
    #    from_date = parse(parameters.get("from_date"))
    #    to_date = parse(parameters.get("to_date"))
    #    if not (user_pk and from_date and to_date):
    #        raise ReportError(
    #            "The 'TimeSummaryByUserReport' report needs "
    #            "a user a from date and to date"
    #        )
    #    return user_pk, from_date, to_date

    def _time_records_by_ticket_year_month(
        self,
    ):  # , from_date, to_date, user):
        result = collections.OrderedDict()
        # temp - previous 3 months
        today = timezone.now()
        from_date = today + relativedelta(months=-3)
        to_date = today
        # first day of the month
        from_date = from_date + relativedelta(day=1)
        # last day of the month
        to_date = to_date + relativedelta(day=1, days=-1)
        # for d in rrule(MONTHLY, dtstart=from_date, until=to_date):
        #    result[d.date()] = TimeAnalysis()
        qs = TimeRecord.objects.filter(
            date_started__gte=from_date,
            date_started__lte=to_date,  # , user=user
            # user__username="tim.bushell",
        ).order_by(
            "user__username",
            "date_started__year",
            "date_started__month",
            "ticket__contact__user__username",
            "ticket__pk",
        )
        for row in qs:
            if row.is_complete:
                # print(
                #     row.created,
                #     row.date_started + relativedelta(day=1),
                #     row.user.username,
                # )
                key = UserTicketYearMonth(
                    user_name=row.user.username,
                    year_month=row.date_started + relativedelta(day=1),
                    ticket_pk=row.ticket.pk,
                )
                if not key in result:
                    result[key] = TimeAnalysis()
                result[key].add(row)
        return result

    def run_csv_report(self, csv_writer, parameters=None):
        count = 0
        csv_writer.writerow(
            (
                "user",
                "year",
                "month",
                "contact",
                "ticket",
                "description",
                "non-charge",
                "fixed",
                "charge",
                "url",
            )
        )
        # user_pk, from_date, to_date = self._check_parameters(parameters)
        data = self._time_records_by_ticket_year_month()
        for detail, analysis in data.items():
            count = count + 1
            ticket = Ticket.objects.get(pk=detail.ticket_pk)
            absolute_url = ticket.get_absolute_url()
            url = urllib.parse.urljoin(settings.HOST_NAME, absolute_url)
            csv_writer.writerow(
                [
                    detail.user_name,
                    detail.year_month.year,
                    detail.year_month.month,
                    str(ticket.contact),
                    ticket.number,
                    ticket.title,
                    analysis.non_charge,
                    analysis.fixed,
                    analysis.charge,
                    url,
                ]
            )
        return count

    def user_passes_test(self, user):
        return user.is_staff


class TimeAnalysisByContact(ReportMixin):
    REPORT_SLUG = "invoice-time-analysis-contact"
    REPORT_TITLE = "Time Analysis by Contact"

    def _check_parameters(self, parameters):
        if not parameters:
            raise ReportError("Cannot run report without any parameters")
        contact_pk = parameters.get("contact_pk")
        from_date = parse(parameters.get("from_date"))
        to_date = parse(parameters.get("to_date"))
        if not (contact_pk and from_date and to_date):
            raise ReportError(
                "The 'time analysis by contact' report needs "
                "a contact, from date and to date"
            )
        return contact_pk, from_date, to_date

    def run_csv_report(self, csv_writer, parameters=None):
        contact_pk, from_date, to_date = self._check_parameters(parameters)
        contact = get_contact_model().objects.get(pk=contact_pk)
        report = TimeRecord.objects.report_time_by_crm_contact(
            contact, from_date, to_date
        )
        csv_writer.writerow(
            ("Ticket", "Description", "Charge", "Fixed", "Non-Charge")
        )
        for row in report:
            csv_writer.writerow(
                (
                    row.pk,
                    row.description,
                    format_minutes(row.data.charge),
                    format_minutes(row.data.fixed),
                    format_minutes(row.data.non_charge),
                )
            )
        return True


class TimeSummaryByUserReport(ReportMixin):
    def run_csv_report(self, csv_writer, parameters=None):
        csv_writer.writerow(
            (
                "user_name",
                "year",
                "month",
                "label",
                "non_minutes",
                "fixed_minutes",
                "charge_minutes",
            )
        )
        summary = time_summary_by_user(date.today())
        for user_name, data in summary.items():
            for year_month, values in data.items():
                analysis = values["analysis"]
                csv_writer.writerow(
                    (
                        user_name,
                        values["year"],
                        values["month"],
                        values["label"],
                        analysis.non_charge,
                        analysis.fixed,
                        analysis.charge,
                    )
                )
        return True
