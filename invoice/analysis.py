# -*- encoding: utf-8 -*-
import logging
import pandas

from crm.models import KanbanCard
from invoice.models import format_minutes, InvoiceError, TimeRecord
from sales_order_ticket.models import SalesOrderTicket


logger = logging.getLogger(__name__)


def _check_tickets_have_a_sales_order():
    """Check all of the tickets have a sales order."""
    qs = SalesOrderTicket.objects.tickets_without_sales_order()
    if qs.count():
        message = "The following tickets need to be linked to a sales order: "
        message = message + ", ".join([str(x.pk) for x in qs])
        logger.error(message)
        raise InvoiceError(message)


def _kanban_card(ticket):
    try:
        return KanbanCard.objects.get(ticket=ticket)
    except KanbanCard.DoesNotExist:
        raise InvoiceError(
            f"Cannot find 'KanbanCard' for ticket {ticket.number}"
        )


def sales_order_invoice(sales_order_time):
    from rich.pretty import pprint
    from rich import print as rprint

    # all of the data
    df = pandas.DataFrame(sales_order_time[1:], columns=sales_order_time[0])
    print()
    rprint("[yellow]df")
    pprint(df, expand_all=True)
    rprint("[yellow]to_invoice")
    # outstanding time ('to_invoice')
    to_invoice = df.loc[df["invoiced"] == False]
    pprint(to_invoice, expand_all=True)
    # summary ('to_invoice')
    rprint("[yellow]to_invoice - pivot_table")
    to_invoice_pivot = pandas.pivot_table(
        to_invoice,
        values=["minutes"],
        index=["ticket"],
        columns=["sales-order"],
        aggfunc="sum",
    ).stack()
    pprint(to_invoice_pivot, expand_all=True)
    rprint("[yellow]invoiced - pivot_table")
    invoiced = df.loc[df["invoiced"] == True]
    invoiced_pivot = pandas.pivot_table(
        invoiced,
        values=["minutes"],
        index=["sales-order"],
        aggfunc="sum",
    ).stack()
    pprint(invoiced_pivot, expand_all=True)

    return to_invoice_pivot.to_json()


def sales_order_time():
    _check_tickets_have_a_sales_order()
    data = [
        [
            "sales-order",
            # "sales-order-hours-type",
            "ticket",
            "title",
            "user",
            "time-code",
            "date-started",
            "invoiced",
            "minutes",
        ]
    ]
    total = 0
    ticket_pks = list(
        TimeRecord.objects.to_invoice().values_list("ticket__pk", flat=True)
    )
    qs = TimeRecord.objects.filter(
        ticket__pk__in=ticket_pks,
        billable=True,
        date_started__isnull=False,
        start_time__isnull=False,
        end_time__isnull=False,
    ).order_by("ticket__pk", "date_started", "start_time")
    for time_record in qs:
        total = total + time_record.minutes
        sales_order_ticket = SalesOrderTicket.objects.get(
            ticket=time_record.ticket
        )
        data.append(
            [
                sales_order_ticket.sales_order.pk,
                # sales_order_ticket.sales_order.hours_type,
                time_record.ticket.pk,
                time_record.ticket.title,
                time_record.ticket.contact.user.username,
                time_record.time_code.description,
                time_record.date_started,
                time_record.has_invoice_line,
                int(time_record.minutes),
            ]
        )
    return data


# def pandas():
#     self.stdout.write(f"\nTotal: {format_minutes(total)}")
#    # pprint(data, expand_all=True)
#     df = pandas.DataFrame(
#        data, columns=["ticket", "title", "contact", "time-code", "minutes"]
#     )
#     pprint(df, expand_all=True)
#     table = pandas.pivot_table(
#        df,
#        values="minutes",
#        index=["contact", "ticket", "time-code"],
#        # columns=["ticket", "title", "contact", "time-code", "minutes"],
#        aggfunc=numpy.sum,
#        fill_value=0,
#     )
#     pprint(table, expand_all=True)
#     result = table.to_dict()
#     pprint(result, expand_all=True)


def time_records_to_data_frame(qs):
    data = []
    total = 0
    for time_record in qs:
        if time_record.is_complete:
            total = total + time_record.minutes
            kanban_card = _kanban_card(ticket=time_record.ticket)
            ticket_free_of_charge = False
            if time_record.ticket.funded:
                # are we doing this piece of work FOC?
                ticket_free_of_charge = time_record.ticket.funded.free_of_charge
            time_code = ""
            if time_record.time_code:
                time_code = time_record.time_code.description
            sales_order_contact = time_record.ticket.contact.user.username
            sales_order_funded = sales_order_number = sales_order_title = ""
            # ticket = f"{time_record.ticket.pk}. {time_record.ticket.title}"
            if ticket_free_of_charge:
                sales_order_funded = "foc"
                # sales_order_title = "FOC"
            elif kanban_card.column.lane.funded:
                # we should find a sales order for this ticket
                sales_order_funded = "DOES-NOT-EXIST"
                # sales_order_title = "Does Not Exist"
            else:
                sales_order_funded = "not-funded"
                # sales_order_title = "Not Funded"
            try:
                sales_order_ticket = SalesOrderTicket.objects.get(
                    ticket=time_record.ticket
                )
                sales_order_funded = "sales-order"
                sales_order_number = (
                    sales_order_ticket.sales_order.sales_order_number
                )
                sales_order_title = sales_order_ticket.sales_order.title
                sales_order_contact = (
                    sales_order_ticket.sales_order.contact.user.username
                )
            except SalesOrderTicket.DoesNotExist:
                pass
            data.append(
                [
                    sales_order_contact,
                    sales_order_number,
                    sales_order_title,
                    sales_order_funded,
                    # f"{sales_order_number}. {sales_order_title}",
                    time_record.ticket.number,
                    time_record.ticket.title,
                    # ticket,
                    ticket_free_of_charge,
                    kanban_card.column.lane.funded,
                    # pandas.Timestamp(time_record.date_started),
                    time_record.pk,
                    time_record.date_started.year,
                    time_record.date_started.month,
                    time_record.date_started.day,
                    time_record.user.username,
                    time_code,
                    time_record.billable,
                    time_record.has_invoice_line,
                    time_record.minutes,
                ]
            )
    df = pandas.DataFrame(
        data,
        columns=[
            "contact",
            "sales-order-number",
            "sales-order",
            "sales-order-funded",
            "ticket-number",
            "ticket",
            "ticket-free-of-charge",
            "kanban-lane-funded",
            # "date",
            "time-record-pk",
            "year",
            "month",
            "day",
            "user",
            "time-code",
            "billable",
            "invoiced",
            "minutes",
        ],
    )
    # df["date"] = pandas.to_datetime(df["date"])
    total_df = df["minutes"].sum()
    print(
        f"\nTotal: {format_minutes(total)} == {format_minutes(total_df)} (total for 'pandas.DataFrame')"
    )
    return df
