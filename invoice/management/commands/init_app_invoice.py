# -*- encoding: utf-8 -*-
from django.core.management.base import BaseCommand

from invoice.reports import (
    TimeAnalysisByContact,
    TimeAnalysisByUserTicketReport,
)


class Command(BaseCommand):
    help = "Initialise invoice application"

    # def _init_time_analysis_by_contact(self):
    #    ReportSpecification.objects.init_report_specification(
    #       slug="invoice-time-analysis-contact",
    #       title="Time Analysis by Contact",
    #       app="invoice",
    #       report_class="TimeAnalysisByContact",
    #       can_update_report=False,
    #    )

    def handle(self, *args, **options):
        self.stdout.write("{}".format(self.help))
        TimeAnalysisByContact().init_report()
        TimeAnalysisByUserTicketReport().init_report()
        self.stdout.write("{} - Complete".format(self.help))
