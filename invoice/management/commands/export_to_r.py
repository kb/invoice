# -*- encoding: utf-8 -*-
from django.core.management.base import BaseCommand

from invoice.service import export_to_r


class Command(BaseCommand):
    help = "Export invoices to CSV file (for R) #3785"

    def handle(self, *args, **options):
        self.stdout.write("{}".format(self.help))
        with open("export_to_r.csv", "w", newline="") as out:
            export_to_r(out)
        self.stdout.write("{} - Complete".format(self.help))
