# -*- encoding: utf-8 -*-
import csv

from dateutil.relativedelta import relativedelta
from decimal import Decimal
from django.core.management.base import BaseCommand
from django.utils import timezone

from invoice.models import TimeRecord


class Command(BaseCommand):
    help = "Export time records as CSV (for Pandas)"

    def handle(self, *args, **options):
        self.stdout.write("{}".format(self.help))

        today = timezone.now() + relativedelta(months=-1)

        first_day = (today + relativedelta(months=-1, day=1)).date()
        last_day = (today + relativedelta(day=1, days=-1)).date()

        self.stdout.write("From {}".format(first_day))
        self.stdout.write("  To {}".format(last_day))

        with open("time-records.csv", "w", newline="", encoding="utf-8") as out:
            csv_writer = csv.writer(out, dialect="excel-tab")
            csv_writer.writerow(
                ["Date", "User", "Minutes", "Ticket", "Code", "Billable"]
            )
            qs = TimeRecord.objects.filter(
                date_started__gte=first_day,
                date_started__lte=last_day,
            ).order_by("date_started", "start_time")
            for time_record in qs:
                self.stdout.write(f"  {time_record.date_started}")
                minutes = Decimal(time_record.minutes).quantize(Decimal(".01"))
                time_code = ""
                if time_record.time_code:
                    time_code = time_record.time_code.description
                csv_writer.writerow(
                    [
                        f"{time_record.date_started}",
                        f"{time_record.user.username}",
                        f"{minutes}",
                        f"{time_record.ticket.number}",
                        f"{time_code}",
                        f"{time_record.billable}",
                    ]
                )

        self.stdout.write("{} - Complete".format(self.help))
