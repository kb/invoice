# -*- encoding: utf-8 -*-
import collections

from datetime import date
from django.core.management.base import BaseCommand

from invoice.models import (
    format_minutes,
    get_contact_model,
    TicketSummary,
    TimeAnalysis,
    TimeRecord,
)


class Command(BaseCommand):
    """Report of time for by developer (excluding a list of tickets).

    Originally copied from:
    ``TimeRecordManager``, ``report_time_by_crm_contact``.

    https://www.kbsoftware.co.uk/crm/ticket/4473/

    """

    help = "Time by developer for a contact #4473"

    def _print(self, report):
        format_string = "{:8.8}  {:40.40}  {:7.7}  {:7.7}  {:7.7}"
        print(
            format_string.format(
                "Ticket", "Description", "Charge", "Fixed", "Non-Chg"
            )
        )
        for row in report:
            print(
                format_string.format(
                    str(row.pk),
                    row.description,
                    format_minutes(row.data.charge),
                    format_minutes(row.data.fixed),
                    format_minutes(row.data.non_charge),
                )
            )

    def _report(self):
        contact = get_contact_model().objects.get(user__username="jasmine")
        from_date = date(2019, 1, 1)
        to_date = date(2019, 3, 31)
        self.stdout.write(
            "Reporting on time for {} ('{}') from {} to {}".format(
                contact.full_name,
                contact.user.username,
                from_date.strftime("%d/%m/%Y"),
                to_date.strftime("%d/%m/%Y"),
            )
        )
        qs = TimeRecord.objects.filter(
            ticket__contact=contact,
            date_started__gte=from_date,
            date_started__lte=to_date,
        ).order_by("-start_time")
        result = collections.OrderedDict()
        for row in qs:
            # only add tickets with completed time records
            if row.is_complete:
                ticket_pk = row.ticket.pk
                if not ticket_pk in result:
                    result[ticket_pk] = TicketSummary(
                        pk=ticket_pk,
                        description=row.ticket.title,
                        contact=row.ticket.contact.full_name,
                        user_name=row.ticket.contact.user.username,
                        data=TimeAnalysis(),
                    )
                result[ticket_pk].data.add(row)
        return list(result.values())

    def handle(self, *args, **options):
        self.stdout.write("{}".format(self.help))
        report = self._report()
        self._print(report)
        self.stdout.write("{} - Complete".format(self.help))
