# -*- encoding: utf-8 -*-
import pandas

from django.core.management.base import CommandError
from django.db.models import Sum
from rich.pretty import pprint

from crm.models import KanbanCard
from invoice.models import format_minutes, InvoiceLine, InvoiceSettings
from sales_order_ticket.models import SalesOrderTicket


def display_data_frame(caption, df):
    print()
    print(caption)
    pprint(df, expand_all=True)


def get_invoice_total_minutes(invoice):
    invoice_settings = InvoiceSettings.objects.settings()
    if not invoice_settings.time_record_product:
        raise CommandError(
            "The invoice settings need a "
            "product selected to use for time records."
        )
    qs = (
        InvoiceLine.objects.filter(invoice=invoice)
        .filter(product=invoice_settings.time_record_product)
        .aggregate(Sum("quantity"))
    )
    invoice_line_quantity = qs["quantity__sum"]
    print(invoice_line_quantity)
    return invoice_line_quantity
