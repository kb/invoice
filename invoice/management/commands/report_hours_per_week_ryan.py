# -*- encoding: utf-8 -*-
import collections
import csv
import tempfile

from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand
from django.utils import timezone

from contact.models import Contact
from invoice.models import TimeRecord
from invoice.pdf import TicketTimeReport


class Command(BaseCommand):
    def handle(self, *args, **options):
        table_data = []

        pdf = TicketTimeReport()
        report_title = "Ryan's Weekly Work Hours"
        name = "ryan"
        # pdf.data(name)
        pdf.create_pdf(report_title, name)
        return
