# -*- encoding: utf-8 -*-
import csv

from datetime import date
from decimal import Decimal

from django.apps import apps
from django.conf import settings
from django.core.management.base import BaseCommand

from crm.models import Ticket
from invoice.models import TimeRecord


class Command(BaseCommand):
    help = "Time by Contact"

    def _time_records(self, ticket):
        """Total time by user for a ticket.

        Return a dictionary containing the user name and total minutes e.g::

          {
              'malcolm.dinsmore': 442.0,
              'Greg': 25.016666666666666,
              'patrick.kimber': 32.5,
          }

        """
        result = {}
        qs = TimeRecord.objects.filter(
            ticket=ticket, date_started__gte=date(2017, 5, 22)
        ).order_by("date_started")
        for time_record in qs:
            user_name = time_record.user.username
            if user_name not in result:
                result[user_name] = 0
            minutes = time_record.minutes
            result[user_name] = result[user_name] + minutes
        return result

    def add_arguments(self, parser):
        parser.add_argument("pk", nargs="+", type=int)

    def handle(self, *args, **options):
        result = {}
        user_names = set()
        contact_model = apps.get_model(settings.CONTACT_MODEL)
        for pk in options["pk"]:
            contact = contact_model.objects.get(pk=pk)
            self.stdout.write("{}".format(contact))
            for ticket in Ticket.objects.contact(contact):
                time_summary = self._time_records(ticket)
                if time_summary:
                    self.stdout.write(
                        "  {:06}  {}".format(
                            ticket.pk, ticket.title, time_summary
                        )
                    )
                    result[ticket.pk] = time_summary
                    for k, v in time_summary.items():
                        user_names.add(k)
        user_names = list(user_names)
        user_names.sort()
        with open("out.csv", "w", newline="") as out:
            csv_writer = csv.writer(out, dialect="excel-tab")
            row = ["ticket", "description"]
            for user_name in user_names:
                row.append(user_name)
            csv_writer.writerow(row)
            for ticket_pk, time_summary in result.items():
                ticket = Ticket.objects.get(pk=ticket_pk)
                row = [ticket.pk, ticket.title]
                for user_name in user_names:
                    total = Decimal()
                    value = time_summary.get(user_name)
                    if value:
                        total = Decimal(value) / Decimal("60")
                    row.append(total.quantize(Decimal(".01")))
                csv_writer.writerow(row)
        self.stdout.write("Complete...")
