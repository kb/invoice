# -*- encoding: utf-8 -*-
import csv

from django.core.management.base import BaseCommand
from django.utils import timezone

from invoice.models import Invoice


class Command(BaseCommand):
    help = "Invoice lines (excluding time)"

    def add_arguments(self, parser):
        parser.add_argument("invoice_pk", type=int)

    def invoice_lines(self, invoice):
        result = []
        for invoice_line in invoice.invoiceline_set.all().order_by(
            "timerecord__created"
        ):
            if invoice_line.has_time_record:
                pass
            else:
                print(
                    invoice_line.invoice.invoice_number,
                    invoice_line.quantity,
                    invoice_line.description,
                    invoice_line.net,
                )
                result.append(
                    [
                        invoice_line.invoice.invoice_date.strftime("%d/%m/%Y"),
                        invoice_line.invoice.contact.user.username,
                        invoice_line.invoice.invoice_number,
                        invoice_line.quantity,
                        invoice_line.net,
                        invoice_line.description,
                    ]
                )
        return result

    def handle(self, *args, **options):
        self.stdout.write(f"{self.help}")
        data = []
        invoice_pk = options.get("invoice_pk")
        invoice = Invoice.objects.get(pk=invoice_pk)
        self.stdout.write(invoice.invoice_date.strftime("%d/%m/%Y"))
        data = data + self.invoice_lines(invoice)
        file_name = "invoice-lines-{}-time-excluded.csv".format(
            timezone.now().strftime("%Y-%m-%d-%H%M%S")
        )
        with open(file_name, "w", newline="") as out:
            csv_writer = csv.writer(out, dialect="excel-tab")
            csv_writer.writerow(
                ["Date", "Contact", "Number", "Description", "Quantity", "Net"]
            )
            for row in data:
                csv_writer.writerow(row)
        self.stdout.write(f"Report written to '{file_name}'...")
