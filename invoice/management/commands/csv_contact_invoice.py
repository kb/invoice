# -*- encoding: utf-8 -*-
import csv

from datetime import datetime
from django.core.management.base import BaseCommand

from contact.models import Contact
from invoice.models import Invoice


class Command(BaseCommand):
    help = "CSV file of Contacts (with Invoice count)"

    def handle(self, *args, **options):
        self.stdout.write("{}".format(self.help))
        file_name = "contact-invoice-{}.csv".format(
            datetime.now().strftime("%Y-%m-%d-%H-%M-%S")
        )
        qs = Contact.current.all().order_by("user__username")
        with open(file_name, "w", newline="") as out:
            csv_writer = csv.writer(out, dialect="excel-tab")
            for x in qs:
                invoice_count = Invoice.objects.current(x).count()
                csv_writer.writerow(
                    [x.full_name.strip(), x.email().strip(), invoice_count]
                )
        self.stdout.write("{} - Complete".format(self.help))
