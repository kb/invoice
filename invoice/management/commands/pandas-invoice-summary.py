# -*- encoding: utf-8 -*-
import pandas

from decimal import Decimal, ROUND_UP
from django.core.management.base import BaseCommand
from django.utils import timezone
from reportlab import platypus
from reportlab.lib import colors
from reportlab.lib.pagesizes import A4
from rich.pretty import pprint

from crm.models import Ticket
from invoice.analysis import time_records_to_data_frame
from invoice.models import Invoice, TimeRecord
from report.pdf import PDFReport, NumberedCanvas
from sales_order.models import SalesOrder
from .util import display_data_frame, get_invoice_total_minutes


def get_time_records_for_invoice(invoice):
    return TimeRecord.objects.filter(invoice_line__invoice=invoice).order_by(
        "date_started", "start_time"
    )


def invoice_analysis(invoice):
    time_records = get_time_records_for_invoice(invoice)
    df = time_records_to_data_frame(time_records)
    file_name_time = timezone.now().strftime("%Y-%m-%d")
    with pandas.ExcelWriter(
        f"{file_name_time}-invoice-{invoice.invoice_number}.xlsx"
    ) as writer:
        df.to_excel(writer, sheet_name="Time Records")
        display_data_frame(
            f"Sales order analysis for invoice {invoice.invoice_number}",
            df.sort_values(
                by=[
                    "contact",
                    "sales-order-number",
                    "ticket-number",
                    "year",
                    "month",
                ]
            ),
        )
        sales_order_ticket_df = df.groupby(
            ["sales-order-number", "ticket-number"]
        ).agg({"minutes": "sum"})
        display_data_frame(
            "Sales Order / Ticket Analysis", sales_order_ticket_df
        )
        sales_order_ticket_df = sales_order_ticket_df.reset_index()
        sales_order_tickets = sales_order_ticket_df.to_dict(orient="records")
        total_minutes = Decimal()
        for x in sales_order_tickets:
            minutes = x["minutes"]
            total_minutes = total_minutes + Decimal(str(minutes))
        pprint(sales_order_tickets)
        print(f"total_minutes: {total_minutes}")
        total = Decimal()
        total = Decimal(total_minutes) / Decimal("60")
        total = total.quantize(Decimal(".01"), rounding=ROUND_UP)
        invoice_total_minutes = get_invoice_total_minutes(invoice)
        print(f"total: {total} == {invoice_total_minutes}")
        print()
        return sales_order_tickets


class SummaryReport(PDFReport):
    def _get_column_styles(self, column_widths):
        # style - add vertical grid lines
        style = []
        for idx in range(len(column_widths) - 1):
            style.append(
                (
                    "LINEAFTER",
                    (idx, 0),
                    (idx, -1),
                    self.GRID_LINE_WIDTH,
                    colors.gray,
                )
            )
        return style

    def _title(self, invoice_number):
        return f"Invoice {invoice_number} - Summary - Sales Orders and Tickets"

    def _total_data(self, total):
        return [
            self._bold("TOTAL"),
            "",
            "",
            self._bold(total, align=self.RIGHT),
        ]

    def _total_data_sales_order(self, total):
        return [
            "",
            "",
            self._bold("Total", align=self.RIGHT),
            self._bold(total, align=self.RIGHT),
        ]

    def _total_style(self, index, shading=None):
        result = [
            (
                "LINEABOVE",
                (0, index - 1),
                (-1, index - 1),
                self.GRID_LINE_WIDTH,
                colors.gray,
            ),
            (
                "LINEBELOW",
                (0, index - 1),
                (-1, index - 1),
                self.GRID_LINE_WIDTH,
                colors.black,
            ),
        ]
        if shading:
            colour = colors.lightblue
        else:
            colour = colors.lightyellow
        result.append(
            (
                "BACKGROUND",
                (0, index - 1),
                (-1, index - 1),
                colour,
            )
        )
        return result

    def summary_as_table(self, invoice, sales_order_tickets):
        data = []
        style = [
            ("ALIGN", (2, 0), (-1, -1), "RIGHT"),
            ("LINEABOVE", (0, 0), (-1, 0), 1, colors.black),
            ("LINEBELOW", (0, 0), (-1, 0), self.GRID_LINE_WIDTH, colors.gray),
            ("VALIGN", (0, 0), (0, -1), "TOP"),
        ]
        data.append(
            [
                self._bold("Sales Order"),
                self._bold("Description"),
                self._bold("Ticket", align=self.RIGHT),
                self._bold("Hours", align=self.RIGHT),
            ]
        )
        # count = 0
        total_hours = Decimal()
        total_hours_sales_order = Decimal()
        previous_sales_order = None
        for x in sales_order_tickets:
            new_sales_order = False
            sales_order_number = int(x["sales-order-number"])
            if previous_sales_order == sales_order_number:
                pass
            else:
                new_sales_order = True
                previous_sales_order = sales_order_number
                if total_hours_sales_order:
                    data.append(
                        self._total_data_sales_order(total_hours_sales_order)
                    )
                    style = style + self._total_style(len(data))
                    total_hours_sales_order = Decimal()
            ticket_number = int(x["ticket-number"])
            minutes = x["minutes"]
            hours = self._round(Decimal(minutes) / Decimal("60"))
            total_hours = total_hours + hours
            total_hours_sales_order = total_hours_sales_order + hours
            ticket = Ticket.objects.get(pk=ticket_number)
            sales_order = SalesOrder.objects.get(pk=sales_order_number)
            if new_sales_order:
                data.append(
                    [
                        sales_order.sales_order_number,
                        self._bold(sales_order.title),
                        "",
                        "",
                    ]
                )
            data.append(
                [
                    "",
                    self._para(ticket.title),
                    ticket_number,
                    hours,
                ]
            )
        if total_hours_sales_order:
            data.append(self._total_data_sales_order(total_hours_sales_order))
            style = style + self._total_style(len(data))
        data.append(self._total_data(total_hours))
        style = style + self._total_style(len(data), shading=True)
        # table
        column_widths = [74, 450, 80, 80]
        style = style + self._get_column_styles(column_widths)
        summary_table = platypus.Table(
            data,
            colWidths=column_widths,
            repeatRows=1,
            style=style,
        )
        doc = platypus.SimpleDocTemplate(
            f"pandas-invoice-summary-{invoice.invoice_number}.pdf",
            title=self._title(invoice.invoice_number),
            pagesize=(A4[1], A4[0]),
        )
        elements = []
        report_title = self._head_3(self._title(invoice.invoice_number))
        elements.append(report_title)
        elements.append(summary_table)
        doc.build(elements, canvasmaker=NumberedCanvas)


class Command(BaseCommand):
    """Invoice summary.

    Code based on::

      invoice/management/commands/pandas-time-record.py

    """

    help = "Invoice summary (with Pandas) #1017"

    def add_arguments(self, parser):
        parser.add_argument("invoice_pk", type=int)

    def handle(self, *args, **options):
        self.stdout.write(f"{self.help}")
        invoice_pk = options["invoice_pk"]
        invoice = Invoice.objects.get(pk=invoice_pk)
        self.stdout.write(
            f"Invoice {invoice.invoice_number} for "
            f"{invoice.contact} "
            f"dated {invoice.invoice_date}"
        )
        sales_order_tickets = invoice_analysis(invoice)
        SummaryReport().summary_as_table(invoice, sales_order_tickets)
        self.stdout.write(f"{self.help} - Complete")
