# -*- encoding: utf-8 -*-
"""
Copy ticket and time recording information to an ElasticSearch index.

To see the data:

1. Restore the data, ``fab domain:www.kbsoftware.co.uk restore:backup``
2. Start ElasticSearch and Kibana
3. Update the ElasticSearch indexes, ``django-admin.py update_kibana``
4. Browse to the Kibana dashboard, http://localhost:5601/
   *Dashboard*, *Last 1 year*.

"""
import logging

from dateutil.relativedelta import relativedelta
from django.conf import settings
from django.core.management.base import BaseCommand
from django.utils import timezone
from django.utils.text import slugify
from elasticsearch import Elasticsearch, NotFoundError
from elasticsearch.helpers import streaming_bulk

from invoice.models import TimeAnalysis, TimeRecord


INVOICE_INDEX = "invoice"
logger = logging.getLogger(__name__)


def elasticsearch():
    return Elasticsearch(
        [
            {
                "host": settings.ELASTICSEARCH_HOST,
                "port": settings.ELASTICSEARCH_PORT,
            }
        ]
    )


def es_index_name(index_name):
    """If this is a test site, then prepend the index name with 'testing_'."""
    if settings.TESTING:
        return "testing_{}".format(index_name)
    else:
        return index_name


def index_settings():
    """Index settings.

    Index-Time Search-as-You-Type:
    https://www.elastic.co/guide/en/elasticsearch/guide/current/_index_time_search_as_you_type.html

    Edge NGram tokenizer - Configuration
    https://www.elastic.co/guide/en/elasticsearch/reference/current/analysis-edgengram-tokenizer.html#_configuration_17

    """
    return {"number_of_shards": 1}


class InvoiceIndex:
    DOC_TYPE_INVOICE = "invoice"

    @staticmethod
    def create(es, index_name):
        """Create ElasticSearch index.

        See ``part.py`` for links to ElasticSearch documentation.

        """
        es.indices.create(
            es_index_name(index_name),
            {
                "mappings": {
                    InvoiceIndex.DOC_TYPE_INVOICE: {
                        "properties": {
                            "charge": {"type": "float"},
                            "charge_fixed": {"type": "float"},
                            "date": {
                                "type": "date",
                                "format": "strict_date_hour_minute",
                            },
                            "fixed": {"type": "float"},
                            "kanban_lane": {"type": "keyword"},
                            "non_charge": {"type": "float"},
                            "root_ticket": {"type": "long"},
                            "ticket_title": {"type": "keyword"},
                            "time_code": {"type": "keyword"},
                            "title": {"type": "text", "analyzer": "english"},
                            "total": {"type": "float"},
                            "user": {"type": "keyword"},
                        }
                    }
                },
                "settings": {"index": index_settings()},
            },
        )

    @staticmethod
    def parse(index_name, update_recent_only=None):
        from_date = timezone.now() + relativedelta(years=-1, day=1)
        to_date = from_date + relativedelta(years=+1, day=1, days=-1)
        # to_date = timezone.now()
        qs = TimeRecord.objects.filter(
            date_started__gte=from_date, date_started__lte=to_date
        ).filter(
            user__username__in=[
                "chris.meardon",
                "Greg",
                "malcolm.dinsmore",
                "patrick.kimber",
                "tim.bushell",
            ]
        )
        for row in qs.order_by("-pk"):
            if row.is_complete:
                analysis = TimeAnalysis()
                analysis.add(row)
                date_started = "{}T{}".format(
                    row.date_started.strftime("%Y-%m-%d"),
                    row.start_time.strftime("%H:%M"),
                )
                time_code = "none"
                if row.time_code:
                    time_code = row.time_code.description
                kanban_lane = "none"
                kanban_card = row.ticket.kanban_card()
                if kanban_card:
                    kanban_lane = kanban_card.column.lane.title
                yield {
                    "_op_type": "index",
                    "_index": es_index_name(index_name),
                    "_type": InvoiceIndex.DOC_TYPE_INVOICE,
                    "_id": row.pk,
                    "_source": {
                        "billable": row.billable,
                        "charge": analysis.charge / 60,
                        "charge_fixed": (analysis.charge + analysis.fixed) / 60,
                        "date": date_started,
                        "fixed": analysis.fixed / 60,
                        "kanban_lane": kanban_lane,
                        "non_charge": analysis.non_charge / 60,
                        "root_ticket": _root_ticket(row.ticket),
                        "ticket_title": _ticket_title(row.ticket),
                        "time_code": time_code,
                        "title": row.title,
                        "total": analysis.total / 60,
                        "user": row.user.username,
                    },
                }


INDEXES = {INVOICE_INDEX: InvoiceIndex}


def _create(es, index_name):
    """Create ElasticSearch index.

    In a private function, so can easily be mocked.

    """
    return INDEXES[index_name].create(es, index_name)


def _convert_date_time_field(date_time_field):
    """

    "yyyy-MM-dd HH:mm:ss",

    """
    return date_time_field.strftime("%Y-%m-%d %H:%M:%S")


def _delete(es, index_name):
    """Delete ElasticSearch index.

    In a private function, so can easily be mocked.

    """
    es.indices.delete(index_name)


def _parse(index_name, update_recent_only):
    return INDEXES[index_name].parse(index_name, update_recent_only)


def _root_ticket(ticket):
    result = None
    if ticket.is_root_node():
        descendant_count = ticket.get_descendant_count()
        if descendant_count:
            result = ticket.pk
    else:
        root_ticket = ticket.get_root()
        if root_ticket:
            result = root_ticket.pk
    return result


def _ticket_title(ticket):
    result = "{}-{}".format(ticket.pk, slugify(ticket.title))
    return result[:40]


def _update_search_index(index_name, update_recent_only=None):
    """Update the ElasticSearch index for SkySpares.

    Keyword arguments:
    update_recent_only -- only update recently modified parts (default None)

    Streaming code copied from:
    https://github.com/elastic/elasticsearch-py/blob/master/example/load.py

    - I am not sure what to think of the code here.  I am deleting documents
      even if they were never added to the index, so I have to tell the system
      not to raise an error (``raise_on_error=False``).
    - For the index refresh (running every five minutes) it would be good if we
      could find the parts which have recently changed to zero stock available,
      then we could just delete those items.

    """
    count = 0
    es = elasticsearch()
    for ok, result in streaming_bulk(
        es, _parse(index_name, update_recent_only), raise_on_error=False
    ):
        action, result = result.popitem()
        # process the information from ES whether the document has been
        # successfully indexed
        if not ok:
            print(result)
            status = result["status"]
            found = result["found"]
            # not an error to try and delete a document which does not exist
            if action == "delete" and status == 404 and not found:
                pass
            else:
                doc_id = result["_id"]
                logger.error(
                    "Failed to {} document '{}': {}".format(
                        action, doc_id, result
                    )
                )
        else:
            count = count + 1
    es.indices.refresh(index_name)
    return count


def drop_create_search_index(index_name):
    """Create the Elasticsearch index."""
    es = elasticsearch()
    try:
        _delete(es, es_index_name(index_name))
    except NotFoundError:
        # not found is not a problem - the next step is to create it
        pass
    _create(es, index_name)


def rebuild_search_index(index_name):
    """Rebuild the whole index - once per day."""
    return _update_search_index(index_name)


class Command(BaseCommand):
    help = "Update Kibana index..."

    def handle(self, *args, **options):
        drop_create_search_index(INVOICE_INDEX)
        self.stdout.write("Created: '{}' index".format(INVOICE_INDEX))
        rebuild_search_index(INVOICE_INDEX)
        self.stdout.write("Complete: {}".format(self.help))
