# -*- encoding: utf-8 -*-
import pandas

from datetime import date
from dateutil.relativedelta import relativedelta
from dateutil.rrule import FR, SU
from decimal import Decimal
from django.core.management.base import BaseCommand
from django.utils import timezone
from rich.pretty import pprint

from invoice.analysis import time_records_to_data_frame
from invoice.models import TimeRecord
from sales_order.models import SalesOrder
from sales_order_ticket.models import SalesOrderTicket
from .util import display_data_frame


def get_time_records_for_sales_order(sales_order):
    """Time records for a sales order."""
    sales_order_tickets = SalesOrderTicket.objects.filter(
        sales_order=sales_order
    ).values_list("ticket__pk", flat=True)
    return (
        TimeRecord.objects.to_report(
            exclude_fixed_price=False, exclude_free_of_charge=True
        )
        .filter(ticket__in=sales_order_tickets)
        .order_by("date_started", "start_time")
    )


def get_time_records_for_user(user_name, from_date=None, to_date=None):
    today = timezone.now().date()
    last_friday = today + relativedelta(weekday=FR(-1))
    if not from_date:
        from_date = last_friday + relativedelta(weekday=SU(-1))
    if not to_date:
        to_date = last_friday + relativedelta(weekday=SU(1))
    print(f"From {from_date} to {to_date} for {user_name}")
    return TimeRecord.objects.filter(
        date_started__gte=from_date,
        date_started__lte=to_date,
        user__username=user_name,
    ).order_by("date_started", "start_time")


def get_time_records_to_invoice():
    """Time records to invoice - starting January 2023."""
    return (
        TimeRecord.objects.to_invoice(
            exclude_fixed_price=False, exclude_free_of_charge=True
        )
        .filter(date_started__gte=date(2023, 1, 1))
        .order_by("date_started", "start_time")
    )


def sales_order_analysis(sales_order_number, hourly_rate):
    sales_order = SalesOrder.objects.get(number=sales_order_number)
    print(sales_order)
    time_records = get_time_records_for_sales_order(sales_order)
    file_name_time = timezone.now().strftime("%Y-%m-%d")
    sales_order_df = time_records_to_data_frame(time_records)
    with pandas.ExcelWriter(
        f"{file_name_time}-sales-order-{sales_order.sales_order_number}.xlsx"
    ) as writer:
        sales_order_df.to_excel(writer, sheet_name="Time Records")
        display_data_frame(
            "Sales Order DataFrame",
            sales_order_df.sort_values(
                by=["contact", "ticket", "year", "month"]
            ),
        )
        print()
        summary = sales_order.summary()
        print(sales_order)
        pprint(summary, expand_all=True)
        total_minutes = sales_order_df["minutes"].sum()
        year_month_df = sales_order_df.groupby(["year", "month", "ticket"]).agg(
            {"minutes": "sum"}
        )
        display_data_frame("Tickets by Month", year_month_df)
        year_month_df.to_excel(writer, sheet_name="Tickets by Month")
        total_hours = Decimal(total_minutes / 60).quantize(Decimal(".01"))
        print()
        print(f"Sales order ({sales_order.get_hours_type_display()})")
        if sales_order.hours_type == SalesOrder.FIXED_PRICE:
            total_value = Decimal(total_hours * hourly_rate)
            caption = f"{total_hours} hours of work at £{hourly_rate}"
            print(f"- {caption:<30} {total_value:>10} ")
            if total_value > sales_order.value:
                print("Nothing to invoice")
                caption = "Sales order value"
                print(f"- {caption:<30} {sales_order.value:>10}")
                caption = "Total value of work"
                print(f"- {caption:<30} {total_value:>10}")
                caption = "Non-chargeable work"
                print(f"- {caption:<30} {total_value - sales_order.value:>10}")
            else:
                caption = "Value left to invoice"
                print(f"- {caption:<30} {sales_order.value - total_value:>10}")
        elif sales_order.hours_type == SalesOrder.HOURS_PER_MONTH:
            maximum_hours_per_month = sales_order.maximum_hours_per_ticket or 0
            maximum_minutes_per_month = maximum_hours_per_month * 60
            caption = "Maximum hours per month"
            print(f"- {caption:<30} {maximum_hours_per_month:>10}")
            caption = "Maximum minutes per month"
            print(f"- {caption:<30} {maximum_minutes_per_month:>10}")
            summary_df = pandas.DataFrame(
                data=[
                    ["Maximum hours per month", maximum_hours_per_month],
                    ["Maximum minutes per month", maximum_minutes_per_month],
                ],
                columns=["Caption", "Value"],
            )
            summary_df.to_excel(writer, sheet_name="Summary")
            year_month_group_df = sales_order_df.groupby(["year", "month"]).agg(
                {"minutes": "sum"}
            )
            display_data_frame("By Month", year_month_group_df)
            year_month_group_df.to_excel(writer, sheet_name="Total by Month")
            year_month_group_warning_df = year_month_group_df[
                year_month_group_df.minutes >= maximum_minutes_per_month
            ]
            display_data_frame(
                "Too much time spent in these months",
                year_month_group_warning_df,
            )
            year_month_group_warning_df.to_excel(
                writer, sheet_name="Too much time spent"
            )
            # )
        elif sales_order.hours_type == SalesOrder.HOURS_PER_TICKET:
            maximum_hours_per_ticket = sales_order.maximum_hours_per_ticket or 0
            maximum_minutes_per_ticket = maximum_hours_per_ticket * 60
            caption = "Maximum hours per ticket"
            print(f"- {caption:<30} {maximum_hours_per_ticket:>10}")
            caption = "Maximum minutes per ticket"
            print(f"- {caption:<30} {maximum_minutes_per_ticket:>10}")
            ticket_df = sales_order_df.groupby(["ticket"]).agg(
                {"minutes": "sum"}
            )
            display_data_frame("Sales Order by Ticket", ticket_df)
            display_data_frame(
                "Too much time spent on these",
                ticket_df[ticket_df.minutes >= maximum_minutes_per_ticket],
            )
        elif sales_order.hours_type in (
            SalesOrder.SUPPORT_ANNUAL,
            SalesOrder.SUPPORT_MONTHLY,
        ):
            support_value = sales_order.value or 0
            hourly_rate = sales_order.contact.invoicecontact.hourly_rate or 0
            caption = f"Value of {sales_order.get_hours_type_display()}"
            print(f"- {caption:<30} {support_value:>10}")
            caption = f"Hourly rate for this customer"
            print(f"- {caption:<30} {hourly_rate:>10}")
            if sales_order.hours_type == SalesOrder.SUPPORT_MONTHLY:
                caption = "Month"
                year_month_df = sales_order_df.groupby(["year", "month"]).agg(
                    {"minutes": "sum"}
                )
            else:
                caption = "Year"
                year_month_df = sales_order_df.groupby(["year"]).agg(
                    {"minutes": "sum"}
                )
            year_month_df["value_at_hourly_rate"] = (
                year_month_df["minutes"].astype(str).apply(Decimal)
                / Decimal(60)
                * Decimal(hourly_rate)
            )
            display_data_frame(f"Sales Orders by {caption}", year_month_df)
            display_data_frame(
                (
                    f"Too much time spent on these {caption.lower()}s "
                    f"(more than £{support_value})"
                ),
                year_month_df[
                    year_month_df.value_at_hourly_rate >= support_value
                ],
            )
    print()


def user_name_analysis(user_name):
    time_records = get_time_records_for_user(
        user_name, from_date=date(2024, 5, 1), to_date=date(2024, 5, 31)
    )
    df = time_records_to_data_frame(time_records)
    file_name_time = timezone.now().strftime("%Y-%m-%d")
    with pandas.ExcelWriter(
        f"{file_name_time}-user-name-{user_name}.xlsx"
    ) as writer:
        df.to_excel(writer, sheet_name="Time Records")
        display_data_frame(
            f"User Analysis for {user_name}",
            df.sort_values(by=["contact", "ticket", "year", "month"]),
        )
        print()


class Command(BaseCommand):
    help = "Time record analysis (with Pandas) (is WIP)"
    hourly_rate = Decimal(25)

    def display_pivot_table(self, caption, pivot_table):
        print()
        print(caption)
        pprint(pivot_table.to_dict(), expand_all=True)

    def example_pivot_tables(self, df):
        table = pandas.pivot_table(
            df,
            values="minutes",
            index=["contact", "ticket", "sales-order", "time-code"],
            # columns=["ticket", "title", "contact", "time-code", "minutes"],
            aggfunc="sum",
            fill_value=0,
        )
        self.display_pivot_table(
            (
                "Pivot table (sum of minutes by contact "
                "+ ticket + sales-order + time-code)"
            ),
            table,
        )
        # sales order - summary
        # remove non-billable time
        df = df[df.billable == False]
        table = pandas.pivot_table(
            df,
            values="minutes",
            index=["contact", "sales-order"],
            # index=["sales-order-contact", "sales-order"],
            # columns=["ticket", "title", "contact", "time-code", "minutes"],
            aggfunc="sum",
            fill_value=0,
        )
        self.display_pivot_table(
            "Pivot table (sum of minutes by contact + sales order)", table
        )
        # display sales orders
        table = pandas.pivot_table(
            df,
            values="minutes",
            index=["sales-order"],
            # index=["sales-order-contact", "sales-order"],
            # columns=["ticket", "title", "contact", "time-code", "minutes"],
            aggfunc="sum",
            fill_value=0,
        )
        self.display_pivot_table("Pivot table (sales orders)", table)

    def handle(self, *args, **options):
        self.stdout.write(f"{self.help}")
        # sales_order_analysis(8, self.hourly_rate)
        # sales_order_analysis(11, self.hourly_rate)
        # user_name = "chiverton-john"
        user_name = "patrick.kimber"
        # user_name = "malcolm.dinsmore"
        user_name_analysis(user_name)
        return
        # user_name = "malcolm"
        # qs = get_time_records_for_user(user_name)
        qs = get_time_records_to_invoice()
        outstanding_time_df = time_records_to_data_frame(qs)
        display_data_frame(
            "DataFrame",
            outstanding_time_df.sort_values(by=["contact", "ticket"]),
        )
        file_name_time = timezone.now().strftime("%Y-%m-%d")
        with pandas.ExcelWriter(
            f"{file_name_time}-outstanding-time.xlsx"
        ) as writer:
            outstanding_time_df.to_excel(writer, sheet_name="Outstanding Time")
        # self.example_pivot_tables(df)
        sales_orders = sorted(
            list(set(outstanding_time_df["sales-order"].tolist()))
        )
        print()
        print("Sales Orders")
        count = 0
        for sales_order_number in sales_orders:
            count = count + 1
            sales_order_analysis(sales_order_number, self.hourly_rate)
            if count > 5:
                break
        self.stdout.write(f"{self.help} - Complete")
