# -*- encoding: utf-8 -*-
from datetime import date, datetime
from decimal import Decimal

from braces.views import (
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    SuperuserRequiredMixin,
    UserPassesTestMixin,
)
from dateutil.relativedelta import relativedelta
from django.contrib import messages
from django.contrib.auth import REDIRECT_FIELD_NAME, get_user_model
from django.contrib.auth.decorators import user_passes_test
from django.core.exceptions import PermissionDenied
from django.db import transaction
from django.http import Http404, HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.utils import timezone
from django.views.decorators.cache import never_cache
from django.views.generic import (
    CreateView,
    FormView,
    ListView,
    TemplateView,
    UpdateView,
)
from django_sendfile import sendfile

from base.url_utils import url_with_querystring
from base.view_utils import BaseMixin, RedirectNextMixin
from contact.views import ContactPermMixin, check_perm_contact
from crm.models import Ticket
from crm.tasks import update_ticket_index
from finance.models import Currency
from report.models import ReportSpecification

from .forms import (
    ContactEmptyForm,
    InvoiceBlankTodayForm,
    InvoiceContactForm,
    InvoiceEmptyForm,
    InvoiceIssueForm,
    InvoiceLineForm,
    InvoiceSettingsForm,
    InvoiceUpdateForm,
    InvoiceUserUpdateForm,
    QuickTimeRecordEmptyForm,
    QuickTimeRecordForm,
    SearchForm,
    TimeRecordEmptyForm,
    TimeRecordForm,
)
from .models import (
    Batch,
    BatchInvoice,
    free_of_charge_ticket_pks,
    get_contact_model,
    Invoice,
    InvoiceContact,
    InvoiceCredit,
    InvoiceError,
    InvoiceIssue,
    InvoiceLine,
    InvoiceSettings,
    InvoiceUser,
    PaymentProcessor,
    QuickTimeRecord,
    TimeRecord,
)
from .report import (
    ReportInvoiceTimeAnalysis,
    ReportInvoiceTimeAnalysisCSV,
    time_summary,
)
from .service import InvoiceCreate, InvoicePrint


@never_cache
@user_passes_test(lambda u: u.is_staff)
def invoice_download(request, pk):
    """https://github.com/johnsensible/django-sendfile"""
    invoice = get_object_or_404(Invoice, pk=pk)
    check_perm_contact(request.user, invoice.contact)
    return sendfile(
        request,
        invoice.pdf.path,
        attachment=True,
        attachment_filename=f"{invoice.invoice_number}.pdf",
    )


@never_cache
@user_passes_test(lambda u: u.is_staff)
def report_invoice_time_analysis(request, pk):
    invoice = get_object_or_404(Invoice, pk=pk)
    check_perm_contact(request.user, invoice.contact)
    response = HttpResponse(content_type="application/pdf")
    file_name = "invoice_{}_time_analysis.pdf".format(invoice.invoice_number)
    response["Content-Disposition"] = 'attachment; filename="{}"'.format(
        file_name
    )
    report = ReportInvoiceTimeAnalysis()
    report.report(invoice, request.user, response)
    return response


@never_cache
@user_passes_test(lambda u: u.is_staff)
def report_invoice_time_analysis_csv(request, pk):
    invoice = get_object_or_404(Invoice, pk=pk)
    check_perm_contact(request.user, invoice.contact)
    response = HttpResponse(content_type="text/csv")
    file_name = "invoice_{}_time_analysis.csv".format(invoice.invoice_number)
    response["Content-Disposition"] = 'attachment; filename="{}"'.format(
        file_name
    )
    report = ReportInvoiceTimeAnalysisCSV()
    report.report(invoice, request.user, response)
    return response


class BatchListView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, ListView
):
    paginate_by = 20

    def get_queryset(self):
        return Batch.objects.all().order_by("-batch_date", "-pk")


class BatchInvoiceListMixin:
    paginate_by = 20

    def _batch(self):
        pk = self.kwargs.get("pk")
        return Batch.objects.get(pk=pk)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        batch = self._batch()
        net, vat = batch.net_and_vat()
        gross = net + vat
        context.update(
            dict(batch=batch, batch_net=net, batch_vat=vat, batch_gross=gross)
        )
        return context

    def get_queryset(self):
        batch = self._batch()
        return BatchInvoice.objects.filter(batch=batch).order_by(
            "-invoice__invoice_date", "-invoice__number"
        )


class BatchlessInvoiceListView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, ListView
):
    paginate_by = 20
    template_name = "invoice/batchless_list.html"

    def get_queryset(self):
        return Invoice.objects.filter(
            upfront_payment=False, is_credit=False
        ).order_by("-invoice_date", "-number")


class ContactInvoiceListView(
    LoginRequiredMixin, ContactPermMixin, BaseMixin, ListView
):
    template_name = "invoice/contact_invoice_list.html"

    def _contact(self):
        pk = self.kwargs.get("pk")
        model = get_contact_model()
        contact = model.objects.get(pk=pk)
        return contact

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(dict(contact=self._contact()))
        return context

    def get_queryset(self):
        contact = self._contact()
        return Invoice.objects.filter(contact=contact).order_by("-number")

    def test_contact(self):
        """The contact for the permission check (see ``ContactPermMixin``)."""
        return self._contact()


class ContactReportUpdateView(
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    RedirectNextMixin,
    BaseMixin,
    UpdateView,
):
    form_class = ContactEmptyForm
    template_name = "invoice/contact_report_detail.html"

    def _contact(self):
        pk = self.kwargs.get("pk")
        model = get_contact_model()
        contact = model.objects.get(pk=pk)
        return contact

    def _report_slug(self):
        return self.kwargs.get("slug")

    def _report_specification(self):
        report_specification = ReportSpecification.objects.get(
            slug=self._report_slug()
        )
        return report_specification

    def form_valid(self, form):
        self.object = form.save(commit=False)
        contact = self._contact()
        report_specification = self._report_specification()
        with transaction.atomic():
            to_date = timezone.now()
            from_date = to_date - relativedelta(years=+1)
            title = "{} - {} from {} to {}".format(
                report_specification.title,
                contact.full_name,
                from_date.strftime("%d/%m/%Y %H:%M"),
                to_date.strftime("%d/%m/%Y %H:%M"),
            )
            parameters = {
                "contact_pk": contact.pk,
                "from_date": from_date,
                "to_date": to_date,
            }
            self.schedule = report_specification.schedule(
                self.request.user, parameters=parameters, title=title
            )
        return HttpResponseRedirect(self.get_success_url())

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        try:
            context.update(
                dict(report_specification=self._report_specification())
            )
        except ReportSpecification.DoesNotExist:
            context.update(dict(report_slug=self._report_slug()))
        return context

    def get_object(self, *args, **kwargs):
        return self._contact()

    def get_success_url(self):
        url = reverse("report.schedule.csv.view", args=[self.schedule.pk])
        next_url = self.request.POST.get(REDIRECT_FIELD_NAME)
        if next_url:
            url = url_with_querystring(url, next=next_url)
        return url


class InvoiceContactCreateView(
    LoginRequiredMixin, ContactPermMixin, BaseMixin, CreateView
):
    model = InvoiceContact
    form_class = InvoiceContactForm

    def _contact(self):
        pk = self.kwargs.get("pk")
        model = get_contact_model()
        contact = model.objects.get(pk=pk)
        return contact

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.contact = self._contact()
        return super().form_valid(form)

    def test_contact(self):
        """The contact for the permission check (see ``ContactPermMixin``)."""
        return self._contact()


class InvoiceContactUpdateView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, UpdateView
):
    model = InvoiceContact
    form_class = InvoiceContactForm


class ContactTicketTimeRecordListView(
    LoginRequiredMixin, ContactPermMixin, BaseMixin, ListView
):
    paginate_by = 20
    template_name = "invoice/contact_ticket_timerecord.html"

    def _contact(self):
        pk = self.kwargs.get("pk")
        model = get_contact_model()
        contact = model.objects.get(pk=pk)
        return contact

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(dict(contact=self._contact()))
        return context

    def get_queryset(self):
        return TimeRecord.objects.report_time_by_crm_contact(
            self._contact(),
            date.today() + relativedelta(years=-1),
            date.today(),
        )

    def test_contact(self):
        """The contact for the permission check (see ``ContactPermMixin``)."""
        return self._contact()


class ContactTimeRecordListMixin:
    def _contact(self):
        pk = self.kwargs.get("pk")
        model = get_contact_model()
        contact = model.objects.get(pk=pk)
        return contact

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(dict(contact=self._contact()))
        return context

    def test_contact(self):
        """The contact for the permission check (see ``ContactPermMixin``)."""
        return self._contact()


class ContactTimeRecordListView(
    LoginRequiredMixin,
    ContactTimeRecordListMixin,
    ContactPermMixin,
    BaseMixin,
    ListView,
):
    paginate_by = 20
    template_name = "invoice/contact_timerecord_list.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(dict(title="Time records"), unbilled=False)
        return context

    def get_queryset(self):
        contact = self._contact()
        return TimeRecord.objects.filter(ticket__contact=contact).order_by(
            "date_started", "start_time"
        )


class ContactTimeRecordOutstandingListView(
    LoginRequiredMixin,
    ContactTimeRecordListMixin,
    ContactPermMixin,
    BaseMixin,
    ListView,
):
    paginate_by = 20
    template_name = "invoice/contact_timerecord_list.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(dict(title="Unbilled time records"), unbilled=True)
        return context

    def get_queryset(self):
        contact = self._contact()
        return TimeRecord.objects.to_invoice_contact(contact, timezone.now())


class DashMixin:
    CHARGE = "Chargeable"
    NON_CHARGE = "Non-Chargeable"

    def _charts(self):
        from_date = timezone.now() + relativedelta(months=-1)
        qs = TimeRecord.objects.filter(
            date_started__gte=from_date, date_started__lte=timezone.now()
        )
        charge = {self.CHARGE: 0, self.NON_CHARGE: 0}
        contact = {}
        user = {}
        for row in qs:
            if row.is_complete:
                minutes = int(row.minutes)
                user_name = row.user.username
                if not user_name in user:
                    user[user_name] = 0
                user[user_name] = user[user_name] + minutes
                if row.user == self.request.user:
                    user_name = row.ticket.contact.user.username
                    if not user_name in contact:
                        contact[user_name] = 0
                    contact[user_name] = contact[user_name] + minutes
                    if row.billable:
                        charge[self.CHARGE] = charge[self.CHARGE] + minutes
                    else:
                        charge[self.NON_CHARGE] = (
                            charge[self.NON_CHARGE] + minutes
                        )
        contact_x = []
        contact_y = []
        for k in sorted(contact, key=contact.get, reverse=True):
            contact_x.append(k)
            contact_y.append(contact[k])
        user_x = []
        user_y = []
        for k in sorted(user, key=user.get, reverse=True):
            user_x.append(k)
            user_y.append(user[k])
        return [
            {
                "charttype": "pieChart",
                "chartdata": {"x": contact_x, "y": contact_y},
                "chartcontainer": "piechart_container",
                "extra": {
                    "x_is_date": False,
                    "x_axis_format": "",
                    "tag_script_js": True,
                    "jquery_on_ready": False,
                },
            },
            {
                "charttype": "pieChart",
                "chartdata": {
                    "x": list(charge.keys()),
                    "y": list(charge.values()),
                },
                "chartcontainer": "charge_container",
                "extra": {
                    "x_is_date": False,
                    "x_axis_format": "",
                    "tag_script_js": True,
                    "jquery_on_ready": False,
                },
            },
            {
                "charttype": "pieChart",
                "chartdata": {"x": user_x, "y": user_y},
                "chartcontainer": "user_container",
                "extra": {
                    "x_is_date": False,
                    "x_axis_format": "",
                    "tag_script_js": True,
                    "jquery_on_ready": False,
                },
            },
        ]

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({"charts": self._charts()})
        return context


class InvoiceCreateViewMixin(BaseMixin, CreateView):
    model = Invoice

    def _contact(self):
        pk = self.kwargs.get("pk")
        model = get_contact_model()
        contact = model.objects.get(pk=pk)
        return contact

    def _check_invoice_settings(self, contact):
        warnings = InvoiceCreate().is_valid(contact)
        for message in warnings:
            messages.warning(self.request, message)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        contact = self._contact()
        self._check_invoice_settings(contact)
        context.update(dict(contact=contact))
        return context


class InvoiceCreditListView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, ListView
):
    paginate_by = 20
    template_name = "invoice/invoice_credit_list.html"

    def get_queryset(self):
        qs = Invoice.objects.current().filter(is_credit=True)
        return qs.order_by("-invoice_date", "prefix", "-number")


class InvoiceDetailMixin:
    model = Invoice

    def _batch_invoice(self):
        result = None
        try:
            result = BatchInvoice.objects.get(invoice=self.object)
        except BatchInvoice.DoesNotExist:
            pass
        return result

    def _invoice_contact(self):
        result = None
        try:
            result = InvoiceContact.objects.get(contact=self.object.contact)
        except InvoiceContact.DoesNotExist:
            pass
        return result

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if self.object.is_credit:
            qs = InvoiceCredit.objects.filter(credit_note=self.object)
        else:
            qs = InvoiceCredit.objects.filter(invoice=self.object)
        context.update(
            dict(
                batch_invoice=self._batch_invoice(),
                invoice_contact=self._invoice_contact(),
                invoice_credits=qs,
            )
        )
        return context


class InvoiceDraftCreateView(
    LoginRequiredMixin, StaffuserRequiredMixin, InvoiceCreateViewMixin
):
    form_class = InvoiceUpdateForm
    template_name = "invoice/invoice_create_draft_form.html"

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.contact = self._contact()
        self.object.number = Invoice.objects.next_number()
        self.object.user = self.request.user
        return super().form_valid(form)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update({"contact": self._contact()})
        return kwargs


class InvoiceIssueListView(
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    RedirectNextMixin,
    BaseMixin,
    ListView,
):
    model = InvoiceIssue
    paginate_by = 20


class InvoiceIssueUpdateView(
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    RedirectNextMixin,
    BaseMixin,
    UpdateView,
):
    form_class = InvoiceIssueForm
    model = InvoiceIssue

    def form_valid(self, form):
        result = super().form_valid(form)
        if self.object.confirmed:
            caption = (
                "credit note" if self.object.invoice.is_credit else "Invoice"
            )
            messages.info(
                self.request,
                "Resolved issue #{} for {} {} dated {}".format(
                    self.object.pk,
                    caption,
                    self.object.invoice.invoice_number,
                    self.object.invoice.invoice_date.strftime("%d/%m/%Y"),
                ),
            )
        return result

    def get_success_url(self):
        next_url = self.request.POST.get(REDIRECT_FIELD_NAME)
        if next_url:
            return next_url
        else:
            return reverse("project.dash")


class InvoiceLineCreateView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, CreateView
):
    form_class = InvoiceLineForm
    model = InvoiceLine
    template_name = "invoice/invoiceline_create_form.html"

    def _get_invoice(self):
        pk = self.kwargs.get("pk")
        invoice = get_object_or_404(Invoice, pk=pk)
        return invoice

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(dict(invoice=self._get_invoice()))
        return context

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.invoice = self._get_invoice()
        self.object.line_number = self.object.invoice.get_next_line_number()
        self.object.user = self.request.user
        self.object.save_and_calculate()
        return HttpResponseRedirect(self.get_success_url())


class InvoiceLineUpdateView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, UpdateView
):
    form_class = InvoiceLineForm
    model = InvoiceLine
    template_name = "invoice/invoiceline_update_form.html"

    def form_valid(self, form):
        """If the form is valid, save the associated model."""
        self.object = form.save(commit=False)
        self.object.save_and_calculate()
        return HttpResponseRedirect(self.get_success_url())

    def get_object(self, *args, **kwargs):
        obj = super().get_object(*args, **kwargs)
        if not obj.user_can_edit:
            raise PermissionDenied()
        return obj


class InvoiceListMixin:
    model = Invoice
    paginate_by = 20

    def _display_payment_processor(self):
        """Show or hide the payment processor field.

        To hide the payment processor field, add the following to your view::

          display_payment_processor = False

        """
        result = True
        try:
            result = self.display_payment_processor
        except AttributeError:
            pass
        return result

    def _form(self):
        is_valid = True
        currency_qs = Currency.objects.all()
        payment_processor_qs = PaymentProcessor.objects.all()
        if self.request.GET:
            form = SearchForm(
                self.request.GET,
                currency_qs=currency_qs,
                display_payment_processor=self._display_payment_processor(),
                payment_processor_qs=payment_processor_qs,
            )
            is_valid = form.is_valid()
        else:
            form = SearchForm(
                initial=self.get_initial(),
                currency_qs=currency_qs,
                display_payment_processor=self._display_payment_processor(),
                payment_processor_qs=payment_processor_qs,
            )
        return form, is_valid

    def _get_datepicker(self, field_name):
        result = None
        value = self.request.GET.get(field_name)
        if value:
            result = datetime.strptime(value, "%d/%m/%Y").date()
        return result

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        form, is_valid = self._form()
        get_parameters = self.request.GET.copy()
        if self.page_kwarg in get_parameters:
            del get_parameters[self.page_kwarg]
        net_total, gross_total = self.net_and_gross()
        context.update(
            {
                "form": form,
                "get_parameters": get_parameters.urlencode(),
                "gross_total": gross_total,
                "net_total": net_total,
            }
        )
        return context

    def net_and_gross(self):
        net = vat = Decimal()
        invoice_date = self._get_datepicker("invoice_date")
        if (
            self.request.GET.get("currency")
            or self.request.GET.get("number")
            or self.request.GET.get("payment_processor")
        ):
            # total if the user selected the invoice date and nothing else
            pass
        elif invoice_date:
            net, vat = InvoiceLine.objects.net_and_vat(invoice_date)
        return net, (net + vat)

    def get_queryset(self):
        currency = payment_processor = None
        currency_pk = self.request.GET.get("currency")
        invoice_date = self._get_datepicker("invoice_date")
        invoice_number = self.request.GET.get("number")
        processor_pk = self.request.GET.get("payment_processor")
        if currency_pk:
            currency = Currency.objects.get(pk=currency_pk)
        if processor_pk:
            payment_processor = PaymentProcessor.objects.get(pk=processor_pk)
        qs = Invoice.objects.all()
        if currency:
            qs = qs.filter(currency=currency)
        if invoice_date:
            qs = qs.filter(invoice_date=invoice_date)
        if invoice_number:
            qs = qs.filter(number=invoice_number)
        if payment_processor:
            qs = qs.filter(upfront_payment_processor=payment_processor)
        return qs.order_by("-invoice_date", "prefix", "-number")

    def get_initial(self):
        return {"invoice_date": self._get_datepicker("invoice_date")}


class InvoicePdfUpdateView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, UpdateView
):
    form_class = InvoiceEmptyForm
    model = Invoice
    template_name = "invoice/invoice_create_pdf_form.html"

    def _check_invoice_print(self, invoice):
        warnings = InvoicePrint().is_valid(invoice)
        for message in warnings:
            messages.warning(self.request, message)

    def get_object(self, *args, **kwargs):
        obj = super().get_object(*args, **kwargs)
        self._check_invoice_print(obj)
        return obj

    def form_valid(self, form):
        InvoicePrint().create_pdf(self.object, header_image=None)
        messages.info(
            self.request,
            "Created PDF for invoice {}, {} at {} today.".format(
                self.object.invoice_number,
                self.object.contact.full_name,
                self.object.created.strftime("%H:%M"),
            ),
        )
        return HttpResponseRedirect(reverse("invoice.list"))


class InvoiceRefreshTimeRecordsUpdateView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, UpdateView
):
    form_class = InvoiceBlankTodayForm
    model = Invoice
    template_name = "invoice/invoice_refresh_time_records_form.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(
            dict(
                timerecords=InvoiceCreate().draft(
                    self.object.contact, date.today()
                )
            )
        )
        return context

    def form_valid(self, form):
        iteration_end = form.cleaned_data["iteration_end"]
        invoice_create = InvoiceCreate()
        self.object = invoice_create.refresh(
            self.request.user, self.object, iteration_end
        )
        if self.object:
            messages.info(
                self.request,
                "Refreshed time for invoice {} at {} today.".format(
                    self.object.invoice_number,
                    self.object.created.strftime("%H:%M"),
                ),
            )
        return HttpResponseRedirect(
            reverse("invoice.detail", args=[self.object.pk])
        )


class InvoiceRemoveTimeRecordsUpdateView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, UpdateView
):
    form_class = InvoiceEmptyForm
    model = Invoice
    template_name = "invoice/invoice_remove_time_records_form.html"

    def form_valid(self, form):
        self.object.remove_time_lines()
        messages.info(
            self.request,
            "Removed time records from invoice {} at {} today.".format(
                self.object.invoice_number,
                self.object.created.strftime("%H:%M"),
            ),
        )
        return HttpResponseRedirect(
            reverse("invoice.detail", args=[self.object.pk])
        )


class InvoiceSettingsUpdateView(
    LoginRequiredMixin, SuperuserRequiredMixin, BaseMixin, UpdateView
):
    form_class = InvoiceSettingsForm

    def get_object(self, *args, **kwargs):
        return InvoiceSettings.load()

    def get_success_url(self):
        return reverse("project.settings")


class InvoiceSetToDraftUpdateView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, UpdateView
):
    form_class = InvoiceEmptyForm
    model = Invoice
    template_name = "invoice/invoice_set_to_draft_form.html"

    def form_valid(self, form):
        self.object.set_to_draft()
        messages.info(
            self.request,
            "Set invoice {} to draft at {} today.".format(
                self.object.invoice_number,
                self.object.created.strftime("%H:%M"),
            ),
        )
        return HttpResponseRedirect(
            reverse("invoice.detail", args=[self.object.pk])
        )


class InvoiceTimeCreateView(
    LoginRequiredMixin, StaffuserRequiredMixin, InvoiceCreateViewMixin
):
    form_class = InvoiceBlankTodayForm
    template_name = "invoice/invoice_create_time_form.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(
            dict(
                timerecords=InvoiceCreate().draft(self._contact(), date.today())
            )
        )
        return context

    def form_valid(self, form):
        currency = form.cleaned_data["currency"]
        iteration_end = form.cleaned_data["iteration_end"]
        invoice_create = InvoiceCreate()
        self.object = invoice_create.create(
            self.request.user, self._contact(), currency, iteration_end
        )
        if self.object:
            messages.info(
                self.request,
                "Draft invoice {} for {} created at {} today.".format(
                    self.object.invoice_number,
                    self.object.contact.full_name,
                    self.object.created.strftime("%H:%M"),
                ),
            )
            return HttpResponseRedirect(reverse("invoice.list"))
        else:
            messages.warning(
                self.request,
                (
                    "Could not create the invoice.  Are there pending time "
                    "records dated on (or before) {}?".format(
                        iteration_end.strftime("%d %B %Y")
                    )
                ),
            )
            return self.render_to_response(self.get_context_data(form=form))


class InvoiceUpdateView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, UpdateView
):
    form_class = InvoiceUpdateForm
    model = Invoice
    template_name = "invoice/invoice_update_form.html"

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update({"contact": self.object.contact})
        return kwargs


class InvoiceUserUpdateView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, UpdateView
):
    form_class = InvoiceUserUpdateForm
    template_name = "invoice/invoice_user_update_form.html"

    def get_object(self, *args, **kwargs):
        try:
            obj = InvoiceUser.objects.get(user=self.request.user)
        except InvoiceUser.DoesNotExist:
            obj = InvoiceUser(user=self.request.user)
            obj.save()
        return obj

    def get_success_url(self):
        return reverse("project.settings")


class QuickTimeRecordCreateView(LoginRequiredMixin, BaseMixin, CreateView):
    form_class = QuickTimeRecordForm
    model = QuickTimeRecord

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.user = self.request.user
        return super().form_valid(form)

    def get_success_url(self):
        return reverse("invoice.quick.time.record.list")


class QuickTimeRecordDeleteView(
    LoginRequiredMixin, UserPassesTestMixin, BaseMixin, UpdateView
):
    form_class = QuickTimeRecordEmptyForm
    model = QuickTimeRecord
    template_name = "invoice/quicktimerecord_delete_form.html"

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.deleted = True
        return super().form_valid(form)

    def get_success_url(self):
        return reverse("invoice.quick.time.record.list")

    def test_func(self, user):
        quick_time_record = self.get_object()
        return self.request.user == quick_time_record.user


class QuickTimeRecordListView(LoginRequiredMixin, BaseMixin, ListView):
    model = QuickTimeRecord

    def get_queryset(self):
        return QuickTimeRecord.objects.quick(self.request.user)


class QuickTimeRecordUpdateView(
    LoginRequiredMixin, UserPassesTestMixin, BaseMixin, UpdateView
):
    form_class = QuickTimeRecordForm
    model = QuickTimeRecord

    def get_success_url(self):
        return reverse("invoice.quick.time.record.list")

    def test_func(self, user):
        quick_time_record = self.get_object()
        return self.request.user == quick_time_record.user


class ReconcileDayView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, TemplateView
):
    template_name = "invoice/reconcile_day_view.html"

    def _batches(self):
        date = self._date().strftime("%Y-%m-%d")
        return Batch.objects.filter(batch_date=date).order_by(
            "batch_date", "currency", "payment_processor"
        )

    def _date(self):
        year = self.kwargs.get("year")
        month = self.kwargs.get("month")
        day = self.kwargs.get("day")
        return date(year, month, day)

    def batch_net_and_gross(self):
        total_net = 0
        total_gross = 0
        batch_date = self._date()
        currencies = Currency.objects.all()
        payment_processors = PaymentProcessor.objects.all()
        for currency in currencies:
            for payment_processor in payment_processors:
                try:
                    batch = Batch.objects.get(
                        batch_date=batch_date.strftime("%Y-%m-%d"),
                        currency=currency,
                        payment_processor=payment_processor,
                    )
                    net, vat = batch.net_and_vat()
                    total_net += net
                    total_gross += vat + net
                except Batch.DoesNotExist:
                    continue

        return total_net, total_gross

    def _unbatched_invoices(self):
        date = self._date().strftime("%Y-%m-%d")
        return Invoice.objects.filter(
            invoice_date=date, upfront_payment=False, is_credit=False
        ).order_by("invoice_date")

    def _issues(self):
        invoice_date = self._date()
        return InvoiceIssue.objects.issues(invoice_date).order_by(
            "invoice_id", "pk"
        )

    def _credit_notes(self):
        date = self._date().strftime("%Y-%m-%d")
        credit_notes = Invoice.objects.filter(
            invoice_date=date, is_credit=True
        )  # .order_by("invoice_date")
        return credit_notes

    def unbatched_invoice_net_and_gross(self):
        total_net = 0
        total_gross = 0
        try:
            invoices = self._unbatched_invoices()
            for invoice in invoices:
                net = invoice.net
                gross = invoice.gross
                total_net += net
                total_gross += gross
        except Invoice.DoesNotExist:
            pass
        return total_net, total_gross

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        batch_date = self._date()
        batches = self._batches()
        batch_net, batch_gross = self.batch_net_and_gross()
        invoices = self._unbatched_invoices()
        (
            invoice_net_total,
            invoice_gross_total,
        ) = self.unbatched_invoice_net_and_gross()
        issues = self._issues()
        credit_notes = self._credit_notes()
        context.update(
            dict(
                batch_date=batch_date,
                batch_gross=batch_gross,
                batch_net=batch_net,
                batches=batches,
                credit_notes=credit_notes,
                invoice_gross=invoice_gross_total,
                invoice_net=invoice_net_total,
                invoiceissue_list=issues,
                invoices=invoices,
                tomorrow=batch_date + relativedelta(days=1),
                yesterday=batch_date + relativedelta(days=-1),
            )
        )
        return context
        # [PK 03/05/2019] Do we need the following lines of code?
        # context.update(
        #    {"form": form, "net_total": net_total, "gross_total": gross_total}
        # )


class TimeRecordCreateView(
    LoginRequiredMixin,
    ContactPermMixin,
    RedirectNextMixin,
    BaseMixin,
    CreateView,
):
    form_class = TimeRecordForm
    model = TimeRecord

    def _ticket(self):
        pk = self.kwargs.get("pk")
        ticket = get_object_or_404(Ticket, pk=pk)
        return ticket

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(dict(ticket=self._ticket()))
        return context

    def get_initial(self):
        return dict(
            date_started=date.today(),
            start_time=timezone.localtime(timezone.now()).time(),
        )

    def form_valid(self, form):
        with transaction.atomic():
            self.object = form.save(commit=False)
            self.object.ticket = self._ticket()
            self.object.user = self.request.user
            result = super().form_valid(form)
            transaction.on_commit(
                lambda: update_ticket_index.send_with_options(
                    args=(self.object.ticket.pk,)
                )
            )
        return result

    def get_success_url(self):
        next_url = self.request.POST.get(REDIRECT_FIELD_NAME)
        if next_url:
            return next_url
        else:
            return super().get_success_url()

    def test_contact(self):
        ticket = self._ticket()
        return ticket.contact


class TimeRecordListView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, ListView
):
    paginate_by = 20
    model = TimeRecord


class TimeRecordSummaryMixin:
    template_name = "invoice/time_record_summary.html"

    def _report(self, user):
        return time_summary(user)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(dict(today=date.today()))
        return context


class TimeRecordSummaryView(
    LoginRequiredMixin, TimeRecordSummaryMixin, BaseMixin, FormView
):
    form_class = TimeRecordEmptyForm

    def form_valid(self, form):
        pk = self.request.POST.get("pk")
        try:
            time_record = TimeRecord.objects.get(pk=pk)
        except TimeRecord.DoesNotExist:
            raise InvoiceError("Time record '{}' does not exist".format(pk))
        time_record.stop()
        return HttpResponseRedirect(reverse("invoice.time.summary"))

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        full_name = self.request.user.get_full_name()
        if not full_name:
            full_name = self.request.user.username
        context.update(
            dict(
                report=self._report(self.request.user),
                running=TimeRecord.objects.running(self.request.user),
                user_full_name=full_name,
            )
        )
        return context


class TimeRecordSummaryUserView(
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    TimeRecordSummaryMixin,
    BaseMixin,
    TemplateView,
):
    def _user(self):
        pk = self.kwargs.get("pk")
        return get_user_model().objects.get(pk=pk)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        user = self._user()
        full_name = user.get_full_name() or user.username
        context.update(
            dict(
                report=self._report(self._user()),
                running=None,
                user_full_name=full_name,
            )
        )
        return context


class TicketListMonthView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, ListView
):
    template_name = "invoice/ticket_list_month.html"

    def _from_date(self):
        month = int(self.kwargs.get("month", 0))
        year = int(self.kwargs.get("year", 0))
        try:
            return date(year, month, 1)
        except ValueError:
            raise Http404("Invalid date.")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        report_date = self._from_date()
        next_month = report_date + relativedelta(months=+1, day=1)
        if next_month > date.today():
            next_month = None
        context.update(
            dict(
                next_month=next_month,
                prev_month=report_date + relativedelta(months=-1, day=1),
                report_date=report_date,
            )
        )
        return context

    def get_queryset(self):
        from_date = self._from_date()
        to_date = from_date + relativedelta(months=+1, day=1, days=-1)
        return TimeRecord.objects.tickets(
            from_date, to_date, self.request.user
        ).order_by("contact__user__username", "pk")


class TicketTimeRecordListView(
    LoginRequiredMixin, ContactPermMixin, BaseMixin, ListView
):
    template_name = "invoice/ticket_timerecord_list.html"

    def _get_ticket(self):
        pk = self.kwargs.get("pk")
        ticket = get_object_or_404(Ticket, pk=pk)
        # self._check_perm(ticket.contact)
        return ticket

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        ticket = self._get_ticket()
        context.update(
            dict(
                ticket=ticket,
                ticket_totals=TimeRecord.objects.ticket_totals(ticket),
            )
        )
        return context

    def get_queryset(self):
        ticket = self._get_ticket()
        return TimeRecord.objects.filter(ticket=ticket).order_by(
            "-date_started", "-start_time"
        )

    def test_contact(self):
        """The contact for the permission check (see ``ContactPermMixin``)."""
        ticket = self._get_ticket()
        return ticket.contact


class TimeRecordOutstandingBillFreeListView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, ListView
):
    """Tickets where time is ``billable`` but the ticket is free of charge."""

    paginate_by = 20
    template_name = "invoice/timerecord_outstanding_bill_free_list.html"

    def _from_date(self):
        return timezone.now() - relativedelta(months=+2)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(dict(from_date=self._from_date()))
        return context

    def get_queryset(self):
        free_of_charge_tickets = free_of_charge_ticket_pks()
        ticket_pks = (
            TimeRecord.objects.to_invoice()
            .filter(date_started__gte=self._from_date())
            .filter(ticket__pk__in=free_of_charge_tickets)
            .values_list("ticket__pk", flat=True)
        )
        return Ticket.objects.filter(pk__in=ticket_pks).order_by(
            "contact__user__username", "pk"
        )


class TimeRecordOutstandingListView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, ListView
):
    paginate_by = 20
    template_name = "invoice/timerecord_outstanding_list.html"

    def get_queryset(self):
        contact_pks = (
            TimeRecord.objects.to_invoice(
                exclude_fixed_price=True, exclude_free_of_charge=True
            )
            .order_by("ticket__contact__pk")
            .distinct("ticket__contact__pk")
            .values_list("ticket__contact__pk", flat=True)
        )
        model = get_contact_model()
        return model.objects.filter(pk__in=contact_pks).order_by(
            "user__username"
        )


class TimeRecordUpdateView(
    LoginRequiredMixin,
    ContactPermMixin,
    RedirectNextMixin,
    BaseMixin,
    UpdateView,
):
    form_class = TimeRecordForm
    model = TimeRecord

    def form_valid(self, form):
        with transaction.atomic():
            result = super().form_valid(form)
            transaction.on_commit(
                lambda: update_ticket_index.send_with_options(
                    args=(self.object.ticket.pk,)
                )
            )
        return result

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        is_old_time_record = not self.object.is_today()
        context.update(
            dict(
                is_old_time_record=is_old_time_record, ticket=self.object.ticket
            )
        )
        return context

    def get_object(self, *args, **kwargs):
        obj = super().get_object(*args, **kwargs)
        if not obj.user_can_edit:
            raise PermissionDenied()
        return obj

    def get_success_url(self):
        next_url = self.request.POST.get(REDIRECT_FIELD_NAME)
        if next_url:
            return next_url
        else:
            return super().get_success_url()

    def test_contact(self):
        time_record = self.get_object()
        return time_record.ticket.contact


class UserTimeRecordListView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, ListView
):
    paginate_by = 20

    def get_queryset(self):
        return TimeRecord.objects.filter(user=self.request.user).order_by(
            "-date_started", "-start_time"
        )
